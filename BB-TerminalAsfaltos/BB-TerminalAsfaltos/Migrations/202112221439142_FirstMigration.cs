﻿namespace BB_TerminalAsfaltos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Weighings", "DriverID", "dbo.Drivers");
            DropForeignKey("dbo.Weighings", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.Weighings", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Weighings", "TankID", "dbo.Tanks");
            DropForeignKey("dbo.Weighings", "TrailerID", "dbo.Trailers");
            DropForeignKey("dbo.Weighings", "TransporterID", "dbo.Transporters");
            DropForeignKey("dbo.Weighings", "VehicleID", "dbo.Vehicles");
            DropIndex("dbo.Weighings", new[] { "VehicleID" });
            DropIndex("dbo.Weighings", new[] { "TrailerID" });
            DropIndex("dbo.Weighings", new[] { "TankID" });
            DropIndex("dbo.Weighings", new[] { "ProductID" });
            DropIndex("dbo.Weighings", new[] { "DriverID" });
            DropIndex("dbo.Weighings", new[] { "TransporterID" });
            DropIndex("dbo.Weighings", new[] { "EntityID" });
            //RenameColumn(table: "dbo.Tickets", name: "Driver_Id", newName: "DriverId");
            //RenameColumn(table: "dbo.Tickets", name: "Entity_Id", newName: "EntityId");
            //RenameColumn(table: "dbo.Tickets", name: "Product_Id", newName: "ProductId");
            //RenameColumn(table: "dbo.Tickets", name: "Tank_Id", newName: "TankId");
            //RenameColumn(table: "dbo.Tickets", name: "Trailer_Id", newName: "TrailerId");
            //RenameColumn(table: "dbo.Tickets", name: "Transporter_Id", newName: "TransporterId");
            //RenameColumn(table: "dbo.Tickets", name: "Vehicle_Id", newName: "VehicleId");
            //RenameIndex(table: "dbo.Tickets", name: "IX_Vehicle_Id", newName: "IX_VehicleId");
            //RenameIndex(table: "dbo.Tickets", name: "IX_Trailer_Id", newName: "IX_TrailerId");
            //RenameIndex(table: "dbo.Tickets", name: "IX_Tank_Id", newName: "IX_TankId");
            //RenameIndex(table: "dbo.Tickets", name: "IX_Product_Id", newName: "IX_ProductId");
            //RenameIndex(table: "dbo.Tickets", name: "IX_Driver_Id", newName: "IX_DriverId");
            //RenameIndex(table: "dbo.Tickets", name: "IX_Transporter_Id", newName: "IX_TransporterId");
            //RenameIndex(table: "dbo.Tickets", name: "IX_Entity_Id", newName: "IX_EntityId");
            //AddColumn("dbo.Drivers", "Name", c => c.String());
            //AddColumn("dbo.Drivers", "DateID", c => c.DateTime(nullable: false));
            //AddColumn("dbo.Drivers", "DateADR", c => c.DateTime(nullable: false));
            //AddColumn("dbo.Drivers", "DateDriversLicense", c => c.DateTime(nullable: false));
            AddColumn("dbo.Drivers", "DateSecurityInduction", c => c.DateTime(nullable: false));
            //AddColumn("dbo.Tickets", "LastPrintDate", c => c.DateTime());
            //AddColumn("dbo.Tickets", "LastPrintTime", c => c.String());
            //AddColumn("dbo.Tickets", "LoadTemperature", c => c.String());
            //AddColumn("dbo.Tickets", "Batch", c => c.String());
            //AddColumn("dbo.Tickets", "Observations", c => c.String());
            //AddColumn("dbo.Tickets", "DeliveryNote", c => c.String());
            //AddColumn("dbo.Tickets", "MaximumWeight", c => c.String());
            //AddColumn("dbo.Tickets", "Tare", c => c.String());
            //AddColumn("dbo.Tickets", "TareHour", c => c.String());
            //AddColumn("dbo.Tickets", "Gross", c => c.String());
            //AddColumn("dbo.Tickets", "Net", c => c.String());
            //AddColumn("dbo.Weighings", "TareHour", c => c.String());
            //AddColumn("dbo.Weighings", "manualTare", c => c.Boolean(nullable: false));
            //AddColumn("dbo.Weighings", "bypassed", c => c.Boolean(nullable: false));
            //AddColumn("dbo.Entities", "Name", c => c.String());
            //AddColumn("dbo.Products", "Name", c => c.String());
            //AddColumn("dbo.Tanks", "Name", c => c.String());
            //AddColumn("dbo.Trailers", "LicensePlate", c => c.String());
            //AddColumn("dbo.Trailers", "DateInsurance", c => c.DateTime(nullable: false));
            //AddColumn("dbo.Trailers", "DateADR", c => c.DateTime(nullable: false));
            //AddColumn("dbo.Transporters", "Name", c => c.String());
            //AddColumn("dbo.Vehicles", "LicensePlate", c => c.String());
            //AddColumn("dbo.Vehicles", "DateInsurance", c => c.DateTime(nullable: false));
            //AddColumn("dbo.Vehicles", "DateADR", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Tickets", "Number", c => c.Int());
            AlterColumn("dbo.Weighings", "VehicleID", c => c.Int());
            AlterColumn("dbo.Weighings", "TrailerID", c => c.Int());
            AlterColumn("dbo.Weighings", "TankID", c => c.Int());
            AlterColumn("dbo.Weighings", "ProductID", c => c.Int());
            AlterColumn("dbo.Weighings", "DriverID", c => c.Int());
            AlterColumn("dbo.Weighings", "TransporterID", c => c.Int());
            AlterColumn("dbo.Weighings", "EntityID", c => c.Int());
            AlterColumn("dbo.Weighings", "MaximumWeight", c => c.String());
            AlterColumn("dbo.Weighings", "Tare", c => c.String());
            AlterColumn("dbo.Weighings", "Gross", c => c.String());
            AlterColumn("dbo.Weighings", "Net", c => c.String());
            CreateIndex("dbo.Weighings", "VehicleID");
            CreateIndex("dbo.Weighings", "TrailerID");
            CreateIndex("dbo.Weighings", "TankID");
            CreateIndex("dbo.Weighings", "ProductID");
            CreateIndex("dbo.Weighings", "DriverID");
            CreateIndex("dbo.Weighings", "TransporterID");
            CreateIndex("dbo.Weighings", "EntityID");
            AddForeignKey("dbo.Weighings", "DriverID", "dbo.Drivers", "Id");
            AddForeignKey("dbo.Weighings", "EntityID", "dbo.Entities", "Id");
            AddForeignKey("dbo.Weighings", "ProductID", "dbo.Products", "Id");
            AddForeignKey("dbo.Weighings", "TankID", "dbo.Tanks", "Id");
            AddForeignKey("dbo.Weighings", "TrailerID", "dbo.Trailers", "Id");
            AddForeignKey("dbo.Weighings", "TransporterID", "dbo.Transporters", "Id");
            AddForeignKey("dbo.Weighings", "VehicleID", "dbo.Vehicles", "Id");
            //DropColumn("dbo.Tickets", "PrintDate");
            //DropColumn("dbo.Tickets", "PrintTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tickets", "PrintTime", c => c.String());
            AddColumn("dbo.Tickets", "PrintDate", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.Weighings", "VehicleID", "dbo.Vehicles");
            DropForeignKey("dbo.Weighings", "TransporterID", "dbo.Transporters");
            DropForeignKey("dbo.Weighings", "TrailerID", "dbo.Trailers");
            DropForeignKey("dbo.Weighings", "TankID", "dbo.Tanks");
            DropForeignKey("dbo.Weighings", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Weighings", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.Weighings", "DriverID", "dbo.Drivers");
            DropIndex("dbo.Weighings", new[] { "EntityID" });
            DropIndex("dbo.Weighings", new[] { "TransporterID" });
            DropIndex("dbo.Weighings", new[] { "DriverID" });
            DropIndex("dbo.Weighings", new[] { "ProductID" });
            DropIndex("dbo.Weighings", new[] { "TankID" });
            DropIndex("dbo.Weighings", new[] { "TrailerID" });
            DropIndex("dbo.Weighings", new[] { "VehicleID" });
            AlterColumn("dbo.Weighings", "Net", c => c.Double(nullable: false));
            AlterColumn("dbo.Weighings", "Gross", c => c.Double(nullable: false));
            AlterColumn("dbo.Weighings", "Tare", c => c.Double(nullable: false));
            AlterColumn("dbo.Weighings", "MaximumWeight", c => c.Double(nullable: false));
            AlterColumn("dbo.Weighings", "EntityID", c => c.Int(nullable: false));
            AlterColumn("dbo.Weighings", "TransporterID", c => c.Int(nullable: false));
            AlterColumn("dbo.Weighings", "DriverID", c => c.Int(nullable: false));
            AlterColumn("dbo.Weighings", "ProductID", c => c.Int(nullable: false));
            AlterColumn("dbo.Weighings", "TankID", c => c.Int(nullable: false));
            AlterColumn("dbo.Weighings", "TrailerID", c => c.Int(nullable: false));
            AlterColumn("dbo.Weighings", "VehicleID", c => c.Int(nullable: false));
            AlterColumn("dbo.Tickets", "Number", c => c.Int(nullable: false));
            DropColumn("dbo.Vehicles", "DateADR");
            DropColumn("dbo.Vehicles", "DateInsurance");
            DropColumn("dbo.Vehicles", "LicensePlate");
            DropColumn("dbo.Transporters", "Name");
            DropColumn("dbo.Trailers", "DateADR");
            DropColumn("dbo.Trailers", "DateInsurance");
            DropColumn("dbo.Trailers", "LicensePlate");
            DropColumn("dbo.Tanks", "Name");
            DropColumn("dbo.Products", "Name");
            DropColumn("dbo.Entities", "Name");
            DropColumn("dbo.Weighings", "bypassed");
            DropColumn("dbo.Weighings", "manualTare");
            DropColumn("dbo.Weighings", "TareHour");
            DropColumn("dbo.Tickets", "Net");
            DropColumn("dbo.Tickets", "Gross");
            DropColumn("dbo.Tickets", "TareHour");
            DropColumn("dbo.Tickets", "Tare");
            DropColumn("dbo.Tickets", "MaximumWeight");
            DropColumn("dbo.Tickets", "DeliveryNote");
            DropColumn("dbo.Tickets", "Observations");
            DropColumn("dbo.Tickets", "Batch");
            DropColumn("dbo.Tickets", "LoadTemperature");
            DropColumn("dbo.Tickets", "LastPrintTime");
            DropColumn("dbo.Tickets", "LastPrintDate");
            DropColumn("dbo.Drivers", "DateSecurityInduction");
            DropColumn("dbo.Drivers", "DateDriversLicense");
            DropColumn("dbo.Drivers", "DateADR");
            DropColumn("dbo.Drivers", "DateID");
            DropColumn("dbo.Drivers", "Name");
            RenameIndex(table: "dbo.Tickets", name: "IX_EntityId", newName: "IX_Entity_Id");
            RenameIndex(table: "dbo.Tickets", name: "IX_TransporterId", newName: "IX_Transporter_Id");
            RenameIndex(table: "dbo.Tickets", name: "IX_DriverId", newName: "IX_Driver_Id");
            RenameIndex(table: "dbo.Tickets", name: "IX_ProductId", newName: "IX_Product_Id");
            RenameIndex(table: "dbo.Tickets", name: "IX_TankId", newName: "IX_Tank_Id");
            RenameIndex(table: "dbo.Tickets", name: "IX_TrailerId", newName: "IX_Trailer_Id");
            RenameIndex(table: "dbo.Tickets", name: "IX_VehicleId", newName: "IX_Vehicle_Id");
            RenameColumn(table: "dbo.Tickets", name: "VehicleId", newName: "Vehicle_Id");
            RenameColumn(table: "dbo.Tickets", name: "TransporterId", newName: "Transporter_Id");
            RenameColumn(table: "dbo.Tickets", name: "TrailerId", newName: "Trailer_Id");
            RenameColumn(table: "dbo.Tickets", name: "TankId", newName: "Tank_Id");
            RenameColumn(table: "dbo.Tickets", name: "ProductId", newName: "Product_Id");
            RenameColumn(table: "dbo.Tickets", name: "EntityId", newName: "Entity_Id");
            RenameColumn(table: "dbo.Tickets", name: "DriverId", newName: "Driver_Id");
            CreateIndex("dbo.Weighings", "EntityID");
            CreateIndex("dbo.Weighings", "TransporterID");
            CreateIndex("dbo.Weighings", "DriverID");
            CreateIndex("dbo.Weighings", "ProductID");
            CreateIndex("dbo.Weighings", "TankID");
            CreateIndex("dbo.Weighings", "TrailerID");
            CreateIndex("dbo.Weighings", "VehicleID");
            AddForeignKey("dbo.Weighings", "VehicleID", "dbo.Vehicles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Weighings", "TransporterID", "dbo.Transporters", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Weighings", "TrailerID", "dbo.Trailers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Weighings", "TankID", "dbo.Tanks", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Weighings", "ProductID", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Weighings", "EntityID", "dbo.Entities", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Weighings", "DriverID", "dbo.Drivers", "Id", cascadeDelete: true);
        }
    }
}
