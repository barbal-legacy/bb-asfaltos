﻿namespace BB_TerminalAsfaltos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TicketMultipleTanks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "TanksID", c => c.String());
            AddColumn("dbo.Weighings", "TanksID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Weighings", "TanksID");
            DropColumn("dbo.Tickets", "TanksID");
        }
    }
}
