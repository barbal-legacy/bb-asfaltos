﻿namespace BB_TerminalAsfaltos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WeighingIsFinished : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Weighings", "IsFinished", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Weighings", "IsFinished");
        }
    }
}
