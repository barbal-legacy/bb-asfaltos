﻿namespace BB_TerminalAsfaltos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OriginalDatabase : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Companies",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        Name = c.String(),
            //        Address = c.String(),
            //        ZipCode = c.String(),
            //        TaxPayerNumber = c.String(),
            //        ContactNumber = c.String(),
            //        Fax = c.String(),
            //        Email = c.String(),
            //        WebSite = c.String(),
            //        DoesTicketNumberResetEveryYear = c.Boolean(nullable: false),
            //        IsTicketDuplicated = c.Boolean(nullable: false),
            //        Logo = c.String(),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //    })
            //    .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.Drivers",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //    })
            //    .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.Tickets",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        Number = c.Int(nullable: false),
            //        PrintDate = c.DateTime(),
            //        PrintTime = c.String(),
            //        WeighingId = c.Int(nullable: false),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //        Entity_Id = c.Int(),
            //        Product_Id = c.Int(),
            //        Tank_Id = c.Int(),
            //        Trailer_Id = c.Int(),
            //        Transporter_Id = c.Int(),
            //        Vehicle_Id = c.Int(),
            //        Driver_Id = c.Int(),
            //    })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Entities", t => t.Entity_Id)
            //    .ForeignKey("dbo.Products", t => t.Product_Id)
            //    .ForeignKey("dbo.Tanks", t => t.Tank_Id)
            //    .ForeignKey("dbo.Weighings", t => t.WeighingId, cascadeDelete: true)
            //    .ForeignKey("dbo.Trailers", t => t.Trailer_Id)
            //    .ForeignKey("dbo.Transporters", t => t.Transporter_Id)
            //    .ForeignKey("dbo.Vehicles", t => t.Vehicle_Id)
            //    .ForeignKey("dbo.Drivers", t => t.Driver_Id)
            //    .Index(t => t.WeighingId)
            //    .Index(t => t.Entity_Id)
            //    .Index(t => t.Product_Id)
            //    .Index(t => t.Tank_Id)
            //    .Index(t => t.Trailer_Id)
            //    .Index(t => t.Transporter_Id)
            //    .Index(t => t.Vehicle_Id)
            //    .Index(t => t.Driver_Id);

            //CreateTable(
            //    "dbo.Weighings",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        VehicleID = c.Int(nullable: false),
            //        TrailerID = c.Int(nullable: false),
            //        TankID = c.Int(nullable: false),
            //        ProductID = c.Int(nullable: false),
            //        LoadTemperature = c.String(),
            //        Batch = c.String(),
            //        Observations = c.String(),
            //        DriverID = c.Int(nullable: false),
            //        TransporterID = c.Int(nullable: false),
            //        EntityID = c.Int(nullable: false),
            //        DeliveryNote = c.String(),
            //        MaximumWeight = c.Double(nullable: false),
            //        Tare = c.Double(nullable: false),
            //        Gross = c.Double(nullable: false),
            //        Net = c.Double(nullable: false),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //    })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Drivers", t => t.DriverID, cascadeDelete: true)
            //    .ForeignKey("dbo.Entities", t => t.EntityID, cascadeDelete: true)
            //    .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
            //    .ForeignKey("dbo.Tanks", t => t.TankID, cascadeDelete: true)
            //    .ForeignKey("dbo.Trailers", t => t.TrailerID, cascadeDelete: true)
            //    .ForeignKey("dbo.Transporters", t => t.TransporterID, cascadeDelete: true)
            //    .ForeignKey("dbo.Vehicles", t => t.VehicleID, cascadeDelete: true)
            //    .Index(t => t.VehicleID)
            //    .Index(t => t.TrailerID)
            //    .Index(t => t.TankID)
            //    .Index(t => t.ProductID)
            //    .Index(t => t.DriverID)
            //    .Index(t => t.TransporterID)
            //    .Index(t => t.EntityID);

            //CreateTable(
            //    "dbo.Entities",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //    })
            //    .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.Products",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //    })
            //    .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.Tanks",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //    })
            //    .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.Trailers",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //    })
            //    .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.Transporters",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //    })
            //    .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.Vehicles",
            //    c => new
            //    {
            //        Id = c.Int(nullable: false, identity: true),
            //        CreatedDate = c.DateTime(nullable: false),
            //        ModifiedDate = c.DateTime(nullable: false),
            //        IsInactive = c.Boolean(nullable: false),
            //    })
            //    .PrimaryKey(t => t.Id);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Weighings", "VehicleID", "dbo.Vehicles");
            DropForeignKey("dbo.Tickets", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.Weighings", "TransporterID", "dbo.Transporters");
            DropForeignKey("dbo.Tickets", "Transporter_Id", "dbo.Transporters");
            DropForeignKey("dbo.Weighings", "TrailerID", "dbo.Trailers");
            DropForeignKey("dbo.Tickets", "Trailer_Id", "dbo.Trailers");
            DropForeignKey("dbo.Tickets", "WeighingId", "dbo.Weighings");
            DropForeignKey("dbo.Weighings", "TankID", "dbo.Tanks");
            DropForeignKey("dbo.Tickets", "Tank_Id", "dbo.Tanks");
            DropForeignKey("dbo.Weighings", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Tickets", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Weighings", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.Tickets", "Entity_Id", "dbo.Entities");
            DropForeignKey("dbo.Weighings", "DriverID", "dbo.Drivers");
            DropIndex("dbo.Weighings", new[] { "EntityID" });
            DropIndex("dbo.Weighings", new[] { "TransporterID" });
            DropIndex("dbo.Weighings", new[] { "DriverID" });
            DropIndex("dbo.Weighings", new[] { "ProductID" });
            DropIndex("dbo.Weighings", new[] { "TankID" });
            DropIndex("dbo.Weighings", new[] { "TrailerID" });
            DropIndex("dbo.Weighings", new[] { "VehicleID" });
            DropIndex("dbo.Tickets", new[] { "Driver_Id" });
            DropIndex("dbo.Tickets", new[] { "Vehicle_Id" });
            DropIndex("dbo.Tickets", new[] { "Transporter_Id" });
            DropIndex("dbo.Tickets", new[] { "Trailer_Id" });
            DropIndex("dbo.Tickets", new[] { "Tank_Id" });
            DropIndex("dbo.Tickets", new[] { "Product_Id" });
            DropIndex("dbo.Tickets", new[] { "Entity_Id" });
            DropIndex("dbo.Tickets", new[] { "WeighingId" });
            DropTable("dbo.Vehicles");
            DropTable("dbo.Transporters");
            DropTable("dbo.Trailers");
            DropTable("dbo.Tanks");
            DropTable("dbo.Products");
            DropTable("dbo.Entities");
            DropTable("dbo.Weighings");
            DropTable("dbo.Tickets");
            DropTable("dbo.Drivers");
            DropTable("dbo.Companies");
        }
    }
}
