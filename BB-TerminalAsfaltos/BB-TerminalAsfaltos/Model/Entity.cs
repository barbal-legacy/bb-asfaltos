﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BB_TerminalAsfaltos.Model
{
    public class Entity : EntityBase
    {
        public Entity()
        {
        }

        public string Name { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }

    }
}
