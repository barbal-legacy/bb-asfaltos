﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BB_TerminalAsfaltos.Model
{
    public class Ticket : EntityBase // AKA Reports
    {
        public Ticket()
        {
            UpdateIds();
        }
        // Número do Talão + Data + Hora
        
        public int? Number { get; set; } // + dd-mm-yyyy + hh:MM
        public DateTime? LastPrintDate { get; set; }
        public string LastPrintTime { get; set; }
        
        public int WeighingId { get; set; }

        public int? VehicleId { get; set; }
        public int? TrailerId { get; set; }
        public int? TankId { get; set; }
        public int? ProductId { get; set; }
        public string LoadTemperature { get; set; }
        public string Batch { get; set; }
        public string Observations { get; set; }

        public int? DriverId { get; set; }
        public int? TransporterId { get; set; }
        public int? EntityId { get; set; }
        public string DeliveryNote { get; set; }

        public string MaximumWeight { get; set; }
        public string Tare { get; set; }
        public string TareHour { get; set; }
        public string Gross { get; set; }
        public string Net { get; set; }

        public string TanksID { get; set; }

        public virtual Weighing Weighing { get; set; }
        public virtual Vehicle Vehicle { get; set; }
        public virtual Trailer Trailer { get; set; }
        public virtual Product Product { get; set; }
        public virtual Driver Driver { get; set; }
        public virtual Transporter Transporter { get; set; }
        public virtual Entity Entity { get; set; }
        public virtual Tank Tank { get; set; }
        //public virtual List<Tank> Tanks { get; set; }

        public Ticket(Weighing weighing, int previousNumber)
        {
            this.Number            = previousNumber;
            this.WeighingId        = weighing.Id;
            this.VehicleId         = weighing.VehicleID;
            this.TrailerId         = weighing.TrailerID;
            this.TankId            = weighing.TankID;
            this.ProductId         = weighing.ProductID;
            this.LoadTemperature   = weighing.LoadTemperature;
            this.Batch             = weighing.Batch;
            this.Observations      = weighing.Observations;
            this.DriverId          = weighing.DriverID;
            this.TransporterId     = weighing.TransporterID;
            this.EntityId          = weighing.EntityID;
            this.DeliveryNote      = weighing.DeliveryNote;
            this.MaximumWeight     = weighing.MaximumWeight;
            this.Tare              = weighing.Tare;
            this.TareHour          = weighing.TareHour;
            this.Gross             = weighing.Gross;
            this.Net               = weighing.Net;
            this.TanksID           = weighing.TanksID;
        }

        public void UpdateIds()
        {
            using (DataModel Context = new DataModel())
            {
                try
                {
                    Weighing weighing = new Weighing();
                    if (WeighingId != null && WeighingId > 0)
                        weighing = Context.Weighings.Find(WeighingId);

                    Vehicle = Context.Vehicles.Find(this.VehicleId);
                    Trailer = Context.Trailers.Find(this.TrailerId);
                    Product = Context.Products.Find(this.ProductId);
                    Driver = Context.Drivers.Find(this.DriverId);
                    Transporter = Context.Transporters.Find(this.TransporterId);
                    Entity = Context.Entities.Find(this.EntityId);

                    if (!String.IsNullOrEmpty(weighing.TanksID))
                    { 
                        if (weighing.CountTanks(weighing.TanksID) > 1)
                        {
                            List<Tank> localTanksList = weighing.TanksConverter(weighing.TanksID);
                            Tank = localTanksList.First();
                        }
                        else if (weighing.CountTanks(weighing.TanksID) == 1)
                        {
                            Tank = Context.Tanks.Find(this.TankId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message); // TODO: Arranjar uma descição
                }
            }
        }

        //public virtual ObservableCollection<Weighing> Weighing { get; set; }
    }
}
