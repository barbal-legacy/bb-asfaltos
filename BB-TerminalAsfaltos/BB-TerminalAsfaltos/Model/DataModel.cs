namespace BB_TerminalAsfaltos.Model
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DataModel : DbContext
    {
        // Your context has been configured to use a 'DataModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'DBMigrationTest2.Model.DataModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'DataModel' 
        // connection string in the application configuration file.
        public DataModel()
            : base("name=DataModel")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Company> Companies         { get; set; }
        public virtual DbSet<Driver> Drivers            { get; set; }
        public virtual DbSet<Entity> Entities           { get; set; }
        public virtual DbSet<Product> Products          { get; set; }
        public virtual DbSet<Tank> Tanks                { get; set; }
        public virtual DbSet<Ticket> Tickets            { get; set; }
        public virtual DbSet<Trailer> Trailers          { get; set; }
        public virtual DbSet<Transporter> Transporters  { get; set; }
        public virtual DbSet<Vehicle> Vehicles          { get; set; }
        public virtual DbSet<Weighing> Weighings        { get; set; }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}