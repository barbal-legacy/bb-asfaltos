﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BB_TerminalAsfaltos.Model
{
    public class Weighing : EntityBase
    {
        public Weighing()
        {
        }
        
        public int? VehicleID                    { get; set; }
        public int? TrailerID                    { get; set; }
        public int? TankID                       { get; set; }
        public int? ProductID                    { get; set; }
        public string LoadTemperature           { get; set; }
        public string Batch                     { get; set; }
        public string Observations              { get; set; }

        public int? DriverID                     { get; set; }
        public int? TransporterID                { get; set; }
        public int? EntityID                     { get; set; }
        public string DeliveryNote              { get; set; }

        public string MaximumWeight             { get; set; }
        public string Tare                      { get; set; }
        public string TareHour                  { get; set; }
        public string Gross                     { get; set; }
        public string Net                       { get; set; }
        public bool manualTare                  { get; set; }
        public bool bypassed                    { get; set; }

        public string TanksID                   { get; set; }

        public bool IsFinished                  { get; set; }

        public virtual Vehicle Vehicle          { get; set; }
        public virtual Trailer Trailer          { get; set; }
        public virtual Tank Tank                { get; set; }
        public virtual Product Product          { get; set; }
        public virtual Driver Driver            { get; set; }
        public virtual Transporter Transporter  { get; set; }
        public virtual Entity Entity            { get; set; }

        public string GetTime()
        {
            return (CreatedDate.Hour + ":" + CreatedDate.Minute);
        }

        public string GetDate()
        {
            return (CreatedDate.Day + "-" + CreatedDate.Month + "-" + CreatedDate.Year);
        }

        /// <summary>
        /// Function to count the number of tanks in <seealso>TanksID</seealso>
        /// </summary>
        /// <returns>number of ';' found in <seealso>TanksID</seealso> </returns>
        private int CountTanks()
        {
            int count = 0;
            foreach (char ch in TanksID)
            {
                if (ch == ';')
                    count++;
            }
            return count;
        }

        /// <summary>
        /// Function to count the number of tanks in a string
        /// </summary>
        /// <param name="tanks">String containing tanks.Name or Tanks.Id, separated by ';' </param>
        /// <returns>number of ';' found </returns>
        public int CountTanks(string tanks)
        {
            int count = 0;
            foreach (char ch in tanks)
            {
                if (ch == ';')
                    count++;
            }
            return count;
        }
        
        /// <summary>
        /// Function that returns a list of Tank 
        /// </summary>
        /// <param name="tanksList">String containing a tank.Id list separated by ';' </param>
        /// <returns>Returns a list of Tank, returns null if the list is empty </returns>
        public List<Tank> TanksConverter(string tanksList)
        {
            if (tanksList == "")
                return null;

            using (DataModel Context = new DataModel())
            {
                try
                {
                    string[] separateStrings = { ",", ";" };
                    string[] tmpList;
                    string lastCharacterString = tanksList.Substring(tanksList.Length-1);
                    List<Tank> tmpTanksList = new List<Tank>();
                    if (lastCharacterString == ";")
                    {
                        tmpList = tanksList.Split(separateStrings, System.StringSplitOptions.RemoveEmptyEntries);
                    }
                    else
                    {
                        tmpList = ConvertTankNameToId(tanksList).Split(separateStrings, System.StringSplitOptions.RemoveEmptyEntries);
                    }

                    foreach (var tankId in tmpList)
                    {
                        if (String.IsNullOrEmpty(tankId))
                            break;

                        tmpTanksList.Add(Context.Tanks.Find(int.Parse(tankId)));
                    }
                    return tmpTanksList;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message); // TODO: Arranjar uma descição
                }
            }
            return null;
        }

        /// <summary>
        /// Function that converts the received tank names to the corresponding ID
        /// </summary>
        /// <param name="tanksList">List of Tank.Name separated by ';' </param>
        /// <returns>Returns a string with the Tank.Id separated by';' </returns>
        public string ConvertTankNameToId(string tanksList)
        {
            string tmpTankIds = "";
            if (tanksList == "")
                return tmpTankIds;
            using (DataModel Context = new DataModel())
            {
                try
                {
                    string[] separateStrings = { ",", ";" };
                    List<Tank> tmpTanksList = Context.Tanks.Where(x => !x.IsInactive).ToList();
                    string[] tmpList = tanksList.Split(separateStrings, System.StringSplitOptions.RemoveEmptyEntries);
                    foreach (var tankName in tmpList)
                    {
                        foreach (Tank tank in tmpTanksList)
                        {
                            if (tankName.ToUpper() == tank.Name.ToUpper())
                                tmpTankIds += tank.Id.ToString() + ";";
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message); // TODO: Arranjar uma descição
                }
            }
            return tmpTankIds;
        }

        /// <summary>
        /// Function that converts the received tank IDs to the corresponding names
        /// </summary>
        /// <param name="tanksList">List of Tank.Id separated by ';' </param>
        /// <returns>Returns a string with the Tank.Name separated by';' </returns>
        public string ConvertTankIdToName(string tanksList)
        {
            //Register vem aqui
            string tmpTankNames = "";
            if (tanksList == "")
                return tmpTankNames;

            List<Tank> tmpTankList = TanksConverter(tanksList);
            foreach (Tank tank in tmpTankList)
            {
                tmpTankNames += tank.Name + ",";
            }
            return tmpTankNames;
        }

        /// <summary>
        /// Refresh Object to Update List according to its ID
        /// </summary>
        /// <param name="Context">DataModel Context to grant the access to the DataBase</param>
        public void RefreshObject(DataModel Context)
        {
            if (TankID.HasValue)
                Tank = Context.Tanks.Find(TankID);

            if (VehicleID.HasValue)
                Vehicle = Context.Vehicles.Find(VehicleID);

            if (TrailerID.HasValue)
                Trailer = Context.Trailers.Find(TrailerID);

            if (ProductID.HasValue)
                Product = Context.Products.Find(ProductID);

            if (DriverID.HasValue)
                Driver = Context.Drivers.Find(DriverID);

            if (TransporterID.HasValue)
                Transporter = Context.Transporters.Find(TransporterID);

            if (EntityID.HasValue)
                Entity = Context.Entities.Find(EntityID);
        }
    }
}
