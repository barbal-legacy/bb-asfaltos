﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BB_TerminalAsfaltos.Model
{
    public class Driver : EntityBase
    {
        public Driver()
        {
            DateID = DateTime.Now;
            DateADR = DateTime.Now;
            DateDriversLicense = DateTime.Now;
            DateSecurityInduction = DateTime.Now;
        }

        public string Name { get; set; }
        public DateTime DateID { get; set; }
        public DateTime DateADR { get; set; }
        public DateTime DateDriversLicense { get; set; }
        public DateTime DateSecurityInduction { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
