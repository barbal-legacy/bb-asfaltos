﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BB_TerminalAsfaltos.Model
{
    public class Vehicle : EntityBase
    {
        public Vehicle()
        {
            DateInsurance = DateTime.Now;
            DateADR = DateTime.Now;
        }

        public string LicensePlate { get; set; }
        public DateTime DateInsurance { get; set; }
        public DateTime DateADR { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
