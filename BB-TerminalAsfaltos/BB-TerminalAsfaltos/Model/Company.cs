﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace BB_TerminalAsfaltos.Model
{
    public class Company : EntityBase
    {
        private string _logo;

        public string Name { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string TaxPayerNumber { get; set; }
        public string ContactNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public bool DoesTicketNumberResetEveryYear { get; set; }
        public bool IsTicketDuplicated { get; set; }
        public string Logo
        {
            get
            {
                return _logo;
            }
            set
            {
                _logo = value;
                if (value == null)
                {
                    return;
                }
                byte[] imgBytes = Convert.FromBase64String(value);
                BitmapImage bitmapImage = new BitmapImage();
                MemoryStream ms = new MemoryStream(imgBytes);
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = ms;
                bitmapImage.EndInit();
                ParsedLogo = bitmapImage;
                NotifyPropertyChanged("ParsedLogo");
            }
        }

        [NotMapped]
        public BitmapImage ParsedLogo { get; set; }
    }
}
