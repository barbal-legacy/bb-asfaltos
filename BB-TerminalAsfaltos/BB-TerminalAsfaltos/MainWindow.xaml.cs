﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using static BB_TerminalAsfaltos.Classes.Generals;
using System.Windows.Shapes;
using Premise.IoT;

namespace BB_TerminalAsfaltos
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Frame mainWindowFrame;
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                this.Width = SystemParameters.PrimaryScreenWidth * 0.95;

                mainWindowFrame = MainWindowFrame;
            }
            catch (Exception ex)
            {
                MessageBox.Show(FindTranslation("ThrowApplicationStartupFailed"), FindTranslation("Warning"), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Frame_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            try
            {
                Frame f = sender as Frame;
                f.NavigationService.RemoveBackEntry();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); // TODO: Alterar msg de erro
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show(FindTranslation("AreYouSureWantToLeave"), FindTranslation("Leave"), MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
