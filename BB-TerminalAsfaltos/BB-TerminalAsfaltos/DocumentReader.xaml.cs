﻿namespace BB_TerminalAsfaltos
{
    using System;
    using System.Collections.Generic;
    //using System.Drawing.Printing;
    using System.Linq;
    using System.Printing;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Media;
    using System.Xml.Linq;

    public partial class DocumentReader : Window
    {
        //private readonly EFModel context = new EFModel();
        private int number_to_print;
        public DocumentReader(Page printPage, int NumberToPrint = 1)
        {
            InitializeComponent();
            LoadCmbPrinters();
            //doc.PageHeight = Double.NaN;
            //doc.PageWidth = Double.NaN;
            number_to_print = NumberToPrint;
            doc.PageHeight = printPage.Height;
            doc.PageWidth = printPage.Width;
            printingDocument.Height = printPage.Height;
            printingDocument.Width = printPage.Width;
            printingDocument.Content = printPage;
            
            flowDoc.Document.ColumnGap = 0;
            flowDoc.Document.ColumnWidth = printPage.Width;
            flowDoc.UpdateLayout();
        }

        private void LoadCmbPrinters()
        {
            cmbPrinter.Items.Clear();
            foreach (PrintQueue printer in new LocalPrintServer().GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local,
                EnumeratedPrintQueueTypes.Connections }))
            {
                if (!printer.IsNotAvailable)
                    cmbPrinter.Items.Add(printer.Name);
            }
            cmbPrinter.SelectedItem = LocalPrintServer.GetDefaultPrintQueue().Name;
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < number_to_print; i++)
            {
                PrintDialog pd = new PrintDialog();
                IDocumentPaginatorSource idocument = doc as IDocumentPaginatorSource;
                //idocument.DocumentPaginator.PageSize = new Size((int)printingDocument.ActualWidth, double.IsNaN(printingDocument.Height) ? 830 : printingDocument.Height);
                idocument.DocumentPaginator.PageSize = new Size((double)doc.PageWidth, double.IsNaN(printingDocument.Height) ? 830 : doc.PageHeight);

                pd.PrintQueue = (from PrintQueue printer in new LocalPrintServer().GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections }).ToList()
                                 where printer.Name == cmbPrinter.SelectedItem.ToString()
                                 select printer).SingleOrDefault();

                pd.PrintDocument(idocument.DocumentPaginator, "Printing Flow Document...");
            }
        }
    }
}
