﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BB_TerminalAsfaltos.Views
{
    /// <summary>
    /// Interaction logic for PageListingsView.xaml
    /// </summary>
    public partial class PageListingsView : Page
    {
        public PageListingsView()
        {
            InitializeComponent();
            ticketsDataGridPreview = ItemsDataGrid;
        }

        public static DataGrid ticketsDataGridPreview;

        //public static void TextBoxChangeText(string text) tbName.Text = text;
    }
}
