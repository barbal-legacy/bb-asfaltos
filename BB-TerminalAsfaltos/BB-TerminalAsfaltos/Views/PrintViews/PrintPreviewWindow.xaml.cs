﻿using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.ViewModels;
using BB_TerminalAsfaltos.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Xps.Packaging;
using System.Threading;
using System.Windows.Xps;

namespace BB_TerminalAsfaltos.Views.PrintViews
{
    /// <summary>
    /// Interaction logic for PrintPreviewWindow.xaml
    /// </summary>
    public partial class PrintPreviewWindow : Window, INotifyPropertyChanged
    {
        public static string path;
        private string _myPath;
        // Path where the document is storedpublic static string docPath;
        public static string docPath;
        public static XpsDocument xpsDoc;
        public static XpsDocumentWriter xpsWriter;
        private FixedDocumentSequence _document;

        public event PropertyChangedEventHandler PropertyChanged;

        public string myPath
        {
            get { return _myPath; }
            set
            {
                if (File.Open(path, FileMode.Open).CanRead)
                {
                    _myPath = path;
                    NotifyPropertyChanged("myPath");
                }
            }
        }

        public PrintPreviewWindow(Ticket ticket, FixedDocumentSequence document)
        {
            Ticket _ticket = ticket;
            InitializeComponent();
            this.Height = SystemParameters.PrimaryScreenHeight * 0.95;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            if (document != null)
            {
                Preview.Document = document;
            }
        }

        /// <summary>
        /// Print Button Click Event
        /// </summary>
        /// <param name="sender">Object Information</param>
        /// <param name="e">State Information and Event data</param>
        private void InvokePrint(object sender, RoutedEventArgs e)
        {
            try
            {
                // Close Preview Window
                this.Close();

                // Create the print dialog object and set options
                PrintDialog pDialog = new PrintDialog();
                pDialog.PageRangeSelection = PageRangeSelection.AllPages;
                pDialog.UserPageRangeEnabled = true;

                // Display the dialog. This returns true if the user presses the print button
                Nullable<Boolean> print = pDialog.ShowDialog();
                if (print == true)
                {
                    XpsDocument xpsDocument = new XpsDocument(docPath, FileAccess.Read);
                    FixedDocumentSequence fixedDocumentSequence = xpsDocument.GetFixedDocumentSequence();
                    pDialog.PrintDocument(fixedDocumentSequence.DocumentPaginator, "Test print job"); // TODO: Ver se aparece algures no software

                    // Close XPS Document to Prevent File Lock
                    xpsDocument.Close();

                    // if the package is not removed from the Package store, we won't be able to re-open the same file again later
                    //(due to System.IO.Packaging.PackageStore's Package store/caching rather than because of any file locks)
                    System.IO.Packaging.PackageStore.RemovePackage(xpsDocument.Uri);

                    // Deletes the temporary file after closing the preview window
                    File.Delete(docPath);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                else
                {
                    File.Delete(docPath);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("InvokePrint\n" + ex.Message);
            }
            
        }

        /// <summary>
        /// Closed Public Method to Allow Window to be Closed From Other Locations
        /// </summary>
        public static void InvokeWindowClosing()
        {
            Application.Current.Windows.OfType<PrintPreviewWindow>().FirstOrDefault().Close();
        }

        private void InvokeExport(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();

        }

        /// <summary>
        /// Performs the File deletion after closing the preview window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            
        }

        protected void NotifyPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }

}