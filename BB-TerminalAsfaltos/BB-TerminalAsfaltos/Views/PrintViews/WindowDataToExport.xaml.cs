﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Xps.Packaging;
using System.Xml;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Microsoft.Win32;
using static BB_TerminalAsfaltos.Classes.Generals;
using System.Windows.Data;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Controls.Primitives;

namespace BB_TerminalAsfaltos.Views.PrintViews
{
    /// <summary>
    /// Interaction logic for PageDataToExportrt.xaml
    /// </summary>
    public partial class WindowDataToExport : Window
    {
        public static DataGrid datagridToExport;
        Excel.Application App; //Excel application
        Excel.Workbook Workbook; //Excel opened file
        Excel.Worksheet Worksheet; //Excel sheet page of the Workbook
        ObservableCollection<DataGridColumn> gridHeaders;
        
        //default missing value for Excel parameters
        object misValue = System.Reflection.Missing.Value;

        //save file window
        SaveFileDialog save = new SaveFileDialog();

        public WindowDataToExport(ObservableCollection<dynamic> gridContent, ObservableCollection<DataGridColumn> ColumnsList)
        {
            InitializeComponent();
            this.Width = SystemParameters.PrimaryScreenWidth * 0.95;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            //aux to remove undesired columns
            List<DataGridColumn> desiredHeaders = new List<DataGridColumn>();

            for(int position = 0; position < ColumnsList.Count; position++)
                if (ColumnsList[position].GetType() == typeof(DataGridTextColumn))
                    desiredHeaders.Add(ColumnsList[position]);

            //as WPF doesn't allow Duplicate headers, we had to "re-create" the headers again
            gridHeaders = new ObservableCollection<DataGridColumn>();

            foreach (DataGridTextColumn column in desiredHeaders)
                gridHeaders.Add(
                    new DataGridTextColumn
                    {
                        Binding = new Binding(column.SortMemberPath.ToString()),
                        Header = column.Header.ToString(),
                        ElementStyle = (Style)Application.Current.TryFindResource("AlignCenter")
                    });

            //get each header and add it to our data preview
            foreach (DataGridColumn column in gridHeaders)
                DataGridExportPreview.Columns.Add(column);

            //DataGrid alraedy has a empty row, so we have to clear it before adding the new items
            DataGridExportPreview.Items.Clear();

            //get each item and add it to our data preview
            foreach (Ticket item in gridContent)
                DataGridExportPreview.Items.Add(item);
        }

        /// <summary>
        /// Export Click to open Windows window for saving in xls type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Export_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Get all Excel Process Id created by the Export Method
                int[] excelProcessId;
                //open the Excel application 
                App = new Excel.Application();
                //create a open file to save the worksheets 
                Workbook = App.Workbooks.Add(misValue);
                //create a sheet inside the open file
                Worksheet = (Excel.Worksheet)Workbook.Worksheets.get_Item(1);

                //aux datagrid to work cells
                datagridToExport = DataGridExportPreview;

                //set savebox properties
                save.Title = FindTranslation("TitleExport");
                save.Filter = "Excel File (*.xls) |*.xls";
                var dlgResult = save.ShowDialog();

                //save file in path defined in Save dialog
                if (dlgResult.Value == true)
                {
                    Workbook.SaveAs(save.FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue,
                    Excel.XlSaveAsAccessMode.xlExclusive, misValue, true, misValue, misValue, misValue);

                    if (App != null)
                    {
                        if (Workbook != null)
                        {
                            if (Worksheet != null)
                            {
                                try
                                {
                                    //run trough each column to fill data 
                                    int i = 1;
                                    foreach (DataGridTextColumn dgColumn in datagridToExport.Columns)
                                    {
                                        //fill the headers on first line
                                        Worksheet.Cells[1, i] = dgColumn.Header;
                                        //run trough each row, select the correspondent cell [row, column] and fill it on Excel Worksheet
                                        for (int j = 0; j < datagridToExport.Items.Count; j++)
                                        {
                                            datagridToExport.SelectedIndex = j;
                                            DataGridRow row = (DataGridRow)datagridToExport.ItemContainerGenerator.ContainerFromItem(datagridToExport.SelectedItem);
                                            DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(row);

                                            if (presenter == null)
                                            {
                                                datagridToExport.ScrollIntoView(row, datagridToExport.Columns[i - 1]);
                                                presenter = GetVisualChild<DataGridCellsPresenter>(row);
                                            }

                                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(i - 1);
                                            TextBlock cellInfo = cell.Content as TextBlock;
                                            //fill the info starting from the second row, below the header
                                            Worksheet.Cells[j + 2, i] = cellInfo.Text.ToString();
                                        }
                                        i++;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(FindTranslation("UnsuccessExport"), FindTranslation("TitleExport"), MessageBoxButton.OK, MessageBoxImage.Error);
                                    //COM is a specified object for the Interop +info https://docs.microsoft.com/en-us/dotnet/standard/native-interop/cominterop
                                    //Marshal.ReleaseComObject() clears the memory allocated for the specified COM object (ends task/service) +info https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.marshal.releasecomobject?view=net-6.0
                                    Marshal.ReleaseComObject(Worksheet);
                                    Marshal.FinalReleaseComObject(Worksheet);
                                    App.Quit();
                                    GetAndKillExcelProcess(App);
                                    return;
                                }

                                //COM is a specified object for the Interop +info https://docs.microsoft.com/en-us/dotnet/standard/native-interop/cominterop
                                //Marshal.ReleaseComObject() clears the memory allocated for the specified COM object (ends task/service) +info https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.marshal.releasecomobject?view=net-6.0
                                Marshal.ReleaseComObject(Worksheet);
                                Marshal.FinalReleaseComObject(Worksheet);
                            }
                        }
                        // Close Excel Application, free object from memory and kill process
                        Workbook.Save();
                        App.Quit();
                        GetAndKillExcelProcess(App);
                    }

                    MessageBoxResult resultWarning = MessageBox.Show(FindTranslation("SuccessExport"), FindTranslation("TitleExport"), MessageBoxButton.OK, MessageBoxImage.Information);
                    if (resultWarning == MessageBoxResult.OK)
                        // close window after OK click 
                        Application.Current.Windows.OfType<WindowDataToExport>().FirstOrDefault().Close();
                }
            }
            catch (Exception ex)
            {
                try
                {
                    //alternative export method for when there's no Excel installed
                    AlternativeExport();
                }
                catch
                {
                    MessageBox.Show(FindTranslation("UnsuccessExport"), FindTranslation("TitleExport"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        [DllImport("user32.dll")] // TODO : Lembrar de verificar se no Setup esta DLL tem de ir junto
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

        /// <summary>
        /// Gets the Excell Process Id and terminates it
        /// </summary>
        /// <param name="excelApp">Excel Application to get id and kill</param>
        public void GetAndKillExcelProcess(Excel.Application excelApp)
        {
            int id;
            GetWindowThreadProcessId(excelApp.Hwnd, out id);
            Process.GetProcessById(id).Kill();
            Marshal.ReleaseComObject(App);
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        // it was necessary to be able to use DataGridCellsPresenter
        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }

        public void AlternativeExport()
        {
            try
            {
                StreamWriter writer;
                //aux datagrid to work cells
                datagridToExport = DataGridExportPreview;

                //set savebox properties
                save.Title = FindTranslation("TitleExport");
                save.Filter = "Excel File (*.xls) |*.xls";
                var dlgResult = save.ShowDialog();

                //save file in path defined in Save dialog
                if (dlgResult.Value == true)
                {
                    writer = new StreamWriter(save.FileName);

                    // select grid cells, copy values to export, unselect cells 
                    datagridToExport.SelectAllCells();
                    datagridToExport.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                    ApplicationCommands.Copy.Execute(null, datagridToExport);
                    String resultat = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
                    String result = (string)Clipboard.GetData(DataFormats.Text);
                    datagridToExport.UnselectAllCells();

                    // open and write the clipboard info in the file created by Interop.Excel
                    writer.WriteLine(result.Replace(',', ' '));
                    writer.Close();

                    MessageBoxResult resultWarning = MessageBox.Show(FindTranslation("SuccessExport"), FindTranslation("TitleExport"), MessageBoxButton.OK, MessageBoxImage.Information);
                    if (resultWarning == MessageBoxResult.OK)
                        // close window after OK click 
                        Application.Current.Windows.OfType<WindowDataToExport>().FirstOrDefault().Close();
                }
            }
            catch
            {
                throw new Exception();
            }
        }            
    }
}
