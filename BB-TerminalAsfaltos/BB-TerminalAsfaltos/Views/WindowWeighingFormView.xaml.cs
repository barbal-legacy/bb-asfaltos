﻿using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static BB_TerminalAsfaltos.Classes.Generals;

namespace BB_TerminalAsfaltos.Views
{
    /// <summary>
    /// Interaction logic for WindowWeighingFormView.xaml
    /// </summary>
    public partial class WindowWeighingFormView : Window
    {
        public static int WhereWindow = 0;
        
        public WindowWeighingFormView()
        {
            InitializeComponent();
            this.Width = SystemParameters.PrimaryScreenWidth * 0.95;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        public static void InvokeWindowClosing()
        {
            Application.Current.Windows.OfType<WindowWeighingFormView>().FirstOrDefault().Close();
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (WhereWindow == 0)
            { 
                MessageBoxResult result = MessageBox.Show(FindTranslation("AreYouSureWantToLeave"), FindTranslation("Leave"), MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {   
                    WindowWeighingFormViewModel.Dispose();

                    //Console.WriteLine("(Yes)IsConnectionOpenned: " + WindowWeighingFormViewModel.leitor.DeviceCommunications.IsConnectionOpenned.ToString());
                } 
                else if (result == MessageBoxResult.No)
                {
                    e.Cancel = true;
                    //Console.WriteLine("(No)IsConnectionOpenned: " + WindowWeighingFormViewModel.leitor.DeviceCommunications.IsConnectionOpenned.ToString());
                }
            }
            else if (WhereWindow == 1)
            {
                WindowWeighingFormViewModel.Dispose();
            }
        }

        private void CmbVehicle_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbVehicle.IsDropDownOpen = true;
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void CmbTrailer_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbTrailer.IsDropDownOpen = true;
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void TextBoxGuide_LostFocus(object sender, RoutedEventArgs e)
        {
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void CmbProduct_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbProduct.IsDropDownOpen = true;
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void TextBoxLoadTemperature_LostFocus(object sender, RoutedEventArgs e)
        {
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void TextBoxBatch_LostFocus(object sender, RoutedEventArgs e)
        {
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void ObservationsBox_LostFocus(object sender, RoutedEventArgs e)
        {
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void CmbDriver_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbDriver.IsDropDownOpen = true;
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void CmbTransporter_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbTransporter.IsDropDownOpen = true;
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void CmbEntity_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbEntity.IsDropDownOpen = true;
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void CmbTank_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbTank.IsDropDownOpen = true;
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }

        private void TbMaxWeight_LostFocus(object sender, RoutedEventArgs e)
        {
            WindowWeighingFormViewModel.RegisterTemporaryWeight();
        }
    }
}
