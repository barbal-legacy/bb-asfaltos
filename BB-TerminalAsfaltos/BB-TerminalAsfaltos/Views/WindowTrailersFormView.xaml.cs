﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BB_TerminalAsfaltos.ViewModels
{
    /// <summary>
    /// Interaction logic for WindowTrailersFormView.xaml
    /// </summary>
    public partial class WindowTrailersFormView : Window
    {
        

        public WindowTrailersFormView()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewKeydown(object sender, KeyEventArgs e)
        {
            var box = (sender as TextBox);
            var text = box.Text;
            var caret = box.CaretIndex;

            if (e.Key == Key.Space)
            {
                var newValue = "-";
                //Update the TextBox' text..
                box.Text = text.Insert(caret, newValue);
                //..move the caret accordingly..
                box.CaretIndex = caret + newValue.Length;
                //..and make sure the keystroke isn't handled again by the TextBox itself:
                e.Handled = true;
            }
        }

        string pattern = @"^([A-Za-z0-9\-])+$";

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            var box = (sender as TextBox);
            var text = box.Text;
            var caret = box.CaretIndex;

            if (IsTextAllowed(e.TextComposition.Text))
            {
                var newValue = "";

                //Update the TextBox' text..
                box.Text = text.Insert(caret, newValue);
                //..move the caret accordingly..
                box.CaretIndex = caret + newValue.Length;
                //..and make sure the keystroke isn't handled again by the TextBox itself:
                e.Handled = true;
            }

        }

        private bool IsTextAllowed(string text)
        {
            return !Regex.IsMatch(text, pattern);
        }
    }
}
