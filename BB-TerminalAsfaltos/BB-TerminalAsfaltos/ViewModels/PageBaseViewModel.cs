﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using BB_TerminalAsfaltos.Views.PrintViews;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Xml;
using static BB_TerminalAsfaltos.Classes.Generals;
using BB_TerminalAsfaltos.Views.PrintViews;
using System.Windows.Data;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace BB_TerminalAsfaltos.ViewModels
{
    class PageBaseViewModel : INotifyPropertyChanged
    {
        private string _filter;
        private string _vehicleLicensePlate;
        private string _trailerLicensePlate;
        private string _driverName;
        private string _entityName;
        private string _productName;
        private string _tankName;
        private string _transporterName;

        //// Preview Documents
        //private string docPath;
        private XpsDocument xpsDoc;

        private DateTime _dateFrom = DateTime.Now.AddMonths(-2);
        private DateTime _dateTo = DateTime.Now;

        public event PropertyChangedEventHandler PropertyChanged;
        public Type ObjectType { get; set; }
        public Type WindowType { get; set; }
        public Type ViewModelType { get; set; }
        public string DataGridName { get; set; }
        
        public ObservableCollection<DataGridColumn> ColumnsList { get; set; }
        public ObservableCollection<dynamic> ItemsCollection { get; set; }
        public ICollectionView CollectionView { get; protected set; }

        public List<Transporter> TransporterList { get; set; }

        //Button Events 
        public ClickedCommand<object> AddCommand { get; set; }
        public ClickedCommand<object> EditCommand { get; set; }
        public ClickedCommand<object> DeleteCommand { get; set; }
        public ClickedCommand<object> PrintCommand { get; set; }
        public ClickedCommand<object> PreviewCommand { get; set; }
        public ClickedCommand<object> ExportCommand { get; set; }

        public Visibility ExportButtonVisibility { get; set; } = Visibility.Collapsed;
        public Visibility AddButtonVisibility { get; set; } = Visibility.Visible;
        public Visibility DateFilterVisibility { get; set; } = Visibility.Visible;
        public Visibility DeleteButtonVisibility { get; set; } = Visibility.Visible;

        #region Bindings
        public string Filter
        {
            get { return _filter; }
            set
            {
                if (value != _filter)
                {
                    _filter = value;
                    MyView.Refresh();
                    NotifyPropertyChanged("Filter");
                }
            }
        }

        public DateTime DateFrom
        {
            get { return _dateFrom;  }
            set
            {
                if (value != _dateFrom)
                {
                    if (value <= _dateTo)
                    { 
                        _dateFrom = value;
                        MyView.Refresh();
                        NotifyPropertyChanged("DateFrom");
                        //NotifyPropertyChanged("Filter");
                        RefreshList();
                    }
                    else
                    {
                        MessageBox.Show(Generals.FindTranslation("DateFromOutOfBounds"));
                    }
                }
            }
        }

        public DateTime DateTo
        {
            get { return _dateTo; }
            set
            {
                if (value != _dateTo)
                {
                    if (value >= _dateFrom)
                    {
                        _dateTo = value;
                        MyView.Refresh();
                        //NotifyPropertyChanged("Filter");
                        NotifyPropertyChanged("DateTo");
                        RefreshList();
                    }
                    else
                    {
                        MessageBox.Show(Generals.FindTranslation("DateToOutOfBounds"));
                    }
                }
            }
        }
        #endregion

        public ICollectionView MyView { get; protected set; }
        
        public PageBaseViewModel(string DataGridName, Type ObjectType, Type WindowType, Type ViewModelType)
        {
            this.DataGridName = DataGridName;
            this.ObjectType = ObjectType;
            this.WindowType = WindowType;
            this.ViewModelType = ViewModelType;
            

            AddCommand = new ClickedCommand<object>(AddCommandAction);
            EditCommand = new ClickedCommand<object>(EditCommandAction);
            DeleteCommand = new ClickedCommand<object>(DeleteCommandAction);
            PrintCommand = new ClickedCommand<object>(PrintCommandAction);
            PreviewCommand = new ClickedCommand<object>(PreviewCommandAction); 
            ExportCommand = new ClickedCommand<object>(ExportCommandAction);
            
            RefreshList();
        }

        /// <summary>
        /// Functionality to preview export list before export
        /// </summary>
        /// <param name="obj">Object received with information to pass for the ticket page</param>
        private void ExportCommandAction(object obj)
        {
            if (this.DataGridName == Generals.FindTranslation("Tickets"))
            {
                using (DataModel Context = new DataModel())
                {
                    //get item list with date filters
                    DateTime localDateTo = DateTo.AddDays(1);

                    ItemsCollection = new ObservableCollection<dynamic>(
                        Context.Tickets
                        .Include("Driver")
                        .Include("Entity")
                        .Include("Product")
                        .Include("Tank")
                        .Include("Trailer")
                        .Include("Transporter")
                        .Include("Vehicle")
                        .Include("Weighing")
                        .OrderByDescending(x => x.Id).Where(x => x.CreatedDate >= DateFrom && x.CreatedDate <= localDateTo && !x.IsInactive).ToDynamicList<dynamic>());
                }

                //open new window WindowDataToExport and send filtered items list and grid headers
                Window DetailWindow = Activator.CreateInstance(typeof(WindowDataToExport), ItemsCollection, ColumnsList) as WindowDataToExport;
                DetailWindow.DataContext = Activator.CreateInstance(ViewModelType, 0);

                DetailWindow.ShowDialog();
            }
            return;
        }

        /// <summary>
        /// Functionality to preview ticket before printing
        /// </summary>
        /// <param name="obj">Object received with information to pass for the ticket page</param>
        public void PreviewCommandAction(object obj)
        {
            if (obj == null)
            {
                MessageBox.Show(FindTranslation("NoItemSelected"));
                return;
            }
            var Eb = obj as EntityBase;
            if (Eb is Ticket)
            {
                Ticket Ticket = Eb as Ticket;
                try
                {
                    // Verify all xps files created but not deleted
                    verifyXpsFilesAndDelete();

                    // Path String to Create XPS File
                    string dateNowString = DateTime.Now.ToString("ddMMyyyyHHmmssffff");
                    PrintPreviewWindow.docPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"XPSDocumentPreview" + dateNowString + ".xps");

                    // Visual creation based on View to show on DocumentViewer on XAML As Binding and Saving the DataContext of the ticket on Database
                    Visual pageToPreview = new Views.PrintViews.A4Plexus { DataContext = new GeneralPrintingViewModel(Ticket) };

                    using (PrintPreviewWindow.xpsDoc = new XpsDocument(PrintPreviewWindow.docPath, FileAccess.ReadWrite))
                    {
                        // Initializing and creation of XpsDocument    
                        PrintPreviewWindow.xpsWriter = XpsDocument.CreateXpsDocumentWriter(PrintPreviewWindow.xpsDoc);

                        // Write View on Xps Document
                        PrintPreviewWindow.xpsWriter.Write(pageToPreview);

                        // Document creation for presentation on DocumentViewer at PrintPreview
                        FixedDocumentSequence preview = PrintPreviewWindow.xpsDoc.GetFixedDocumentSequence();
                        PrintPreviewWindow previewWindow = new PrintPreviewWindow(Ticket, preview);
                        previewWindow.Show();
                    }

                    // Internal Note: https://stackoverflow.com/questions/1442607/how-do-i-get-wpfs-documentviewer-to-release-its-file-lock-on-the-source-xps-doc
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// Verifies all files on path that were created as xps document and couldn't delete after usage, deleting immediatly
        /// </summary>
        public void verifyXpsFilesAndDelete()
        {
            try
            {
                string[] files = System.IO.Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)), "*.xps");

                for (var i = 0; i < files.Length; i++)
                {
                    File.Delete(files[i]);
                }
            }
            catch
            {
               Console.WriteLine("Falhou ao apagar ficheiros xps");
            }
            
        }

        /// <summary>
        /// Functionality to print
        /// </summary>
        /// <param name="obj"></param>
        public void PrintCommandAction(object obj)
        {
            if (obj == null)
            {
                MessageBox.Show(FindTranslation("NoItemSelected"));
                return;
            }
            var Eb = obj as EntityBase;
            if (Eb is Ticket)
            {
                Ticket Ticket = Eb as Ticket;
                try
                {
                    // Close Preview Window
                    PrintPreviewWindow.InvokeWindowClosing();

                    // Create the print dialog object and set options
                    PrintDialog pDialog = new PrintDialog();
                    pDialog.PageRangeSelection = PageRangeSelection.AllPages;
                    pDialog.UserPageRangeEnabled = true;

                    // Display the dialog. This returns true if the user presses the print button
                    Nullable<Boolean> print = pDialog.ShowDialog();
                    if (print == true)
                    {
                        XpsDocument xpsDocument = new XpsDocument(PrintPreviewWindow.docPath, FileAccess.ReadWrite);
                        FixedDocumentSequence fixedDocumentSequence = xpsDocument.GetFixedDocumentSequence();
                        pDialog.PrintDocument(fixedDocumentSequence.DocumentPaginator, "");

                        // Close XPS Document to Prevent File Lock
                        xpsDocument.Close();

                        // if the package is not removed from the Package store, we won't be able to re-open the same file again later
                        //(due to System.IO.Packaging.PackageStore's Package store/caching rather than because of any file locks)
                        System.IO.Packaging.PackageStore.RemovePackage(xpsDocument.Uri);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        protected bool PreFilter(object obj)
        {
            if (string.IsNullOrWhiteSpace(Filter))
            {
                return true;
            }
            return CollectionFilter(obj);
        }

        protected virtual bool CollectionFilter(object obj)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Shows the register window for the object that needs to be added to database for further form completion
        /// </summary>
        /// <param name="obj"></param>
        protected void AddCommandAction(object obj)
        {
            try 
            { 
                Window DetailWindow = new Window();
                
                if (this.DataGridName == Generals.FindTranslation("Tickets"))
                {
                    DetailWindow = Activator.CreateInstance(typeof(WindowWeighingFormView)) as Window;
                    DetailWindow.DataContext = Activator.CreateInstance(typeof(WindowWeighingFormViewModel), 0);
                }
                else
                { 
                    DetailWindow = Activator.CreateInstance(WindowType) as Window;
                    DetailWindow.DataContext = Activator.CreateInstance(ViewModelType, 0);
                }
                DetailWindow.Closed += DetailWindow_Closed;
                DetailWindow.Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void DetailWindow_Closed(object sender, EventArgs e)
        {
            RefreshList();
        }

        enum WhichTicketWindow { None, TemporaryTicketWindow, Any }

        /// <summary>
        /// Edit method based on object sent through the edit action
        /// </summary>
        /// <param name="obj"></param>
        private void EditCommandAction(object obj)
        {
            if (obj == null)
            {
                MessageBox.Show(FindTranslation("NoItemSelected"));
                return;
            }
            int openedWindow = (int)WhichTicketWindow.None;
            Window DetailWindow = Activator.CreateInstance(WindowType) as Window;

            if (this.DataGridName == Generals.FindTranslation("TemporaryTickets"))
            {
                if (!String.IsNullOrEmpty((string)ObjectType.GetProperty("Tare").GetValue(obj, null)))
                {
                    MessageBoxResult result = MessageBox.Show(FindTranslation("TemporaryWeighingAlreadyStarted"), FindTranslation("Warning"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.Yes)
                    {
                        DetailWindow.DataContext = Activator.CreateInstance(typeof(WindowWeighingFormViewModel), obj);
                        openedWindow = (int)WhichTicketWindow.TemporaryTicketWindow;
                        DetailWindow.Closed += DetailWindow_Closed;
                        DetailWindow.Show();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    DetailWindow.DataContext = Activator.CreateInstance(typeof(WindowWeighingFormViewModel), obj);
                    openedWindow = (int)WhichTicketWindow.TemporaryTicketWindow;
                    DetailWindow.Closed += DetailWindow_Closed;
                    DetailWindow.Show();
                }
            }
            else
            {
                DetailWindow.DataContext = Activator.CreateInstance(ViewModelType, ObjectType.GetProperty("Id").GetValue(obj, null));
                openedWindow = (int)WhichTicketWindow.Any;
                DetailWindow.Closed += DetailWindow_Closed;
                DetailWindow.Show();
            }
        }

        /// <summary>
        /// Delete method based on object or list of objects sent through the delete action
        /// </summary>
        /// <param name="obj"></param>
        private void DeleteCommandAction(object obj)
        {
            IList List = (IList)obj;
            EntityBase Eb;
            if (List.Count == 0)
            {
                MessageBox.Show(FindTranslation("NoItemSelected"));
            }
            else
            {
                MessageBoxResult result = MessageBox.Show(FindTranslation("AreYouSureWantToRemove") + $" ({List.Count})", FindTranslation("Remove"), MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    List<object> SelectedItems = ((IList<object>)obj).ToList();
                    using (DataModel Context = new DataModel())
                    {
                        
                        SelectedItems.ForEach(x =>
                        {
                            int Id = int.Parse(x.GetType().GetProperty("Id").GetValue(x, null).ToString());
                            Eb = Context.Set(ObjectType).Find(Id) as EntityBase;
                            Eb.IsInactive = true;
                            Context.SaveChanges();
                        });
                        RefreshList();
                    }
                }
            }
        }

        /// <summary>
        /// Gets the updated list from database
        /// </summary>
        protected virtual void RefreshList()
        {
            try
            {
                using (DataModel Context = new DataModel())
                {
                    ItemsCollection = new ObservableCollection<dynamic>(Context.Set(ObjectType).ToDynamicList<dynamic>().Where(x => !x.IsInactive));
                    Context.Tickets
                        .Include("Driver")
                        .Include("Entity")
                        .Include("Product")
                        .Include("Tank")
                        .Include("Trailer")
                        .Include("Transporter")
                        .Include("Vehicle")
                        .OrderByDescending(x => x.Id).Where(x => !x.IsInactive).AsQueryable<dynamic>();

                    NotifyPropertyChanged("ItemsCollection");
                    MyView = CollectionViewSource.GetDefaultView(ItemsCollection);
                    MyView.Filter = PreFilter;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao carregar lista"); //TODO: Arranjar tradução
            }
            
        }

        protected void NotifyPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
