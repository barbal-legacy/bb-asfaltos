﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using System.Xml.Serialization;
using BB_TerminalAsfaltos.Classes;
using Premise.Communications.Entities;
using Premise.IoT;

namespace BB_TerminalAsfaltos.ViewModels
{
    class PageSettingsViewModel:INotifyPropertyChanged
    {

        // Variables
        private bool _isserialconnection = true;
        private bool engineInUse;
        
        public event PropertyChangedEventHandler PropertyChanged;
        public string CurrentWeight { get; set; } = "---------";

        public string SelectedReader { get; set; }
        public string SelectedComPort { get; set; }
        public string SelectedBaudRate { get; set; }
        public string SelectedDataBits { get; set; }
        public string SelectedParity { get; set; }
        public string SelectedStopBits { get; set; }

        public string SelectedRelay1 { get; set; }
        public string SelectedRelay2 { get; set; }
        public string SelectedAlertThreshold { get; set; }
        public string RelaysTimeout { get; set; } = "200";
        public string WeightTimeout { get; set; } = "200";

        public string MinTareWeight       { get; set; }
        public string TurnOnRelay2Weight  { get; set; }
        public string TurnOffRelay2Weight { get; set; }
        public string TurnOffRelay1Weight { get; set; }

        public string SelectedWeighingModule { get; set; }
        public string SelectedRelayBoardModule { get; set; }

        public int SupportedRelayCount { get; set; } = 0;

        public List<string> RelayListName { get; set; }
        public List<string> AlertThresholdList { get; set; } = new List<string>
        {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"
        };

        public string Ip { get; set; }
        public string IpPort { get; set; }

        public List<string> ComPortList { get; set; } = SerialPort.GetPortNames().ToList();
        public List<string> BaudRateList { get; set; } = new List<string>
        {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "38400",
            "56000",
            "57600",
            "115200",
            "128000",
            "230400",
            "256000",
            "460800",
            "921600"
        };
        public List<string> DataBitsList { get; set; } = new List<string> { "5", "6", "7", "8" };
        public List<string> ParityList { get; set; } = Enum.GetNames(typeof(Parity)).ToList();
        public List<string> StopBitsList { get; set; } = Enum.GetNames(typeof(StopBits)).ToList();
        public List<string> ReadersList { get; set; } = new List<string>();
        
        public bool IsSerialConnection {
            get { return _isserialconnection; }
            set
            {
                _isserialconnection = value;
                IsNotSerialConnection = !value;
            }
        } 
        
        public bool IsNotSerialConnection { get; set; } = false;
        public ClickedCommand<object> SaveCommand { get; set; }
        //public ClickedCommand<object> TestConnectionCommand { get; set; }

        IoTDevice iDevice;
        public XElement OldSettings { get; set; }
        public XElement NewSettings { get; set; }
        

        // Page Constructor
        public PageSettingsViewModel()
        {
            SaveCommand = new ClickedCommand<object>(Save);
            //TestConnectionCommand = new ClickedCommand<object>(TestConnection);

            RelayListName = new List<string>();
            
            try
            {
                //Load combobox of existing readers
                XDocument DeviceManagerXDocument = LoadFile("DeviceManager.xml");

                if (DeviceManagerXDocument == null)
                    throw new FileNotFoundException(Generals.FindTranslation("FileNotFoundDeviceManager"));

                foreach (var device in DeviceManagerXDocument.Descendants("Device"))
                {
                    ReadersList.Add(device.Attribute("Name").Value);
                }

                // Load selected reader supported modules
                SelectedWeighingModule = DeviceManagerXDocument.Descendants("Devices").First().Descendants("Device").First().Descendants("Modules").First().Descendants("Weighing").First().Attribute("Dll")?.Value;
                SelectedRelayBoardModule = DeviceManagerXDocument.Descendants("Devices").First().Descendants("Device").First().Descendants("Modules").First().Descendants("RelayBoard").First().Attribute("Dll")?.Value;

                // Inform if reader doesn't have a supported module
                if (SelectedWeighingModule == null)
                    MessageBox.Show(Generals.FindTranslation("WeighingModuleNotSupported"));
                if (SelectedRelayBoardModule == null)
                    MessageBox.Show(Generals.FindTranslation("RelayBoardModuleNotSupported"));

                // Load Relay list names to populate combobox
                foreach (var item in DeviceManagerXDocument.Descendants("Device").First().Descendants("Modules").First().Descendants("RelayBoard").First().Descendants("Relays").First().Descendants("Relay"))
                {
                    RelayListName.Add(item.Attribute("Name")?.Value);
                    SupportedRelayCount++;
                }

                //Load saved settings from xml
                XDocument SavedSettings = LoadFile("Readers.xml");
                // If there are no settings in "Readers.xml" or the file doesn't exist
                if (SavedSettings == null)
                {
                    // Load settings from DeviceManager.xml
                    LoadDeviceManagerSettings(new IoT().GetDevice(SelectedReader).IotDeviceSettings.XmlDeviceSettings);
                }
                else
                { 
                    // Load settings from Reader.xml
                    XElement reader = SavedSettings.Descendants("reader").First();
                    LoadReaderSettings(reader);
                }

                // Load weighing logic configuration from xml
                XDocument SavedConfigurations = LoadFile("Configuration.xml");
                if (SavedConfigurations == null)
                    MessageBox.Show(Generals.FindTranslation("FileNotFoundConfiguration"));
                else
                { 
                   XElement config = SavedConfigurations.Descendants("configuration").First();

                    MinTareWeight = config.Attribute("MinTareWeight")?.Value;
                    TurnOnRelay2Weight = config.Attribute("TurnOnRelay2Weight")?.Value;
                    TurnOffRelay2Weight = config.Attribute("TurnOffRelay2Weight")?.Value;
                    TurnOffRelay1Weight = config.Attribute("TurnOffRelay1Weight")?.Value;
                
                    // Logic specific for W17 indicator
                    if (SelectedReader == "W17")
                    {
                        string tmpt1 = config.Attribute("SelectedRelay1")?.Value;
                        string tmpt2 = config.Attribute("SelectedRelay2")?.Value;

                        int tmpR1 = int.Parse(tmpt1 != "" ? tmpt1 : "0");
                        int tmpR2 = int.Parse(tmpt2 != "" ? tmpt2 : "0");
                        
                        SelectedRelay1 = RelayListName[W17RelayValueConverter(tmpR1) - 1];
                        SelectedRelay2 = RelayListName[W17RelayValueConverter(tmpR2) - 1];
                    }
                    else
                    {
                        SelectedRelay1 = config.Attribute("SelectedRelay1")?.Value;
                        SelectedRelay2 = config.Attribute("SelectedRelay2")?.Value;
                    }

                    SelectedAlertThreshold = config.Attribute("SelectedAlertThreshold")?.Value;
                    
                    RelaysTimeout = config.Attribute("RelaysTimeout")?.Value;
                    WeightTimeout = config.Attribute("WeightTimeout")?.Value;

                    if (String.IsNullOrEmpty(RelaysTimeout))
                        RelaysTimeout = "200";
                    if (String.IsNullOrEmpty(WeightTimeout))
                        WeightTimeout = "200";

                }

                // store as OldSettings
                OldSettings = (new IoT().GetDevice(SelectedReader)).IotDeviceSettings.XmlDeviceSettings;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            NotifyPropertyChanged(string.Empty);
        }

        public int W17RelayConverter(int relayNumber)
        {
            return (SupportedRelayCount - (relayNumber - 1));
        }

        public int W17RelayValueConverter(int relayNumber)
        {
            switch(relayNumber)
            {
                case 1: return 8;
                case 2: return 7;
                case 3: return 6;
                case 4: return 5;
                case 5: return 4;
                case 6: return 3;
                case 7: return 2;
                case 8: return 1;
                default: return -1;
            }
        }

        // Function to search the position of a string in a list and return its position
        private int GetPositionInList(string textToSearch)
        {
            int position = 0;
            foreach (string text in RelayListName)
            {
                if (text == textToSearch)
                    return position;
                position++;
            }
            return position;
        }

        /*
        private Parity GetParity()
        {
            switch(SelectedParity)
            {
                case "Odd": return Parity.Odd;
                case "Even": return Parity.Even;
                case "None": return Parity.None;
                case "Mark": return Parity.Mark;
                case "Space": return Parity.Space;
                default: throw new Exception(Generals.FindTranslation("")); // TODO: add invalid parity error to pt-PT
            }
        }

        private StopBits GetStopbits()
        {
            switch (SelectedStopBits)
            {
                case "None": return StopBits.None;
                case "One": return StopBits.One;
                case "OnePointFive": return StopBits.OnePointFive;
                case "Two": return StopBits.Two;
                default: throw new Exception(Generals.FindTranslation("")); // TODO: add invalid stopbits error to pt-PT
            }
        }
        */

        // Function to load the "filename" .xml file
        private XDocument LoadFile(string filename)
        {
            try
            { 
                return XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filename));
            }
            catch (FileNotFoundException)
            {
                switch(filename)
                {
                    case "Readers.xml":
                        MessageBox.Show(Generals.FindTranslation("FileNotFoundReaders"));
                        break;
                    case "ReadersList.xml":
                        MessageBox.Show(Generals.FindTranslation("FileNotFoundReadersList"));
                        break;
                    case "DeviceManager.xml":
                        MessageBox.Show(Generals.FindTranslation("FileNotFoundDeviceManager"));
                        break;
                    case "Configuration.xml":
                        MessageBox.Show(Generals.FindTranslation("FileNotFoundConfiguration"));
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return null;
        }

        // Method to load the settings stored in Readers.xml
        private void LoadReaderSettings(XElement element)
        {
            SelectedReader = element.Attribute("ReaderCode")?.Value;
            SelectedComPort = element.Attribute("ComPort")?.Value;
            SelectedBaudRate = element.Attribute("BaudRate")?.Value;
            SelectedDataBits = element.Attribute("DataBits")?.Value;
            SelectedParity = element.Attribute("Parity")?.Value;
            SelectedStopBits = element.Attribute("StopBits")?.Value;
            IsSerialConnection = bool.Parse(element.Attribute("IsSerialConnection")?.Value);
            Ip = element.Attribute("Ip")?.Value;
            IpPort = element.Attribute("IpPort")?.Value;
            SupportedRelayCount = element.Attribute("SupportedRelayCount").Value != "" ? int.Parse(element.Attribute("SupportedRelayCount").Value) : 0;
        }

        // Method to load the settings stored in DeviceManager.xml
        private void LoadDeviceManagerSettings(XElement element)
        {
            try
            {
                SelectedReader = element.Attribute("Name")?.Value;

                foreach (var item in element.Descendants("Modules").First().Descendants("RelayBoard").First().Descendants("Relays").First().Descendants("Relay"))
                {
                    RelayListName.Add(item.Attribute("Name")?.Value);
                    SupportedRelayCount++;
                }

                // CheckConnectionType
                switch (element.Attribute("ConnectionType")?.Value)
                {
                    case "Serial":
                        XElement serialSettings = element.Descendants("Settings").First();
                        SelectedComPort     = serialSettings.Attribute("Port")?.Value;
                        SelectedBaudRate    = serialSettings.Attribute("Baudrate")?.Value;
                        SelectedParity      = serialSettings.Attribute("Parity")?.Value;
                        SelectedDataBits    = serialSettings.Attribute("DataBits")?.Value;
                        SelectedStopBits    = serialSettings.Attribute("StopBits")?.Value;
                        Ip                  = "";
                        IpPort              = "";
                        IsSerialConnection  = true;
                    break;
                    case "Socket":
                        XElement socketSettings = element.Descendants("Settings").First();
                        SelectedComPort     = "";
                        SelectedBaudRate    = "";
                        SelectedParity      = "";
                        SelectedDataBits    = "";
                        SelectedStopBits    = "";
                        Ip                  = socketSettings.Attribute("Ip")?.Value;
                        IpPort              = socketSettings.Attribute("IpPort")?.Value;
                        IsSerialConnection = false;
                    break;
                    
                    default: throw new Exception(Generals.FindTranslation("FileEmptyDeviceManager"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // Method to save settings to Readers.xml
        private void SaveReadersXMLFile()
        {
            if (!File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Readers.xml")))
            {
                File.Create(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Readers.xml"));
            }
            XDocument doc = new XDocument(
                new XElement("root",
                    new XElement("readers",
                        new XElement("reader",
                            new XAttribute("ReaderCode", SelectedReader),
                            new XAttribute("BaudRate", SelectedBaudRate),
                            new XAttribute("ComPort", SelectedComPort),
                            new XAttribute("DataBits", SelectedDataBits),
                            new XAttribute("Parity", SelectedParity),
                            new XAttribute("StopBits", SelectedStopBits),
                            new XAttribute("Ip", Ip),
                            new XAttribute("IpPort", IpPort),
                            new XAttribute("IsSerialConnection", IsSerialConnection),
                            new XAttribute("SupportedRelayCount", SupportedRelayCount)
                            )
                        )
                    )
                );
            doc.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Readers.xml"));
        }

        // Method to save settings to Configuration.xml
        private void SaveConfigurationXMLFile()
        {
            if (!File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration.xml")))
            {
                File.Create(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration.xml"));
            }

            SelectedRelay1 = GetPositionInList(SelectedRelay1).ToString();
            SelectedRelay2 = GetPositionInList(SelectedRelay2).ToString();

            if (SelectedReader == "W17")
            {
                SelectedRelay1 = (W17RelayValueConverter(int.Parse(SelectedRelay1) + 1)).ToString();
                SelectedRelay2 = (W17RelayValueConverter(int.Parse(SelectedRelay2) + 1)).ToString();
            }

            XDocument doc2 = new XDocument(
                new XElement("root",
                    new XElement("configurations",
                        new XElement("configuration",
                            new XAttribute("MinTareWeight", MinTareWeight),
                            new XAttribute("TurnOnRelay2Weight", TurnOnRelay2Weight),
                            new XAttribute("TurnOffRelay2Weight", TurnOffRelay2Weight),
                            new XAttribute("TurnOffRelay1Weight", TurnOffRelay1Weight),
                            new XAttribute("SelectedRelay1", SelectedRelay1),
                            new XAttribute("SelectedRelay2", SelectedRelay2),
                            new XAttribute("SelectedAlertThreshold", SelectedAlertThreshold),
                            new XAttribute("RelaysTimeout", RelaysTimeout),
                            new XAttribute("WeightTimeout", WeightTimeout)
                            )
                        )
                    )
            );

            doc2.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration.xml"));

            if (SelectedReader == "W17")
            {
                SelectedRelay1 = RelayListName[W17RelayValueConverter(int.Parse(SelectedRelay1)) - 1];
                SelectedRelay2 = RelayListName[W17RelayValueConverter(int.Parse(SelectedRelay2)) - 1];
            }
        }

        // Function to generate an XElement with the DeviceManager.xml structure
        private XElement GenerateXElementFromSelectedSettings()
        {
            string ConnectionType = IsSerialConnection ? "Serial" : "Socket";
            if (IsSerialConnection)
            { 
                return 
                        new XElement("Device", 
                            new XAttribute("Name", SelectedReader),
                            new XAttribute("ConnectionType", ConnectionType),
                            new XElement("Settings",
                                new XAttribute("Port", SelectedComPort),
                                new XAttribute("Baudrate", SelectedBaudRate),
                                new XAttribute("Parity", SelectedParity),
                                new XAttribute("Databits", SelectedDataBits),
                                new XAttribute("Stopbits", SelectedStopBits)
                            ),
                            new XElement("Modules", 
                                new XElement("Weighing", 
                                    new XAttribute("Dll", SelectedWeighingModule)
                                ),
                                new XElement("RelayBoard",
                                    new XAttribute("Dll", SelectedRelayBoardModule),
                                    new XElement("Relays", 
                                        new XElement("Relay", new XAttribute("Name", RelayListName[0])),
                                        new XElement("Relay", new XAttribute("Name", RelayListName[1])),
                                        new XElement("Relay", new XAttribute("Name", RelayListName[2])),
                                        new XElement("Relay", new XAttribute("Name", RelayListName[3]))
                                    )
                                )
                            )
                    );
            }
            else
            {
                return 
                        new XElement("Device",
                            new XAttribute("Name", SelectedReader),
                            new XAttribute("ConnectionType", ConnectionType),
                            new XElement("Settings",
                                new XAttribute("Ip", SelectedComPort),
                                new XAttribute("Port", Ip),
                                new XAttribute("Parity", IpPort)
                            ),
                            new XElement("Modules",
                                new XElement("Weighing",
                                    new XAttribute("Dll", SelectedWeighingModule)
                                ),
                                new XElement("RelayBoard",
                                    new XAttribute("Dll", SelectedRelayBoardModule),
                                    new XElement("Relays",
                                        new XElement("Relay", new XAttribute("Name", RelayListName[0])),
                                        new XElement("Relay", new XAttribute("Name", RelayListName[1])),
                                        new XElement("Relay", new XAttribute("Name", RelayListName[2])),
                                        new XElement("Relay", new XAttribute("Name", RelayListName[3]))
                                    )
                                )
                            )
                        
                    );
            }
            return null;
        }

        // Method to Save the selected Settings
        private void Save(object obj)
        {
            // Avoid both relays being the same
            if (SelectedRelay1 == SelectedRelay2)
            {
                MessageBox.Show(Generals.FindTranslation("DuplicateRelaySelection"));
                return;
            }

            // Save reader.xml file
            SaveReadersXMLFile();

            // Save configuration.xml file
            SaveConfigurationXMLFile();

            try
            {
                // Update DeviceManager.xml file with the new settings
                NewSettings = GenerateXElementFromSelectedSettings();

                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);

                XElement xmlFile = XElement.Load(Path.Combine(Path.GetDirectoryName(path), "DeviceManager.xml"));

                 XElement xmlNode = xmlFile.Elements().Where(i => i.Attribute("Name").Value == SelectedReader).FirstOrDefault();
                if (xmlNode != null)
                {
                    xmlNode.Remove();
                }

                xmlFile.Add(NewSettings);
                xmlFile.Save(Path.Combine(Path.GetDirectoryName(path), "DeviceManager.xml"));

                MessageBox.Show(Generals.FindTranslation("SaveWithSucess"));
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show(Generals.FindTranslation("FileNotFoundDeviceManager"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        protected void NotifyPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
