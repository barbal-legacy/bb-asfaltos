﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using static BB_TerminalAsfaltos.Classes.Generals;

namespace BB_TerminalAsfaltos.ViewModels
{
    class PageTransportersViewModel : PageBaseViewModel
    {
        public static Transporter Transporter { get; set; }

        public PageTransportersViewModel() : base(FindTranslation("Transporters"), typeof(Transporter), typeof(WindowTransportersFormView), typeof(WindowTransportersFormViewModel))
        {
            DateFilterVisibility = Visibility.Collapsed;
            DeleteButtonVisibility = Visibility.Collapsed;

            using (DataModel Context = new DataModel())
            {
                Transporter = Context.Transporters.FirstOrDefault();
                if (Transporter == null)
                {
                    Transporter = new Transporter();
                }
            }
            ColumnsList = new ObservableCollection<DataGridColumn>()
                {
                    new DataGridTextColumn
                        {
                            Binding=new Binding("Id"),
                            Header=FindTranslation("ID"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                    {
                        Binding=new Binding("Name"),
                        Header=FindTranslation("Name"),
                        ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                    }
                };
            
        }

        protected override bool CollectionFilter(object obj)
        {
            return !((Transporter)obj).IsInactive
            && ((Transporter)obj).Name?.IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}
