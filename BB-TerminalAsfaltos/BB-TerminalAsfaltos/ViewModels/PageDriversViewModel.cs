﻿using System;
using System.Collections.Generic;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Collections.ObjectModel;
using static BB_TerminalAsfaltos.Classes.Generals;

namespace BB_TerminalAsfaltos.ViewModels
{
    class PageDriversViewModel : PageBaseViewModel
    {
        public static Driver Driver { get; set; }

        public PageDriversViewModel() : base(FindTranslation("Drivers"), typeof(Driver), typeof(WindowDriversFormView), typeof(WindowDriversFormViewModel))
        {
            DateFilterVisibility = Visibility.Collapsed;
            DeleteButtonVisibility = Visibility.Collapsed;

            ColumnsList = new ObservableCollection<DataGridColumn>()
                    {
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Id"),
                            Header=FindTranslation("ID"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Name"),
                            Header=FindTranslation("Name"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("DateID") { StringFormat = "dd/MM/yyyy"},
                            Header=FindTranslation("ExpirationDateId"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("DateDriversLicense") { StringFormat = "dd/MM/yyyy"},
                            Header=FindTranslation("ExpirationDateDriverLicense"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("DateADR") { StringFormat = "dd/MM/yyyy"},
                            Header=FindTranslation("ExpirationDateADR"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },new DataGridTextColumn
                        {
                            Binding=new Binding("DateSecurityInduction") { StringFormat = "dd/MM/yyyy"},
                            Header=FindTranslation("ExpirationDateSecurityInduction"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                    };
            
        }

        protected override bool CollectionFilter(object obj)
        {
            return !((Driver)obj).IsInactive
               && ((Driver)obj).Name?.IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Driver)obj).DateID.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Driver)obj).DateDriversLicense.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Driver)obj).DateADR.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}
