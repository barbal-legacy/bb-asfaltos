﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BB_TerminalAsfaltos.ViewModels
{
    class WindowEntitiesFormViewModel : WindowBaseViewModel
    {
        public WindowEntitiesFormViewModel(int Id) : base(typeof(Entity))
        {
            WindowTitle = Generals.FindTranslation("GeneralRegister");
            if (Id == 0)
            {
                RemoveButtonVisivility = System.Windows.Visibility.Collapsed;
                IsNewRecord = true;
                Item = new Entity();
            }
            else
            {
                RemoveButtonVisivility = System.Windows.Visibility.Visible;
                using (DataModel Context = new DataModel())
                {
                    Item = Context.Entities.Find(Id);
                }
            }
        }
    }
}
