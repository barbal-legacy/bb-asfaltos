﻿using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views.PrintViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace BB_TerminalAsfaltos.ViewModels
{
    public class GeneralPrintingViewModel 
    {
        
        public Visibility DuplicatedVisibility { get; set; } = Visibility.Collapsed;
        public Company Company { get; set; }
        public static Ticket Ticket { get; set; }

        public string CurrentDate { get; set; } = DateTime.Now.ToString("dd-MM-yyyy");
        public string CurrentHour { get; set; } = DateTime.Now.ToString("HH:mm");

        public string Tare { get; set; }
        public string Gross { get; set; }
        public string Net { get; set; }
        //public string LoadTemperature { get; set; }

        // Load Page
        public GeneralPrintingViewModel(Ticket ticket)
        {
            if (ticket == null)
            {
                return;
            }

            try
            {
                using (DataModel Context = new DataModel())
                {
                    Tare = PlaceDotOrComma(ticket.Tare) + " Kg";
                    Gross = PlaceDotOrComma(ticket.Gross) + " Kg";
                    Net = PlaceDotOrComma(ticket.Net) + " Kg";

                    /* 
                    //Auto separate k units
                    Tare = String.Format("{0,8:N0}", int.Parse(ticket.Tare)) + " Kg";
                    Gross = String.Format("{0,8:N0}", int.Parse(ticket.Gross)) + " Kg";
                    Net = String.Format("{0,6:N0}", int.Parse(ticket.Net)) + " Kg";
                    */

                    //LoadTemperature = Ticket.LoadTemperature + " ºC";

                    ticket.LastPrintDate = DateTime.Parse(CurrentDate);
                    ticket.LastPrintTime = CurrentHour;
                        
                    Context.Entry(ticket).State = System.Data.Entity.EntityState.Modified;
                    Context.SaveChanges();

                    Ticket = ticket;

                    // get company settings to finish print
                    Company = Context.Companies.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        //public GeneralPrintingViewModel(Ticket ticket)
        //{
        //    try
        //    {
        //        Tare = PlaceDotOrComma(ticket.Tare) + " Kg";
        //        Gross = PlaceDotOrComma(ticket.Gross) + " Kg";

        //        //Gross = Ticket.Gross + " Kg";
        //        Net = PlaceDotOrComma(ticket.Net) + " Kg";

        //        //LoadTemperature = Ticket.LoadTemperature + " ºC";

        //        ticket.LastPrintDate = DateTime.Parse(CurrentDate);
        //        ticket.LastPrintTime = CurrentHour;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }

        //}

        private string PlaceDotOrComma(string number)
        {
            if (String.IsNullOrEmpty(number))
                return "";

            if (number.Length <= 3)
                return number;

            string tmp = "";
            int current_position = 0;
            foreach(char c in number)
            {
                if (current_position == number.Length - 3)
                    tmp += ".";
                tmp += c;
                current_position++;
            }

            return tmp;
        }
    }
}
