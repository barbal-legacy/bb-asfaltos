﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace BB_TerminalAsfaltos.ViewModels
{
    class MainWindowViewModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ClickedCommand<object> MenuCommand { get; set; }
        public Page PageToShow { get; set; }
        public static Page currentPage;
        public MainWindowViewModel()
        {
            try
            {
                MenuCommand = new ClickedCommand<object>(MenuCommandPageView);
                MenuCommandPageView("Tickets");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); // TODO: Adicionar ao pt-PT
                //MenuCommandPageView("Drivers");
            }
        }

        private void MenuCommandPageView(object obj) {
            

            switch (obj)
            {
                case "Drivers":
                    PageToShow = new PageListingsView() { DataContext = new PageDriversViewModel() };
                    break;
                case "Entities":
                    PageToShow = new PageListingsView() { DataContext = new PageEntitiesViewModel() };
                    break;
                case "Products":
                    PageToShow = new PageListingsView() { DataContext = new PageProductsViewModel() };
                    break;
                case "Tanks":
                    PageToShow = new PageListingsView() { DataContext = new PageTanksViewModel() };
                    break;
                case "Tickets":
                    PageToShow = new PageListingsView() { DataContext = new PageTicketsViewModel() };
                    break;
                case "TemporaryTickets":
                    PageToShow = new PageListingsView() { DataContext = new PageTemporaryTicketsViewModel() };
                    break;
                case "Trailers":
                    PageToShow = new PageListingsView() { DataContext = new PageTrailersViewModel() };
                    break;
                case "Transporters":
                    PageToShow = new PageListingsView() { DataContext = new PageTransportersViewModel() };
                    break;
                case "Vehicles":
                    PageToShow = new PageListingsView() { DataContext = new PageVehiclesViewModel() };
                    break;
                case "Settings":
                    PageToShow = new PageSettingsView() { DataContext = new PageSettingsViewModel() };
                    break;
                case "Exit":
                    Generals.GetCurrentWindow().Close();
                    break;
                default:
                    break;
            }
            currentPage = PageToShow;
            NotifyPropertyChanged("PageToShow");
        }


        protected void NotifyPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
