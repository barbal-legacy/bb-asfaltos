﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using static BB_TerminalAsfaltos.Classes.Generals;


namespace BB_TerminalAsfaltos.ViewModels
{
    class WindowTicketFormViewModel : WindowBaseViewModel
    {
        public WindowTicketFormViewModel(int Id) : base(typeof(Ticket))
        {
            WindowTitle = Generals.FindTranslation("Tickets");

            if (Id == 0)
            {
                IsNewRecord = true;
                Item = new Ticket();
            }
            else
            {
                using (DataModel Context = new DataModel())
                {
                    Item = Context.Tickets.Find(Id);
                    
                    
                    ClientsList      = Context.Entities.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                    VehiclesList     = Context.Vehicles.Where(x => !x.IsInactive).OrderBy(x => x.LicensePlate).ToList();
                    TrailersList     = Context.Trailers.Where(x => !x.IsInactive).OrderBy(x => x.LicensePlate).ToList();
                    TanksList        = Context.Tanks.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                    //TanksList        = new List<string>Context.Tickets.Find(Id).TanksID;
                    ProductsList     = Context.Products.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                    DriversList      = Context.Drivers.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                    TransportersList = Context.Transporters.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                }
            }
        }
    }
}
