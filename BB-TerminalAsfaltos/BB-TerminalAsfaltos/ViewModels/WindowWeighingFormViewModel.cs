﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using static BB_TerminalAsfaltos.Classes.Generals;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Timers;
using Premise.IoT;
using Premise.IoT.Entities.Weight;
using System.Threading;
using Premise.IoT.Core.Modules;
using WPFCustomMessageBox;
using BB_TerminalAsfaltos.Views;
using System.Data.Common;
using System.IO.Ports;
using System.Text;
using static BB_TerminalAsfaltos.Classes.WeighReader;

namespace BB_TerminalAsfaltos.ViewModels
{
    class WindowWeighingFormViewModel : WindowBaseViewModel, INotifyPropertyChanged
    {
        DataModel Context = new DataModel();
        public event PropertyChangedEventHandler PropertyChanged;
        public static WeighReader reader;
        private Type ObjectType;
        public static bool isConnectionOpen = false;

        Type WindowToShow, ViewModelToShow;

        enum weighingState { IDLE = 0, STATE1 = 1, STATE2 = 2, STATE3 = 3, STATE4 = 4, BYPASS = 5 }
        static int currentWeighingState = 0;                                                                            // check state of weighing in process

        int minTareValue = 200;                                                                                         // SET MIN TARE VALUE IN CASE CONFIGURATION.XML IS EMPTY 
        int turnOnRelay2Weight = 500;                                                                                   // SET MIN relay2 weight VALUE IN CASE CONFIGURATION.XML IS EMPTY 
        int turnOffRelay2Weight = 1200;                                                                                 // SET MIN relay2 OFF weight VALUE IN CASE CONFIGURATION.XML IS EMPTY 
        int turnOffRelay1Weight = 200;                                                                                  // SET MIN relay1 OFF weight VALUE IN CASE CONFIGURATION.XML IS EMPTY
        int selectedAlertThreshold = 1;                                                                                 // SET alerts threshold VALUE IN CASE CONFIGURATION.XML IS EMPTY
        int selectedRelay1 = 8;                                                                                         // SET default relay1 IN CASE CONFIGURATION.XML IS EMPTY
        int selectedRelay2 = 7;                                                                                         // SET default relay2 IN CASE CONFIGURATION.XML IS EMPTY
        int limitMaxWeight = 25000;                                                                                     // SET default MAX value for the MaxWeight (target weight)

        string rsIdleANDrsState4;                                                                                       // variables to be used to compare relays statuses
        string rsState1ANDrsState3;                                                                                     //
        string rsState2;                                                                                                //
        bool[] checkRelaysStatusState = { false, true, true, true, true, false };

        int actualTare = -1;
        int temporaryWeightID = 0;
        string tareHour;
        bool manualTare = false;                                                                                        // check if the user wrote down the tare value manually
        bool alertFired = false;
        bool bypassed = false;
        bool expiredDocs = false;
        public static bool wasFieldEdited = false;

        public static int GeneralTimeoutValue = 300;

        public string CurrentWeight { get; set; } = "---";
        public Ticket Ticket { get; set; } = new Ticket();
        public static Weighing Weight { get; set; } 

        #region Bindings
        private string _getStatusLabel { get; set; }
        public string GetStatusLabel
        {
            get { return _getStatusLabel; }
            set
            {
                if (value != _getStatusLabel)
                {
                    _getStatusLabel = value;
                    NotifyPropertyChanged("GetStatusLabel");
                }
            }
        }

        private int? _vehicleid;
        public int? VehicleId
        {
            get { return _vehicleid; }
            set
            {
                if (_vehicleid == value) return;
                _vehicleid = value;
                Weight.VehicleID = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("VehicleId");
            }
        }

        private int? _trailerid;
        public int? TrailerId
        {
            get { return _trailerid; }
            set
            {
                if (_trailerid == value) return;
                _trailerid = value;
                Weight.TrailerID = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("TrailerId");
            }
        }

        private int? _productid;
        public int? ProductId
        {
            get { return _productid; }
            set
            {
                if (_productid == value) return;
                _productid = value;
                Weight.ProductID = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("ProductId");
            }
        }

        private string _loadTemperature;
        public string LoadTemperature
        {
            get { return _loadTemperature; }
            set
            {
                if (_loadTemperature == value) return;
                _loadTemperature = value;
                Weight.LoadTemperature = value;
                NotifyPropertyChanged("LoadTemperature");
            }
        }

        private int? _tankid;
        public int? TankId
        {
            get { return _tankid; }
            set
            {
                if (_tankid == value) return;
                _tankid = value;
                Weight.TankID = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("TankId");
            }
        }

        private string _tankNameLabel;
        public string TankNameLabel
        {
            get { return _tankNameLabel; }
            set
            {
                if (_tankNameLabel == value) return;
                _tankNameLabel = value;
                Weight.TanksID = value;
                if (!String.IsNullOrEmpty(Weight.TanksID))
                    Weight.TankID = Weight.TanksConverter(Weight.TanksID).FirstOrDefault().Id;
                NotifyPropertyChanged("TankNameLabel");
            }
        }

        private string _batch;
        public string Batch
        {
            get { return _batch; }
            set
            {
                if (_batch == value) return;
                _batch = value;
                Weight.Batch = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("Batch");
            }
        }

        private string _observations;
        public string Observations
        {
            get { return _observations; }
            set
            {
                if (_observations == value) return;
                _observations = value;
                Weight.Observations = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("Observations");
            }
        }

        private int? _driverid;
        public int? DriverId
        {
            get { return _driverid; }
            set
            {
                if (_driverid == value) return;
                _driverid = value;
                Weight.DriverID = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("DriverId");
            }
        }

        private int? _transporterid;
        public int? TransporterId
        {
            get { return _transporterid; }
            set
            {
                if (_transporterid == value) return;
                _transporterid = value;
                Weight.TransporterID = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("TransporterId");
            }
        }

        private int? _entityid;
        public int? EntityId
        {
            get { return _entityid; }
            set
            {
                if (_entityid == value) return;
                _entityid = value;
                Weight.EntityID = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("EntityId");
            }
        }

        private string _deliveryNote;
        public string DeliveryNote
        {
            get { return _deliveryNote; }
            set
            {
                if (_deliveryNote == value) return;
                _deliveryNote = value;
                Weight.DeliveryNote = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("DeliveryNote");
            }
        }

        private string _maxWeight;
        public string MaxWeight
        {
            get { return _maxWeight; }
            set
            {
                if (_maxWeight == value) return;
                _maxWeight = value;
                Weight.MaximumWeight = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("MaxWeight");
            }
        }

        private string _tare;
        public string Tare
        {
            get { return _tare; }
            set
            {
                if (_tare == value) return;
                _tare = value;
                wasFieldEdited = true;
                NotifyPropertyChanged("Tare");
            }
        }

        private string _grossWeight;
        public string GrossWeight
        {
            get { return _grossWeight; }
            set
            {
                if (_grossWeight == value) return;
                _grossWeight = value;
                NotifyPropertyChanged("GrossWeight");
            }
        }

        private string _netWeight;
        public string NetWeight
        {
            get { return _netWeight; }
            set
            {
                if (_netWeight == value) return;
                _netWeight = value;
                NotifyPropertyChanged("NetWeight");
            }
        }

        private bool _isEnabled_Driver = true;
        public bool IsEnabled_Driver
        {
            get { return _isEnabled_Driver; }
            set
            {
                if (_isEnabled_Driver == value) return;
                _isEnabled_Driver = value;
                NotifyPropertyChanged("IsEnabled_Driver");
            }
        }

        private bool _isEnabled_Trailer = true;
        public bool IsEnabled_Trailer
        {
            get { return _isEnabled_Trailer; }
            set
            {
                if (_isEnabled_Trailer == value) return;
                _isEnabled_Trailer = value;
                NotifyPropertyChanged("IsEnabled_Trailer");
            }
        }


        private bool _isEnabled_Vehicle = true;
        public bool IsEnabled_Vehicle
        {
            get { return _isEnabled_Vehicle; }
            set
            {
                if (_isEnabled_Vehicle == value) return;
                _isEnabled_Vehicle = value;
                NotifyPropertyChanged("IsEnabled_Vehicle");
            }
        }

        private bool _isEnabled_tare = true;
        public bool IsEnabled_tare
        {
            get { return _isEnabled_tare; }
            set
            {
                if (_isEnabled_tare == value) return;
                _isEnabled_tare = value;
                NotifyPropertyChanged("IsEnabled_tare");
            }
        }

        private bool _isEnabled_btTare = true;
        public bool IsEnabled_btTare
        {
            get { return _isEnabled_btTare; }
            set
            {
                if (_isEnabled_btTare == value) return;
                _isEnabled_btTare = value;
                NotifyPropertyChanged("IsEnabled_btTare");
            }
        }

        private bool _isEnabled_maxWeight = true;
        public bool IsEnabled_maxWeight
        {
            get { return _isEnabled_maxWeight; }
            set
            {
                if (_isEnabled_maxWeight == value) return;
                _isEnabled_maxWeight = value;
                NotifyPropertyChanged("IsEnabled_maxWeight");
            }
        }

        private bool _visibility_btStart = true;
        public bool Visibility_btStart
        {
            get { return _visibility_btStart; }
            set
            {
                if (_visibility_btStart == value) return;
                _visibility_btStart = value;
                NotifyPropertyChanged("Visibility_btStart");
            }
        }

        private bool _visibility_btEnd = false;
        public bool Visibility_btEnd
        {
            get { return _visibility_btEnd; }
            set
            {
                if (_visibility_btEnd == value) return;
                _visibility_btEnd = value;
                NotifyPropertyChanged("Visibility_btEnd");
            }
        }

        private bool _visibility_btReset = true;
        public bool Visibility_btReset
        {
            get { return _visibility_btReset; }
            set
            {
                if (_visibility_btReset == value) return;
                _visibility_btReset = value;
                NotifyPropertyChanged("Visibility_btReset");
            }
        }

        private bool _visibility_btCancel = false;
        public bool Visibility_btCancel
        {
            get { return _visibility_btCancel; }
            set
            {
                if (_visibility_btCancel == value) return;
                _visibility_btCancel = value;
                NotifyPropertyChanged("Visibility_btCancel");
            }
        }

        private bool _visibility_btRelays = false;
        public bool Visibility_btRelays
        {
            get { return _visibility_btRelays; }
            set
            {
                if (_visibility_btRelays == value) return;
                _visibility_btRelays = value;
                NotifyPropertyChanged("Visibility_btRelays");
            }
        }

        private bool _relay1State = false;
        public bool Relay1State
        {
            get { return _relay1State; }
            set
            {
                if (_relay1State == value) return;
                _relay1State = value;
                NotifyPropertyChanged("Relay1State");
            }
        }

        private bool _relay2State = false;
        public bool Relay2State
        {
            get { return _relay2State; }
            set
            {
                if (_relay2State == value) return;
                _relay2State = value;
                NotifyPropertyChanged("Relay2State");
            }
        }

        private ObservableCollection<string> _ErrorsList = new ObservableCollection<string>();
        public ObservableCollection<string> ErrorsList
        {
            get { return _ErrorsList; }
            set
            {
                if (_ErrorsList == value) return;
                _ErrorsList = value;
                NotifyPropertyChanged("ErrorsList");
            }
        }

        private ObservableCollection<string> _alertsList = new ObservableCollection<string>();
        public ObservableCollection<string> AlertsList
        {
            get { return _alertsList; }
            set
            {
                if (_alertsList == value) return;
                _alertsList = value;
                NotifyPropertyChanged("AlertsList");
            }
        }

        # endregion Contructors

        public List<Vehicle> VehiclesList { get; set; }
        public List<Trailer> TrailersList { get; set; }
        public List<Tank> TanksList { get; set; }
        public List<Product> ProductsList { get; set; }
        public List<Driver> DriversList { get; set; }
        public List<Transporter> TransportersList { get; set; }
        public List<Entity> ClientsList { get; set; }

        public ClickedCommand<object> ResetCancelWeightCommand { get; set; }
        public ClickedCommand<object> EndWeightCommand { get; set; }
        public ClickedCommand<object> TareCommand { get; set; }
        public ClickedCommand<object> StartWeightCommand { get; set; }
        public ClickedCommand<object> AddTankCommand { get; set; }
        public ClickedCommand<object> OpenInsertWindowCommand { get; set; }

        public WindowWeighingFormViewModel(object obj) : base(typeof(Weighing))
        {
            InitializeComponents(obj);
            FillFormWithData(obj);
        }

        public WindowWeighingFormViewModel(int Id) : base(typeof(Ticket))
        {
            InitializeComponents(null);
        }

        private void InitializeComponents(object obj)
        {
            WindowTitle = Generals.FindTranslation("Weighing");

            Weight = obj as Weighing;
            if (Weight == null)
                Weight = new Weighing();

            currentWeighingState = 0;

            // Load reader settings and weight logic values
            try
            {
                reader = new WeighReader();
                // get values from Configuration.xml
                minTareValue = !String.IsNullOrEmpty(reader.MinTareWeight) ? Int16.Parse(reader.MinTareWeight) : minTareValue;
                turnOnRelay2Weight = !String.IsNullOrEmpty(reader.TurnOnRelay2Weight) ? Int16.Parse(reader.TurnOnRelay2Weight) : turnOnRelay2Weight;
                turnOffRelay2Weight = !String.IsNullOrEmpty(reader.TurnOffRelay2Weight) ? Int16.Parse(reader.TurnOffRelay2Weight) : turnOffRelay2Weight;
                turnOffRelay1Weight = !String.IsNullOrEmpty(reader.TurnOffRelay1Weight) ? Int16.Parse(reader.TurnOffRelay1Weight) : turnOffRelay1Weight;
                selectedAlertThreshold = !String.IsNullOrEmpty(reader.SelectedAlertThreshold) ? Int16.Parse(reader.SelectedAlertThreshold) : selectedAlertThreshold;
                selectedRelay1 = !String.IsNullOrEmpty(reader.SelectedRelay1) ? Int16.Parse(reader.SelectedRelay1) : selectedRelay1;
                selectedRelay2 = !String.IsNullOrEmpty(reader.SelectedRelay2) ? Int16.Parse(reader.SelectedRelay2) : selectedRelay2;

                #region Predefined Relay Status Builder
                //Set estimaded relays status for each weighing state, beforehand, to allow for future comparisons

                //Initial relays status (W17 - 1: Off | 0: On)
                char[] relayStatusBuilder = "11111111".ToCharArray();

                //rs = Relay Status
                //rsIdleANDrsState4 = Weighingstate IDLE and State4
                rsIdleANDrsState4 = Encoding.ASCII.GetString(Encoding.Default.GetBytes(relayStatusBuilder));

                relayStatusBuilder[selectedRelay1 - 1] = '0';
                //rsState1ANDrsState3 = Weighingstate State1 and State3
                rsState1ANDrsState3 = Encoding.ASCII.GetString(Encoding.Default.GetBytes(relayStatusBuilder));

                relayStatusBuilder[selectedRelay2 - 1] = '0';
                rsState2 = Encoding.ASCII.GetString(Encoding.Default.GetBytes(relayStatusBuilder));
                #endregion
            }
            catch
            {
                MessageBoxResult result = MessageBox.Show(FindTranslation("ReaderNotFound"));

                if (result == MessageBoxResult.OK)
                {
                    WindowWeighingFormView.WhereWindow = 1;
                    WindowWeighingFormView.InvokeWindowClosing();
                }
            }

            ResetCancelWeightCommand = new ClickedCommand<object>(ResetCancelWeightAction);
            EndWeightCommand = new ClickedCommand<object>(EndWeightAction);
            TareCommand = new ClickedCommand<object>(TareCommandAction);
            StartWeightCommand = new ClickedCommand<object>(StartWeightCommandAction);
            AddTankCommand = new ClickedCommand<object>(AddTankCommandAction);
            OpenInsertWindowCommand = new ClickedCommand<object>(OpenInsertWindowCommandAction);

            try
            {
                ClientsList = Context.Entities.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                VehiclesList = Context.Vehicles.Where(x => !x.IsInactive).OrderBy(x => x.LicensePlate).ToList();
                TrailersList = Context.Trailers.Where(x => !x.IsInactive).OrderBy(x => x.LicensePlate).ToList();
                TanksList = Context.Tanks.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                ProductsList = Context.Products.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                DriversList = Context.Drivers.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                TransportersList = Context.Transporters.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();

                reader.OnWeightReceived += Reader_GetWeight;
                isConnectionOpen = reader.Open();
                //reset relays state
                ResetRelaysStatus();

                //StressTest3(); //StressTest#() (1, 2, 3)
            }
            catch (Exception ex)
            {
                MessageBoxResult result = MessageBox.Show(FindTranslation("WeighingLoadingConfigurationsError"));

                if (result == MessageBoxResult.OK)
                {
                    WindowWeighingFormView.WhereWindow = 1;
                    WindowWeighingFormView.InvokeWindowClosing();
                }
            }
        }
        
        /// <summary>
        /// Method to fill the data from DB in this window
        /// </summary>
        /// <param name="obj">Weighing object with the previously saved data </param>
        private void FillFormWithData(object obj)
        {
            if (obj == null)
                return;

            var Eb = obj as EntityBase;
            if (Eb is Weighing)
            {
                Weighing Weighing = Eb as Weighing;
                try
                {
                    temporaryWeightID = Weighing.Id;
                    Batch = Weighing.Batch;
                    DeliveryNote = Weighing.DeliveryNote;
                    DriverId = Weighing.DriverID;
                    EntityId = Weighing.EntityID;
                    LoadTemperature = Weighing.LoadTemperature;
                    MaxWeight = Weighing.MaximumWeight;
                    Observations = Weighing.Observations;
                    ProductId = Weighing.ProductID;
                    TankId = Weighing.TankID;
                    TrailerId = Weighing.TrailerID;
                    TransporterId = Weighing.TransporterID;
                    VehicleId = Weighing.VehicleID;
                    TankNameLabel = Weighing.TanksID;
                    tareHour = Weighing.TareHour;
                    
                    // Se tem uma tara -> currentWeighingState != (int)weighingState.IDLE;
                    if (String.IsNullOrEmpty(Weighing.Tare))
                    {
                        currentWeighingState = (int)weighingState.IDLE;
                        Tare = "";
                        manualTare = false;
                        GrossWeight = "";
                        NetWeight = "";
                    }
                    else
                    {
                        currentWeighingState = (int)weighingState.STATE1;
                        Tare = Weighing.Tare;
                        tareHour = tareHour;
                        manualTare = Weighing.manualTare;
                        GrossWeight = Weighing.Gross;
                        NetWeight = Weighing.Net;
                    }

                    StartWeightCommandAction(null);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// Resets all variables so that it matches the initial state
        /// </summary>
        private void Reset()
        {
            VehicleId = null;
            TrailerId = null;
            TankId = null;
            TankNameLabel = "";
            ProductId = null;
            LoadTemperature = null;
            Batch = null;
            Observations = null;
            DriverId = null;
            TransporterId = null;
            EntityId = null;
            DeliveryNote = null;
            MaxWeight = null;
            Tare = null;
            tareHour = "";
            GrossWeight = null;
            NetWeight = null;

            alertFired = false;
            bypassed = false;
            actualTare = 0;
            currentWeighingState = (int)weighingState.IDLE;

            IsEnabled_tare = true;
            IsEnabled_Vehicle = true;
            IsEnabled_Trailer = true;
            IsEnabled_Driver = true;
            IsEnabled_btTare = true;
            IsEnabled_maxWeight = true;
            Visibility_btStart = true;
            Visibility_btReset = true;
            Visibility_btEnd = false;
            Visibility_btCancel = false;
            Visibility_btRelays = false;

            ErrorsList.Clear();

            Weight = new Weighing();
            Weight.Id = temporaryWeightID;

            NotifyPropertyChanged(string.Empty);
        }

        /// <summary>
        /// Cancel function on weighing process
        /// </summary>
        /// <param name="obj"></param>
        private void ResetCancelWeightAction(object obj)
        {
            if (currentWeighingState > (int)weighingState.IDLE)
            {
                Weight.IsInactive = true;
                RegisterTemporaryWeight();
                WindowClosing();
            }
            else
                Reset();
        }

        private void WindowClosing()
        {
            WindowWeighingFormView.WhereWindow = 0;
            WindowWeighingFormView.InvokeWindowClosing();
        }

        /// <summary>
        /// End weighing process by adding the retrieved data into Tickets and Weighings 
        /// Resets the page after finishing
        /// </summary>
        /// <param name="obj">Object for action conclusion</param>
        private void EndWeightAction(object obj)
        {
            if (currentWeighingState != (int)weighingState.STATE4)
            {
                MessageBoxResult result = MessageBox.Show(FindTranslation("AreYouSureWantToEnd"), FindTranslation("Alert"), MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                    return;
            }

            RegisterWeight();

            bool wasSaveOk = false;

            int errorCount = 0;
            
            using (DataModel Context = new DataModel())
            {
                try
                {
                    if (Weight.Id == 0)
                    {
                        Weight.IsFinished = true;
                        Context.Weighings.Add(Weight);
                    }
                    else
                    {
                        Weight.IsFinished = true;
                        Weight.RefreshObject(Context);
                        Context.Entry(Weight).State = System.Data.Entity.EntityState.Modified;
                    }

                    if (Context.SaveChanges() > 0)
                    {
                        Context.Tickets.Add(RegisterTicket(Weight.Id));
                        if (Context.SaveChanges() > 0)
                        {
                            MessageBox.Show(Generals.FindTranslation("RegisteredWithSucess"));
                            wasSaveOk = true;
                        }
                        else
                        {
                            MessageBox.Show(Generals.FindTranslation("RegisterError"));
                            WindowWeighingFormView.WhereWindow = 1;
                            WindowWeighingFormView.InvokeWindowClosing();
                        }
                    }
                    else
                    {
                        MessageBox.Show(Generals.FindTranslation("RegisterError"));
                        WindowWeighingFormView.WhereWindow = 1;
                        WindowWeighingFormView.InvokeWindowClosing();
                    }

                    NotifyPropertyChanged(string.Empty);
                    Reset();

                    if (wasSaveOk)
                    {
                        WindowWeighingFormView.WhereWindow = 1;
                        WindowWeighingFormView.InvokeWindowClosing();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Generals.FindTranslation("RegisterError"));
                    WindowWeighingFormView.WhereWindow = 1;
                    WindowWeighingFormView.InvokeWindowClosing();
                }
            }
        }
        /// <summary>
        /// Access to DataBase to save temporary changes on Weighing Table after every Got Focus on Inputs/Combobox
        /// </summary>
        public static void RegisterTemporaryWeight()
        {
            try
            {
                using (DataModel Context = new DataModel())
                {
                    Weight.RefreshObject(Context);
                    if (Weight.Id == 0 && wasFieldEdited)
                    {
                        Weight = new Weighing();
                        Context.Weighings.Add(Weight);
                        Context.SaveChanges();
                    }
                    else if (Weight.Id > 0 && wasFieldEdited)
                    {
                        Context.Entry(Weight).State = System.Data.Entity.EntityState.Modified;
                        Context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message); // TODO : Add description to pt-PT file
            }
        }
        
        /// <summary>
        /// Access to DataBase to register data on Weighing Table
        /// </summary>
        public void RegisterWeight()
        {
            Weight.Batch = Batch;
            Weight.DeliveryNote = DeliveryNote;
            Weight.DriverID = DriverId;
            Weight.EntityID = EntityId;
            Weight.Gross = GrossWeight;
            Weight.LoadTemperature = LoadTemperature;
            Weight.MaximumWeight = MaxWeight;
            Weight.Net = NetWeight;
            Weight.Observations = Observations;
            Weight.ProductID = ProductId;
            Weight.TankID = TankId;
            Weight.Tare = Tare;
            Weight.TrailerID = TrailerId;
            Weight.TransporterID = TransporterId;
            Weight.VehicleID = VehicleId;
            Weight.manualTare = manualTare;
            Weight.bypassed = bypassed;
            Weight.TareHour = tareHour;
        }

        private Ticket RegisterTicket(int Id)
        {
            int number = 0;
            List<Ticket> tickets = Context.Tickets.ToList();

            if (tickets.Count <= 0)
                number = 1;
            else
                number = (tickets[tickets.Count - 1].Number == null ? 1 : tickets[tickets.Count - 1].Number.Value + 1);

            List<Weighing> weighings = Context.Weighings.ToList();
            if (Id == 0)
            {
                return new Ticket(weighings[weighings.Count - 1], number);
            }
            else if (Id > 0)
            {
                for (var i = 0; i < weighings.Count; i++)
                {
                    if (weighings[i].Id == Id)
                    {
                        return new Ticket(weighings[i], number);
                    }
                }
                return null;
            }
            else
            {
                MessageBox.Show(FindTranslation("NewTicketNotPossibleToRegister"));
                return null;
            }
        }


        /// <summary>
        /// Gets the weighing to be used as Tare 
        /// </summary>
        /// <param name="obj"></param>
        private void TareCommandAction(object obj)
        {
            if (CurrentWeight == "---")
                return;
            Tare = CurrentWeight;

            actualTare = int.Parse(Tare);
        }

        /// <summary>
        /// Starts weighing process by changing the state and hiding/showing the necessary buttons to proceed
        /// </summary>
        /// <param name="obj"></param>
        private void StartWeightCommandAction(object obj)
        {
            ErrorsList.Clear();
            if (!String.IsNullOrEmpty(MaxWeight) && !String.IsNullOrEmpty(Tare) && !String.IsNullOrEmpty(TankNameLabel) && DriverId != null && VehicleId != null && TrailerId != null && !expiredDocs)
            {
                //check if Tare is equal or higher than the minimum accepted value
                if (int.Parse(Tare) >= minTareValue && int.Parse(MaxWeight) <= limitMaxWeight)
                {
                    if (currentWeighingState == (int)weighingState.IDLE)
                    { 
                        if (int.Parse(Tare) != actualTare)
                            manualTare = true;
                        tareHour = DateTime.Now.ToString("HH:mm");
                    }

                    Weight.Tare = Tare;
                    Weight.manualTare = manualTare;
                    Weight.TareHour = tareHour;
                    NotifyPropertyChanged(string.Empty);
                    Weight.RefreshObject(Context);
                    RegisterTemporaryWeight();

                    IsEnabled_tare = false;
                    IsEnabled_btTare = false;
                    IsEnabled_maxWeight = false;

                    IsEnabled_Vehicle = false;
                    IsEnabled_Trailer = false;
                    IsEnabled_Driver  = false;

                    Visibility_btStart = false;
                    Visibility_btReset = false;
                    Visibility_btEnd = true;
                    Visibility_btCancel = true;

                    reader.ForceRelaysStatus((char)Convert.ToInt32(rsState1ANDrsState3, 2));

                    currentWeighingState = (int)weighingState.STATE1;
                }
                else
                {
                   if (int.Parse(Tare) < minTareValue)
                        ErrorsList.Add("(" + DateTime.Now + ") " + FindTranslation("TareMinNotMet") + " (" + minTareValue + "kg)");

                   if (int.Parse(MaxWeight) > limitMaxWeight)
                        ErrorsList.Add("(" + DateTime.Now + ") " + FindTranslation("MaxWeightLimitExceeded") + " (" + limitMaxWeight + "kg)");
                }
            }
            else
            {
                if (String.IsNullOrEmpty(MaxWeight))
                    ErrorsList.Add("(" + DateTime.Now + ") " + FindTranslation("MaxWeightEmpty"));

                if (String.IsNullOrEmpty(Tare))
                    ErrorsList.Add("(" + DateTime.Now + ") " + FindTranslation("TareEmpty"));

                if (expiredDocs)
                    ErrorsList.Add("(" + DateTime.Now + ") " + FindTranslation("ExpiredDocs"));

                if (DriverId == null)
                    ErrorsList.Add("(" + DateTime.Now + ") " + FindTranslation("DriverEmpty"));

                if (VehicleId == null)
                    ErrorsList.Add("(" + DateTime.Now + ") " + FindTranslation("VehicleEmpty"));

                if (TrailerId == null)
                    ErrorsList.Add("(" + DateTime.Now + ") " + FindTranslation("TrailerEmpty"));

                if (String.IsNullOrEmpty(TankNameLabel))
                    ErrorsList.Add("(" + DateTime.Now + ") " + FindTranslation("TankEmpty"));
            }
        }

        private int tanksCounter = 0;
        private List<int> localTanksList = new List<int>();
        /// <summary>
        /// Adds Tanks for user to select
        /// </summary>
        private void AddTankCommandAction(object obj)
        {
            if (TankId.HasValue)
            {
                using (DataModel Context = new DataModel())
                {
                    TankNameLabel += Context.Tanks.Find(TankId).Name + ", ";
                    tanksCounter++;
                    if (tanksCounter % 3 == 0)
                        TankNameLabel += "\n";
                    localTanksList.Add(TankId.Value);

                    NotifyPropertyChanged("TankNameLabel");
                }
            }
        }

        private void OpenInsertWindowCommandAction(object obj)
        {
            try
            {
                Window DetailWindow = new Window();
                setTypesForWindowShowAndUpdateLists(obj, false);
                DetailWindow = Activator.CreateInstance(WindowToShow) as Window;
                DetailWindow.DataContext = Activator.CreateInstance(ViewModelToShow, 0);
                DetailWindow.ShowDialog();
                
                if (!DetailWindow.IsActive)
                {
                    setTypesForWindowShowAndUpdateLists(obj, true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void setTypesForWindowShowAndUpdateLists(object obj, bool isUpdatingList)
        {
            var temporaryDriversList = DriversList;
            var temporaryClientsList = ClientsList;
            var temporaryProductsList = ProductsList;
            var temporaryTanksList = TanksList;
            var temporaryTrailersList = TrailersList;
            var temporaryTransportersList = TransportersList;
            var temporaryVehiclesList = VehiclesList;

            switch (obj)
            {
                case "Drivers":
                    WindowToShow = typeof(WindowDriversFormView);
                    ViewModelToShow = typeof(WindowDriversFormViewModel);
                    if (isUpdatingList)
                    {
                        DriversList = Context.Drivers.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                        if (temporaryDriversList.Count != DriversList.Count)
                        {
                            DriverId = Context.Drivers.Where(x => !x.IsInactive).OrderByDescending(x => x.Id).ToList().First().Id;
                            NotifyPropertyChanged(string.Empty);
                            NotifyPropertyChanged("DriverId");
                        }
                    }
                    break;
                case "Entities":
                    WindowToShow = typeof(WindowEntitiesFormView);
                    ViewModelToShow = typeof(WindowEntitiesFormViewModel);
                    if (isUpdatingList)
                    {
                        ClientsList = Context.Entities.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                        if (temporaryClientsList.Count != ClientsList.Count)
                        {
                            EntityId = Context.Entities.Where(x => !x.IsInactive).OrderByDescending(x => x.Id).ToList().First().Id;
                            NotifyPropertyChanged(string.Empty);
                            NotifyPropertyChanged("EntityId");
                        }
                    }
                    break; 
                case "Products":
                    WindowToShow = typeof(WindowProductsFormView);
                    ViewModelToShow = typeof(WindowProductsFormViewModel);
                    if (isUpdatingList)
                    {
                        ProductsList = Context.Products.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                        if (temporaryProductsList.Count != ProductsList.Count)
                        {
                            ProductId = Context.Products.Where(x => !x.IsInactive).OrderByDescending(x => x.Id).ToList().First().Id;
                            NotifyPropertyChanged(string.Empty);
                            NotifyPropertyChanged("ProductId");
                        }
                    }
                    break;
                case "Tanks":
                    WindowToShow = typeof(WindowTanksFormView);
                    ViewModelToShow = typeof(WindowTanksFormViewModel);
                    if (isUpdatingList)
                    {
                        TanksList = Context.Tanks.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                        if (temporaryTanksList.Count != TanksList.Count)
                        {
                            TankId = Context.Tanks.Where(x => !x.IsInactive).OrderByDescending(x => x.Id).ToList().First().Id;
                            NotifyPropertyChanged(string.Empty);
                            NotifyPropertyChanged("TankId");
                        }
                    }
                    break;
                case "Tickets":
                    WindowToShow = typeof(WindowTicketFormView);
                    ViewModelToShow = typeof(WindowTicketFormViewModel);
                    break;
                case "Trailers":
                    WindowToShow = typeof(WindowTrailersFormView);
                    ViewModelToShow = typeof(WindowTrailersFormViewModel);
                    if (isUpdatingList)
                    {
                        TrailersList = Context.Trailers.Where(x => !x.IsInactive).OrderBy(x => x.LicensePlate).ToList();
                        if (temporaryTrailersList.Count != TrailersList.Count)
                        {
                            TrailerId = Context.Trailers.Where(x => !x.IsInactive).OrderByDescending(x => x.Id).ToList().First().Id;
                            NotifyPropertyChanged(string.Empty);
                            NotifyPropertyChanged("TrailerId");
                        }
                    }
                    break;
                case "Transporters":
                    WindowToShow = typeof(WindowTransportersFormView);
                    ViewModelToShow = typeof(WindowTransportersFormViewModel);
                    if (isUpdatingList)
                    {
                        TransportersList = Context.Transporters.Where(x => !x.IsInactive).OrderBy(x => x.Name).ToList();
                        if (temporaryTransportersList.Count != TransportersList.Count)
                        {
                            TransporterId = Context.Transporters.Where(x => !x.IsInactive).OrderByDescending(x => x.Id).ToList().First().Id;
                            NotifyPropertyChanged(string.Empty);
                            NotifyPropertyChanged("TransporterId");
                        }
                    }
                    break;
                case "Vehicles":
                    WindowToShow = typeof(WindowVehiclesFormView);
                    ViewModelToShow = typeof(WindowVehiclesFormViewModel);
                    if (isUpdatingList)
                    {
                        VehiclesList = Context.Vehicles.Where(x => !x.IsInactive).OrderBy(x => x.LicensePlate).ToList();
                        if (temporaryVehiclesList.Count != VehiclesList.Count)
                        {
                            VehicleId = Context.Vehicles.Where(x => !x.IsInactive).OrderByDescending(x => x.Id).ToList().First().Id;
                            NotifyPropertyChanged(string.Empty);
                            NotifyPropertyChanged("VehicleId");
                        }
                    }
                    break;
                case "Exit":
                    Generals.GetCurrentWindow().Close();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// It's used to keep track about the weighing state and make individual verifications for each state
        /// </summary>
        /// <returns></returns>
        private async Task CheckWeighingProcessState()
        {
            Application.Current.Dispatcher.Invoke((Action)delegate {   
                if (checkRelaysStatusState[currentWeighingState])
                {
                    GetRelaysStatus();
                    checkRelaysStatusState[currentWeighingState] = false;
                }

                switch (currentWeighingState)
                {
                    case (int)weighingState.STATE1:
                    {   
                        if ((!Relay1State || Relay2State) && alertFired == false)
                        {
                            alertFired = true;
                            string alertMessage = string.Format(FindTranslation("SomethingOddHappened"), "\n", FindTranslation("On"), FindTranslation("Off"), Relay1State ? FindTranslation("On") : FindTranslation("Off"), Relay2State ? FindTranslation("On") : FindTranslation("Off"));
                            MessageBoxResult result = MessageBoxResult.None;

                            try
                            {
                                result = CustomMessageBox.ShowYesNoCancel(alertMessage, FindTranslation("Warning"), FindTranslation("Bypass"), FindTranslation("Cancel"), FindTranslation("Ignore"), MessageBoxImage.Warning);

                                if (result == MessageBoxResult.Yes)
                                {
                                    MessageBoxResult innerResult = MessageBox.Show(FindTranslation("AreYouSureWantToContinueAction"), FindTranslation("Warning"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
                                    if (innerResult == MessageBoxResult.Yes)
                                    {
                                        currentWeighingState = (int)weighingState.BYPASS;
                                        bypassed = true;
                                    }

                                    alertFired = false;
                                    break;
                                }

                                if (result == MessageBoxResult.No)
                                {
                                    ResetCancelWeightAction(new object());
                                    alertFired = false;
                                }

                                if (result == MessageBoxResult.Cancel)
                                {
                                    alertFired = false;
                                }

                            }
                            catch (Exception ex)
                            {
                                ErrorsList.Add("(" + DateTime.Now + ") " + "(" + currentWeighingState + ") " + ex.Message);
                            }
                        }

                        if (Int16.Parse(CurrentWeight) >= turnOnRelay2Weight) // check if the weight meets the condition to turn on Relay2
                        {
                            currentWeighingState = (int)weighingState.STATE2;
                            reader.ForceRelaysStatus((char)Convert.ToInt32(rsState2, 2));
                        }

                        break;
                    }

                    case (int)weighingState.STATE2:
                    {
                        if ((!Relay1State || !Relay2State) && alertFired == false)
                        {
                            string alertMessage = string.Format(FindTranslation("SomethingOddHappened"), "\n", FindTranslation("On"), FindTranslation("On"), Relay1State ? FindTranslation("On") : FindTranslation("Off"), Relay2State ? FindTranslation("On") : FindTranslation("Off"));
                            MessageBoxResult result = MessageBoxResult.None;

                            try
                            {
                                alertFired = true;
                                result = CustomMessageBox.ShowYesNoCancel(alertMessage, FindTranslation("Warning"), FindTranslation("Bypass"), FindTranslation("Cancel"), FindTranslation("Ignore"), MessageBoxImage.Warning);

                                if (result == MessageBoxResult.Yes)
                                {
                                    MessageBoxResult innerResult = MessageBox.Show(FindTranslation("AreYouSureWantToContinueAction"), FindTranslation("Warning"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
                                    if (innerResult == MessageBoxResult.Yes)
                                    {
                                        currentWeighingState = (int)weighingState.BYPASS;
                                        bypassed = true;
                                    }

                                    alertFired = false;
                                    break;
                                }

                                if (result == MessageBoxResult.No)
                                {
                                    ResetCancelWeightAction(new object());
                                    alertFired = false;
                                }

                                if (result == MessageBoxResult.Cancel)
                                {
                                    alertFired = false;
                                }

                            }
                            catch (Exception ex)
                            {
                                ErrorsList.Add("(" + DateTime.Now + ") " + "(" + currentWeighingState + ") " + ex.Message);
                            }
                        }

                        if (Int16.Parse(MaxWeight) - Int16.Parse(CurrentWeight) <= turnOffRelay2Weight) // check if the weight meets the condition to turn off Relay2
                        {
                            currentWeighingState = (int)weighingState.STATE3;
                            reader.ForceRelaysStatus((char)Convert.ToInt32(rsState1ANDrsState3, 2));
                            }

                        break;
                    }

                    case (int)weighingState.STATE3:
                    {
                        if ((!Relay1State || Relay2State) && alertFired == false)
                        {
                            string alertMessage = string.Format(FindTranslation("SomethingOddHappened"), "\n", FindTranslation("On"), FindTranslation("Off"), Relay1State ? FindTranslation("On") : FindTranslation("Off"), Relay2State ? FindTranslation("On") : FindTranslation("Off"));
                            MessageBoxResult result = MessageBoxResult.None;

                            try
                            {
                                alertFired = true;
                                result = CustomMessageBox.ShowYesNoCancel(alertMessage, FindTranslation("Warning"), FindTranslation("Bypass"), FindTranslation("Cancel"), FindTranslation("Ignore"), MessageBoxImage.Warning);

                                if (result == MessageBoxResult.Yes)
                                {
                                    MessageBoxResult innerResult = MessageBox.Show(FindTranslation("AreYouSureWantToContinueAction"), FindTranslation("Warning"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
                                    if (innerResult == MessageBoxResult.Yes)
                                    {
                                        currentWeighingState = (int)weighingState.BYPASS;
                                        bypassed = true;
                                    }

                                    alertFired = false;
                                    break;
                                }

                                if (result == MessageBoxResult.No)
                                {
                                    ResetCancelWeightAction(new object());
                                    alertFired = false;
                                }

                                if (result == MessageBoxResult.Cancel)
                                {
                                    alertFired = false;
                                }

                            }
                            catch (Exception ex)
                            {
                                ErrorsList.Add("(" + DateTime.Now + ") " + "(" + currentWeighingState + ") " + ex.Message);
                            }
                        }

                        if (Int16.Parse(MaxWeight) - Int16.Parse(CurrentWeight) <= turnOffRelay1Weight) // check if the weight meets the condition to turn off Relay1
                        {
                            currentWeighingState = (int)weighingState.STATE4;
                            reader.ForceRelaysStatus((char)Convert.ToInt32(rsIdleANDrsState4, 2));
                        }

                        break;
                    }

                    case (int)weighingState.STATE4:
                    {
                        if ((Relay1State || Relay2State) && alertFired == false)
                        {
                            string alertMessage = string.Format(FindTranslation("SomethingOddHappened"), "\n", FindTranslation("Off"), FindTranslation("Off"), Relay1State ? FindTranslation("On") : FindTranslation("Off"), Relay2State ? FindTranslation("On") : FindTranslation("Off"));
                            MessageBoxResult result = MessageBoxResult.None;

                            try
                            {
                                alertFired = true;
                                result = CustomMessageBox.ShowYesNoCancel(alertMessage, FindTranslation("Warning"), FindTranslation("Bypass"), FindTranslation("Cancel"), FindTranslation("Ignore"), MessageBoxImage.Warning);

                                if (result == MessageBoxResult.Yes)
                                {
                                    MessageBoxResult innerResult = MessageBox.Show(FindTranslation("AreYouSureWantToContinueAction"), FindTranslation("Warning"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
                                    if (innerResult == MessageBoxResult.Yes)
                                    {
                                        currentWeighingState = (int)weighingState.BYPASS;
                                        bypassed = true;
                                    }

                                    alertFired = false;
                                    break;
                                }

                                if (result == MessageBoxResult.No)
                                {
                                    ResetCancelWeightAction(new object());
                                    alertFired = false;
                                }

                                if (result == MessageBoxResult.Cancel)
                                {
                                    alertFired = false;
                                }
                            }
                            catch (Exception ex)
                            {
                                ErrorsList.Add("(" + DateTime.Now + ") " + "(" + currentWeighingState + ") " + ex.Message);
                            }
                        }

                        break;
                    }

                    case (int)weighingState.BYPASS:
                    {
                        //Visibility_btRelays = true;
                        break;
                    }
                }
            });
        }

        /// <summary>
        /// Informs the View that the property have been change
        /// ADDITIONAL FUNCTION: keeps updating the textboxes of Gross and Net Weight and also fires the method CheckWeighingState() 
        /// to keep tracking the state while the weighing is not finished
        /// </summary>
        /// <param name="PropertyName"></param>
        protected void NotifyPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));

            if (currentWeighingState >= (int)weighingState.STATE1 && PropertyName == "CurrentWeight")
            {
                if (currentWeighingState == (int)weighingState.STATE1 && IsEnabled_tare && !String.IsNullOrEmpty(GrossWeight))
                    StartWeightCommandAction(null);

                if (String.IsNullOrEmpty(MaxWeight) || String.IsNullOrEmpty(Tare))
                {
                    currentWeighingState = (int)weighingState.IDLE;
                    IsEnabled_tare = true;
                    IsEnabled_btTare = true;
                    IsEnabled_maxWeight = true;
                    Visibility_btStart = true;
                    Visibility_btReset = true;
                    Visibility_btEnd = false;
                    Visibility_btCancel = false;
                    NotifyPropertyChanged(string.Empty);
                    ResetRelaysStatus();
                    MessageBox.Show(FindTranslation("ErrorStartingWeight"));
                    return;
                }
                else
                {
                    GrossWeight = (int.Parse(CurrentWeight) + int.Parse(Tare)).ToString();
                    NetWeight = CurrentWeight;
                    CheckWeighingProcessState();
                }
            }
            
            if (PropertyName == "VehicleId" || PropertyName == "TrailerId" || PropertyName == "DriverId")
                GetAlerts();
        }

        /// <summary>
        /// Continuously fetch the weight shown in physical reader
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Reader_GetWeight(object sender, WeightDataEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(Tare))
                {
                    CurrentWeight = (int.Parse(e.weight) - int.Parse(Tare)).ToString();
                    Thread.Sleep(200);
                }
                else
                {
                    CurrentWeight = e.weight;
                    Thread.Sleep(200);
                }
            }
            catch (Exception ex)
            {
                ErrorsList.Add("(" + DateTime.Now + ") " + ex.Message);
            }

            NotifyPropertyChanged("CurrentWeight");
            Thread.Sleep(200);
            if(isConnectionOpen)
                reader.GetWeight();
        }

        /// <summary>
        /// Gets the alerts refering to expired dates 
        /// </summary>
        private async void GetAlerts()
        {
            AlertsList.Clear();

            DateTime alertThreshold = DateTime.Now.AddMonths(selectedAlertThreshold);

            var alertsDrivers = Context.Drivers.Where(x => (x.DateDriversLicense < alertThreshold || x.DateADR < alertThreshold || x.DateID < alertThreshold || x.DateSecurityInduction < alertThreshold) && x.Id == DriverId && !x.IsInactive).Select(x => new { x.Name, x.DateDriversLicense, x.DateADR, x.DateID, x.DateSecurityInduction }).ToList();
            var alertsVehicles = Context.Vehicles.Where(x => (x.DateInsurance < alertThreshold || x.DateADR < alertThreshold) && x.Id == VehicleId && !x.IsInactive).Select(x => new { x.LicensePlate, x.DateInsurance, x.DateADR }).ToList();
            var alertsTrailers = Context.Trailers.Where(x => (x.DateInsurance < alertThreshold || x.DateADR < alertThreshold) && x.Id == TrailerId && !x.IsInactive).Select(x => new { x.LicensePlate, x.DateInsurance, x.DateADR }).ToList();

            if (alertsDrivers.Count > 0)
            {
                foreach (var driver in alertsDrivers)
                {
                    if (driver.DateDriversLicense < alertThreshold)
                        AlertsList.Add(FindTranslation("DriverName_") + " " + driver.Name + " | " + FindTranslation("LicenseExpireIn") + " " + (driver.DateDriversLicense - DateTime.Now).Days + " " + FindTranslation("Days"));

                    if (driver.DateADR < alertThreshold)
                        AlertsList.Add(FindTranslation("DriverName_") + " " + driver.Name + " | " + FindTranslation("ADRExpireIn") + " " + (driver.DateADR - DateTime.Now).Days + " " + FindTranslation("Days"));

                    if (driver.DateID < alertThreshold)
                        AlertsList.Add(FindTranslation("DriverName_") + " " + driver.Name + " | " + FindTranslation("IDExpireIn") + " " + (driver.DateID - DateTime.Now).Days + " " + FindTranslation("Days"));

                    if (driver.DateSecurityInduction < alertThreshold)
                        AlertsList.Add(FindTranslation("DriverName_") + " " + driver.Name + " | " + FindTranslation("SecurityInductionExpireIn") + " " + (driver.DateSecurityInduction - DateTime.Now).Days + " " + FindTranslation("Days"));
                }
            }

            if (alertsVehicles.Count > 0)
            {
                foreach (var vehicle in alertsVehicles)
                {
                    if (vehicle.DateInsurance < alertThreshold)
                        AlertsList.Add(FindTranslation("Vehicle_") + " " + vehicle.LicensePlate + " | " + FindTranslation("InsuranceExpireIn") + " " + (vehicle.DateInsurance - DateTime.Now).Days + " " + FindTranslation("Days"));

                    if (vehicle.DateADR < alertThreshold)
                        AlertsList.Add(FindTranslation("Vehicle_") + " " + vehicle.LicensePlate + " | " + FindTranslation("ADRExpireIn") + " " + (vehicle.DateADR - DateTime.Now).Days + " " + FindTranslation("Days"));
                }
            }

            if (alertsTrailers.Count > 0)
            {
                foreach (var trailer in alertsTrailers)
                {
                    if (trailer.DateInsurance < alertThreshold)
                        AlertsList.Add(FindTranslation("Trailer_") + " " + trailer.LicensePlate + " | " + FindTranslation("InsuranceExpireIn") + " " + (trailer.DateInsurance - DateTime.Now).Days + " " + FindTranslation("Days"));

                    if (trailer.DateADR < alertThreshold)
                        AlertsList.Add(FindTranslation("Trailer_") + " " + trailer.LicensePlate + " | " + FindTranslation("ADRExpireIn") + " " + (trailer.DateADR - DateTime.Now).Days + " " + FindTranslation("Days"));
                }
            }

            expiredDocs = (alertsDrivers.Any(x => x.DateDriversLicense < DateTime.Now || x.DateADR < DateTime.Now || x.DateID < DateTime.Now || x.DateSecurityInduction < DateTime.Now) || alertsVehicles.Any(x => (x.DateInsurance < DateTime.Now || x.DateADR < DateTime.Now)) || alertsTrailers.Any(x => (x.DateInsurance < DateTime.Now || x.DateADR < DateTime.Now))) ? true : false;
        }

        /// <summary>
        /// Checks the relayboard relays statuses
        /// </summary>
        private async void GetRelaysStatus()
        {
            #region Description
            /* Criar, ao carregar os selectedRelay1 e 2, um metodo que associe os estado ao binário correspondente
             * ex: string[] currentBinaryState  -> se os selectedRelays forem 8 e 7 -> selectedRelay1(R1) e selectedRelay2(R2) respetivamente
             *                                                                     ???? 1 1  [R2] [R1]
             *                                  -> IDLE:   currentBinaryState[0] | ???? 1 1    1    1
             *                                  -> State1: currentBinaryState[1] | ???? 1 1    1    0
             *                                  -> State2: currentBinaryState[2] | ???? 1 1    0    0
             *                                  -> State3: currentBinaryState[3] | ???? 1 1    1    0
             *                                  -> State4: currentBinaryState[4] | ???? 1 1    1    1
             *                                  -> Bypass: currentBinaryState[5] | ???? 1 1    1    1
             *
             * Ler Relés
             *   Verificar estado e comparar
             *       se IDLE -> 1111 1111
             *           se estado != 1111 1111
             *               ResetRelayStatus()
             *       se State1 -> 1111 1110
             *          se estado != 1111 1110
             *           -> ler outra vez
             *       se state2 -> 1111 1100
             *          se estado != 1111 1100
             *           -> ler outra vez
             *       se State3 -> 1111 1110
             *          se estado != 1111 1110
             *           -> ler outra vez
             *       se State4 -> 1111 1111
             *           se estado != 1111 1111
             *            -> ler outra vez
             *
             *   Após "5" leituras -> Alerta com a WPFCustomMessageBox -> Sobrescrever | Ignorar | Cancelar   
             *
             */
            #endregion
            try
            {
                string relayStatus = "";
                bool statusOK = false;
                int counter = 0;

                do
                {
                    relayStatus = reader.GetRelaysStatus();
                    Thread.Sleep(200);

                    switch (currentWeighingState)
                    {
                        case (int)weighingState.IDLE:
                        case (int)weighingState.STATE4:
                            if (relayStatus == rsIdleANDrsState4)
                                statusOK = true;
                            else
                                reader.ResetRelaysStatus();

                            break;
                        case (int)weighingState.STATE2:
                            if (relayStatus == rsState2)
                                statusOK = true;
                            else
                                //send char equivalent to desired relays status
                                reader.ForceRelaysStatus((char)Convert.ToInt32(rsState2, 2));

                            break;
                        case (int)weighingState.STATE1:
                        case (int)weighingState.STATE3:
                            if (relayStatus == rsState1ANDrsState3)
                                statusOK = true;
                            else
                                //send char equivalent to desired relays status
                                reader.ForceRelaysStatus((char)Convert.ToInt32(rsState1ANDrsState3, 2));

                            break;
                    }

                    counter++;
                } while (!statusOK && counter < 5);

                relayStatus.ToCharArray();
                Relay1State = relayStatus[selectedRelay1 - 1] == '0' ? true : false;
                Relay2State = relayStatus[selectedRelay2 - 1] == '0' ? true : false;
                //GetStatusLabel = "";
                //foreach (var i in relayStatus)
                //    GetStatusLabel += i.ToString() + " ";
                // Console.WriteLine(GetStatusLabel);
            }
            catch (Exception ex)
            {
                ErrorsList.Add("(" + DateTime.Now + ") " + ex.Message);
            }
        }

        public void ResetRelaysStatus()
        {
            try
            {
                reader.ResetRelaysStatus();
                Console.WriteLine("relays reset");
            }
            catch (Exception ex)
            {
                ErrorsList.Add("(" + DateTime.Now + ") " + ex.Message);
            }
        }

        public static void Dispose()
        {
            try
            {
                if (isConnectionOpen)
                {
                    isConnectionOpen = false;
                    Thread.Sleep(200);
                    reader.Close();
                }
                    
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }
        #region Stress TESTS
        /*void StressTest1()
        {
            int currentTestStep = 0;
            string lixo = "";
            string lixoGeral = "";
            do
            {
                lixo = "";
                // Start over / Reset
                if (leitor != null)
                {
                    leitor.DeviceCommunications.Close();
                    Thread.Sleep(50);
                    leitor.Open();

                    //leitor.WeighingModule.StartGrossWeighing();
                }
                else if (leitor == null)
                {
                    leitor = iot.GetDevice("W17");
                    leitor.Open();
                    //leitor.WeighingModule.StartGrossWeighing();
                }

                ResetRelaysStatus();

                MaxWeight = "2500";
                Tare = "500";
                CurrentWeight = "0";

                int currentWeight = 0;
                int LocalCurrentWeightState = 0;

                lixo += "(" + DateTime.Now.ToString() + ")\tMaxWeight = " + MaxWeight;
                lixo += "\n(" + DateTime.Now.ToString() + ")\tTare = " + Tare;
                lixo += "\n(" + DateTime.Now.ToString() + ")\tCurrentWeight = " + CurrentWeight;
                lixo += "\n(" + DateTime.Now.ToString() + ")\tcurrentWeighingState = " + currentWeighingState;


                // Operation Start

                Thread.Sleep(100);
                // get values from Configuration.xml
                minTareValue = !String.IsNullOrEmpty(config.Attribute("MinTareWeight")?.Value) ? Int16.Parse(config.Attribute("MinTareWeight")?.Value) : minTareValue;
                turnOnRelay2Weight = !String.IsNullOrEmpty(config.Attribute("TurnOnRelay2Weight")?.Value) ? Int16.Parse(config.Attribute("TurnOnRelay2Weight")?.Value) : turnOnRelay2Weight;
                turnOffRelay2Weight = !String.IsNullOrEmpty(config.Attribute("TurnOffRelay2Weight")?.Value) ? Int16.Parse(config.Attribute("TurnOffRelay2Weight")?.Value) : turnOffRelay2Weight;
                turnOffRelay1Weight = !String.IsNullOrEmpty(config.Attribute("TurnOffRelay1Weight")?.Value) ? Int16.Parse(config.Attribute("TurnOffRelay1Weight")?.Value) : turnOffRelay1Weight;
                int i = 1;
                while (i>0)
                {
                    leitor.DeviceCommunications.Send("0x53 0x59 0xff 0xff 0xff 0xff 0xff 0xff 0xff 0xff 0x0d", -1); // desliga
                    Thread.Sleep(200);

                    for (int hex1 = 0; hex1 <= 15; hex1++)
                    {
                        for (int hex2 = 0; hex2 <= 15; hex2++)
                        {

                            string toSend = "0x53 0x59 0x" + hex1.ToString("X") + hex2.ToString("X") + " 0xff 0xff 0xff 0xff 0xff 0xff 0xff 0x0d";
                            leitor.DeviceCommunications.Send(toSend, -1); // desliga
                            Thread.Sleep(50);
                            string result1 = "";
                            bool[] test1 = leitor.RelayBoardModule.GetStatusWithTimeout(50);
                            Console.Write("SY " + hex1.ToString("X") + hex2.ToString("X") + "\tSU: ");
                            int j = 8;
                            foreach (bool x in test1)
                            {
                                string estado = (x == false ? "1" : "0");
                                Console.Write("[R" + j + "] " + estado + " ");
                                j--;
                            }

                            //Console.Write("\tPeso: " + leitor.DeviceCommunications.Send("0x53 0x42 0x0d", 50));

                            Console.Write("\n");


                        }
                    }

                    leitor.DeviceCommunications.Send("0x53 0x59 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x0d", -1); // liga
                    Thread.Sleep(200);
                    i--;
                    string result = "";
                    bool[] test = leitor.RelayBoardModule.GetStatusWithTimeout(200);
                    foreach (bool x in test)
                        Console.Write("SU: " + x + " ");
                    Console.Write("\n");

                }

                leitor.RelayBoardModule.TurnOn(selectedRelay1);

                // Passo 1
                currentWeighingState = (int)weighingState.STATE1;
                Thread.Sleep(100);


                CheckWeighingProcessState();
                Thread.Sleep(100);
                lixo += "\n(" + DateTime.Now.ToString() + ")\tCurrentWeight = " + currentWeight.ToString();
                lixo += "\n(" + DateTime.Now.ToString() + ")\tcurrentWeighingState = " + currentWeighingState;
                Thread.Sleep(100);


                do
                {
                    currentWeight += 20;
                    CurrentWeight = currentWeight.ToString();
                    Thread.Sleep(20);
                    CheckWeighingProcessState();
                    lixo += "\n(" + DateTime.Now.ToString() + ")\tCurrentWeight = " + CurrentWeight;
                    lixo += "\n(" + DateTime.Now.ToString() + ")\tcurrentWeighingState = " + currentWeighingState;
                    LocalCurrentWeightState = currentWeighingState;

                    if (Int16.Parse(MaxWeight) <= Int16.Parse(CurrentWeight))
                    {
                        LocalCurrentWeightState = 0;
                    }

                    //lixo += "\n(" + DateTime.Now.ToString() + ")\tPeso (Raw): " + leitor.WeighingModule.GetGrossWeight().Raw;

                } while (LocalCurrentWeightState != 0);

                Thread.Sleep(100);

                Console.WriteLine("\n" + currentTestStep + ": " + lixo);
                Console.WriteLine("\nCurrentCount: " + currentTestStep);
                currentTestStep++;
                lixoGeral += "\n" + lixo;
                leitor.WeighingModule.StopContinuousWeighing();

            } while (currentTestStep < 500);

            Console.WriteLine(lixoGeral);
        }

    */
        void StressTest2()
        {
            Console.WriteLine("Start StressTest2");
            //if (leitor.DeviceCommunications.IsConnectionOpenned)
            //    leitor.DeviceCommunications.Close();

            string data = "";
            SerialPort serialPort = new SerialPort();
            
                serialPort = new SerialPort();
                serialPort.PortName = "COM5";
                serialPort.BaudRate = 9600;
                serialPort.DataBits = 8;
                serialPort.Parity = Parity.None;
                serialPort.StopBits = StopBits.One;
                serialPort.ReadTimeout = 200;

            try
            {
                if (!serialPort.IsOpen)
                    serialPort.Open();

                byte[] byteCommand;
                string myChar = ""; myChar += (char)13;

                for (int i = 0; i < 256; i++)
                {
                    data = "SY" + (char)i + "ÿÿÿÿÿÿÿ" + myChar;
                
                    //data = data.Replace("\\r", myChar);
                    ConvertFromASCIIToBinary(data, out byteCommand);
                    //serialPort.Write(data);

                    Regex regex = new Regex(Regex.Unescape("([A-Z])+"));

                    
                    string dataReceived = "";
                    serialPort.Write(byteCommand, 0, byteCommand.Length);

                    Thread.Sleep(200);

                    data = "SU" + myChar;
                    ConvertFromASCIIToBinary(data, out byteCommand);
                    serialPort.Write(byteCommand, 0, byteCommand.Length);

                    DateTime date = DateTime.Now.AddSeconds(1);
                    byte[] tmpByte = new byte[10];
                    while (true)
                    {
                        //while (!dataReceived.Contains((char)13) && !dataReceived.Contains((char)10))
                        try
                        {
                            serialPort.Read(tmpByte, 0, 10).ToString(); //dataReceived += 
                            // space   C8 C7 ... C1 \r

                        }
                        catch (TimeoutException tex)
                        {
                            Console.WriteLine("\n****************************" + "\nERROR: " + i + "\nMessage: " + tex.Message + "\n****************************");
                            break;
                        }
                        finally
                        {
                            
                        }

                        if (tmpByte[9] == (char)13)
                            break;
                        if (regex.Matches(dataReceived ?? "").Count != 0)
                            break;
                    }
                    //Convert.ToString(byteArray[20], 2).PadLeft(8, '0');
                    Console.WriteLine("Bytes: " + Convert.ToString(tmpByte[1], 2).PadLeft(8, '0'));
                    Console.WriteLine("Read Existing: " + dataReceived);
                    Thread.Sleep(200);

                    //data = "SZ" + i.ToString("D6") + myChar;
                    //ConvertFromASCIIToBinary(data, out byteCommand);
                    //serialPort.Write(byteCommand, 0, byteCommand.Length);

                    //Thread.Sleep(200);

                    regex = new Regex(Regex.Unescape("([0-9])+"));
                    data = "SB" + myChar;
                    ConvertFromASCIIToBinary(data, out byteCommand);
                    serialPort.Write(byteCommand, 0, byteCommand.Length);
                    dataReceived = "";
                    while (!dataReceived.Contains((char)13) && !dataReceived.Contains((char)10) && DateTime.Now < date)
                    {
                        dataReceived += serialPort.ReadExisting();
                    }

                    //dataReceived = dataReceived.Trim(' ', '+');
                    //string tmp = "";
                    //tmp = "SZ" + Int32.Parse(dataReceived).ToString("####.##") + (char)13;
                    //serialPort.Write(tmp);
                    //if (regex.Matches(dataReceived ?? "").Count != 0)
                    //{
                    //    OnDataReceived?.Invoke(this, dataReceived);
                    //    break;
                    //}
                    
                    Console.WriteLine("Peso: " + dataReceived);

                    Console.WriteLine("Leitura: " + i.ToString("D6"));

                    Thread.Sleep(200);

                }
                //DateTime date = DateTime.Now.AddSeconds(1);
                //byte[] tmpByte;
                //while (true)
                //{
                //    while (!dataReceived.Contains((char)13) && !dataReceived.Contains((char)10) && DateTime.Now < date)
                //    {
                //        //tmpByte = new byte[20];
                //        //dataReceived += serialPort.Read(tmpByte, 0, 20);
                //        dataReceived += serialPort.ReadExisting();

                //        //Console.WriteLine(tmpByte);
                //    }
                //    //if (regex.Matches(dataReceived ?? "").Count != 0)
                //    //{
                //    //    OnDataReceived?.Invoke(this, dataReceived);
                //    //    break;
                //    //}
                //    Console.WriteLine(DateTime.Now);
                //    break;
                //}


            }
            catch (Exception ex)
            {
                Console.WriteLine("Open() error: " + ex.Message + "\n\n");
            }
            Console.WriteLine("Finish StressTest2\n\n");

        }

        void StressTest3()
        {
            for (int alex = 0; alex < 500000; alex++)
            {
                string peso = CurrentWeight;
                currentWeighingState = (int)weighingState.IDLE;
                GetRelaysStatus();
                currentWeighingState = (int)weighingState.STATE1;
                GetRelaysStatus();
                currentWeighingState = (int)weighingState.STATE2;
                GetRelaysStatus();
                currentWeighingState = (int)weighingState.STATE3;
                GetRelaysStatus();
                currentWeighingState = (int)weighingState.STATE4;
                GetRelaysStatus();
            }
        }
        void ConvertFromASCIIToBinary(string data, out byte[] byteCommand)
        {
            int position = 0;
            byteCommand = new byte[data.Length + 1];

            byteCommand = Encoding.Default.GetBytes(data.ToCharArray());
        }

        void SendData(string data, int timeout)
        {
            
            byte[] byteCommand;
            string myChar = ""; myChar += (char)13;
            data = data.Replace("\\r", myChar);
            ConvertFromASCIIToBinary(data, out byteCommand);
            //serialPort.Write(data);
            //DataReceivedTest(byteCommand, timeout);
        }
        #endregion
    }
}
