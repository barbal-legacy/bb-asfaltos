﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static BB_TerminalAsfaltos.Classes.Generals;

namespace BB_TerminalAsfaltos.ViewModels
{
    class PageTicketsViewModel : PageBaseViewModel
    {
        public PageTicketsViewModel() : base(FindTranslation("Tickets"), typeof(Ticket), typeof(WindowTicketFormView), typeof(WindowTicketFormViewModel))
        {
            ExportButtonVisibility = Visibility.Visible;
            AddButtonVisibility = Visibility.Visible;
            DateFilterVisibility = Visibility.Visible;
            DeleteButtonVisibility = Visibility.Collapsed;

            ColumnsList = new ObservableCollection<DataGridColumn>()
            {
                //new DataGridTextColumn
                //{
                //    Binding=new Binding("Id"),
                //    Header=FindTranslation("ID"),
                //    ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                //},
                new DataGridTextColumn
                {
                    Binding=new Binding("Number"),
                    Header=FindTranslation("TicketNumber"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("CreatedDate") { StringFormat = "dd/MM/yyyy"},
                    Header=FindTranslation("CreatedDate"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Vehicle.LicensePlate"),
                    Header=FindTranslation("LicensePlateVehicle"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Trailer.LicensePlate"),
                    Header=FindTranslation("LicensePlateTrailer"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },new DataGridTextColumn
                {
                    Binding=new Binding("Driver.Name"),
                    Header=FindTranslation("Driver"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Transporter.Name"),
                    Header=FindTranslation("Transporter"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Entity.Name"),
                    Header=FindTranslation("Entity"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("DeliveryNote"),
                    Header=FindTranslation("DeliveryNote"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("TanksID"),
                    Header=FindTranslation("Tank"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Product.Name"),
                    Header=FindTranslation("Products"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("LoadTemperature"),
                    Header=FindTranslation("LoadTemperature"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Batch"),
                    Header=FindTranslation("Batch"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Observations"),
                    Header=FindTranslation("Observations"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Tare"),
                    Header=FindTranslation("Tare"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("TareHour"),
                    Header=FindTranslation("Hour"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Gross"),
                    Header=FindTranslation("Gross"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("Net"),
                    Header=FindTranslation("Net"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },
                new DataGridTextColumn
                {
                    Binding=new Binding("MaximumWeight"),
                    Header=FindTranslation("MaxWeight"),
                    ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                },

            };
            
            // Header and FrameworkElementFactory Initialization
            DataGridTemplateColumn printCol1 = new DataGridTemplateColumn { Header = FindTranslation("PrintReceipt") };
            FrameworkElementFactory btn_creation1 = new FrameworkElementFactory(typeof(Button));
            FrameworkElementFactory imageFramework = new FrameworkElementFactory(typeof(Image));
            
            // Image Print Initialization as a BitmapImage and set Image UriSource to corresponding path
            BitmapImage icon = new BitmapImage();
            icon.BeginInit();
            icon.UriSource = new Uri("pack://application:,,,/Images/icons8-print-50.png", UriKind.RelativeOrAbsolute); // TODO: Atualizar o caminho para ser possível usar Paths Relativos
            icon.EndInit();

            // Database connection to Refresh Collection Ticket Data
            RefreshList();
            
            // Image Association to FrameworkElementFactory and Printer Image Properties set value
            imageFramework.SetValue(Image.SourceProperty, icon);
            imageFramework.SetValue(Image.HeightProperty, 30d);
            imageFramework.SetValue(Image.WidthProperty, 30d);
            imageFramework.SetValue(Image.VerticalAlignmentProperty, VerticalAlignment.Top);
            
            // Printer Image Append to button creation
            btn_creation1.AppendChild(imageFramework);
            
            // Button Style set value (Transparent Properties for now)
            btn_creation1.SetValue(Button.BackgroundProperty, Brushes.Transparent);
            btn_creation1.SetValue(Button.BorderBrushProperty, Brushes.Transparent);
            
            // Event Creation applied to the button
            btn_creation1.AddHandler(System.Windows.Controls.Primitives.ButtonBase.ClickEvent, new RoutedEventHandler(PreviewCommandHandler));
            
            // Cell Template Creation and Insertion of the Header and Button to the first position of the Observable
            DataTemplate cellTemplate1 = new DataTemplate();
            cellTemplate1.VisualTree = btn_creation1;
            printCol1.CellTemplate = cellTemplate1;
            ColumnsList.Insert(0, printCol1); 
        }

        public void PreviewCommandHandler(object sender, RoutedEventArgs e)
        {
            PreviewCommandAction(PageListingsView.ticketsDataGridPreview.SelectedItem);
        }

        protected override bool CollectionFilter(object obj)
        {
            //if (((Ticket)obj).CreatedDate.Date <= DateFrom.Date || ((Ticket)obj).CreatedDate.Date >= DateTo.Date)
            //    return false;

            return !((Ticket)obj).IsInactive
               && (((Ticket)obj).Batch?.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).Observations?.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).Number?.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).DeliveryNote?.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).LoadTemperature?.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).Driver?.Name.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).Entity?.Name.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).Product?.Name.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).Tank?.Name.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).Trailer?.LicensePlate.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).Transporter?.Name.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
               || ((Ticket)obj).Vehicle?.LicensePlate.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0);
        }

        protected override void RefreshList()
        {
            try
            {
                using (DataModel Context = new DataModel())
                {
                    DateTime localDateTo = DateTo.AddDays(1);
                    //ItemsCollection = new ObservableCollection<dynamic>(Context.Tickets.Include("UserOpened").Include("UserClosed").Include("Client").Include("Transporter").OrderByDescending(x => x.Id).ToDynamicList<dynamic>());
                    ItemsCollection = new ObservableCollection<dynamic>( 
                        Context.Tickets
                        .Include("Driver")
                        .Include("Entity")
                        .Include("Product")
                        .Include("Tank")
                        .Include("Trailer")
                        .Include("Transporter")
                        .Include("Vehicle")
                        .Include("Weighing")
                        .OrderByDescending(x => x.Id).Where(x => x.CreatedDate >= DateFrom && x.CreatedDate <= localDateTo && !x.IsInactive).ToDynamicList<dynamic>());
                
                    NotifyPropertyChanged("ItemsCollection");
                    MyView = CollectionViewSource.GetDefaultView(ItemsCollection);
                    MyView.Filter = PreFilter;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao carregar página dos tickets!");
            }

        }

    }
}
