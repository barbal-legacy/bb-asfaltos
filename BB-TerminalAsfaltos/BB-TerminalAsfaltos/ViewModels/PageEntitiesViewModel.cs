﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using static BB_TerminalAsfaltos.Classes.Generals;

namespace BB_TerminalAsfaltos.ViewModels
{
    class PageEntitiesViewModel : PageBaseViewModel
    {
       
        public PageEntitiesViewModel() : base(FindTranslation("Entities"), typeof(Entity), typeof(WindowEntitiesFormView), typeof(WindowEntitiesFormViewModel))
        {
            DateFilterVisibility = Visibility.Collapsed;
            DeleteButtonVisibility = Visibility.Collapsed;

            ColumnsList = new ObservableCollection<DataGridColumn>()
                    {
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Id"),
                            Header=FindTranslation("ID"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Name"),
                            Header=FindTranslation("Name"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        
                    };
        }

        protected override bool CollectionFilter(object obj)
        {
            return !((Entity)obj).IsInactive
               && ((Entity)obj).Name?.IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}
