﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace BB_TerminalAsfaltos.ViewModels
{
    class WindowUsersFormViewModel : WindowBaseViewModel
    {
        public WindowUsersFormViewModel(Type ObjectType) : base(ObjectType)
        {
        }

        //public bool IsNotNewRecord { get; set; }
        //public string Password1 { get; set; }
        //public string Password2 { get; set; }
        //public bool DoesAdminExists { get; set; } = true;

        //public ClickedCommand<object> ChangePasswordCommand { get; set; }

        //public WindowUsersFormViewModel(int Id) :base(typeof(User))
        //{
        //    WindowTitle = Generals.FindTranslation("Users");
        //    if (Id == 0)
        //    {
        //        IsNewRecord = true;
        //        Item = new User();
        //    }
        //    else
        //    {
        //        using (DataModel Context = new DataModel())
        //        {
        //            Item = Context.Users.Find(Id);
        //        }
        //    }
        //    IsNotNewRecord = !IsNewRecord;
        //    OnBeforeSave += WindowUsersFromViewModel_OnBeforeSave;
        //    ChangePasswordCommand = new ClickedCommand<object>(ChangePassword);
        //}
        //public WindowUsersFormViewModel(bool DoesAdminExists) : base(typeof(User))
        //{
        //    this.DoesAdminExists = DoesAdminExists;
        //    IsNewRecord = true;
        //    OnBeforeSave += WindowUsersFromViewModel_OnBeforeSave;
        //    Item = new User() { IsAdmin = true } ;
        //}

        //private void WindowUsersFromViewModel_OnBeforeSave(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(Password1))
        //    {
        //        ((User)Item).Password = Encryption.EncryptRijndael(Password1);
        //    }
        //}

        //protected override bool CanSave()
        //{
        //    if (Password1!=Password2 && IsNewRecord)
        //    {
        //        MessageBox.Show(Generals.FindTranslation("IncorrectPassWord"));
        //        return false;
        //    }
        //    using (DataModel Context = new DataModel())
        //    {
        //        int Id = ((User)Item).Id;
        //        string username = ((User)Item).Username;
        //        if (Context.Users.Count(x => x.Username == username && x.Id != Id) > 0)
        //        {
        //            MessageBox.Show(Generals.FindTranslation("UserAlreadyExists"));
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        //private void ChangePassword(object obj)
        //{
        //    new WindowUsersFormPasswordView(Item as User).ShowDialog();
        //}
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        protected override bool CanSave()
        {
            return base.CanSave();
        }
    }
}