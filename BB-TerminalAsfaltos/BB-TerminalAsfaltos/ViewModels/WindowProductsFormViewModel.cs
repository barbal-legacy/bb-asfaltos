﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BB_TerminalAsfaltos.ViewModels
{
    class WindowProductsFormViewModel : WindowBaseViewModel
    {

        public WindowProductsFormViewModel(int Id) : base(typeof(Product))
        {
            idToUpdatePreviousWindow = -1;
            WindowTitle = Generals.FindTranslation("GeneralRegister");
            if (Id == 0)
            {
                RemoveButtonVisivility = System.Windows.Visibility.Collapsed;
                IsNewRecord = true;
                Item = new Product();
                idToUpdatePreviousWindow = Id;
            }
            else
            {
                RemoveButtonVisivility = System.Windows.Visibility.Visible;
                using (DataModel Context = new DataModel())
                {
                    Item = Context.Products.Find(Id);
                }
            }
        }
    }
}
