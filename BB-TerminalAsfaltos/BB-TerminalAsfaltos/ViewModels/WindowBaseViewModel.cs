﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace BB_TerminalAsfaltos.ViewModels
{
    class WindowBaseViewModel : INotifyPropertyChanged
    {
        private object _item;
        public static int idToUpdatePreviousWindow;
        public List<Vehicle> VehiclesList { get; set; }
        public List<Trailer> TrailersList { get; set; }
        public List<Tank> TanksList { get; set; }
        public List<Product> ProductsList { get; set; }
        public List<Driver> DriversList { get; set; }
        public List<Transporter> TransportersList { get; set; }
        public List<Entity> ClientsList { get; set; }

        public event EventHandler OnBeforeSave;
        public event PropertyChangedEventHandler PropertyChanged;

        private Type ObjectType;
        public bool IsNewRecord { get; set; } = false;
        public object Item
        {
            get { return _item; }
            set
            {
                _item = value;
                ItemId = (int)ObjectType.GetProperty("Id").GetValue(value, null);
            }
        }

        public int ItemId { get; set; }
        public string WindowTitle { get; set; }

        public ClickedCommand<object> SaveCommand { get; set; }
        public ClickedCommand<object> DeleteCommand { get; set; }
        public ClickedCommand<object> UndoCommand { get; set; }
        public static Visibility RemoveButtonVisivility { get; set; }


        public WindowBaseViewModel(Type ObjectType)
        {
            this.ObjectType = ObjectType;
            SaveCommand = new ClickedCommand<object>(Save);
            DeleteCommand = new ClickedCommand<object>(Delete);
            UndoCommand = new ClickedCommand<object>(Undo);
            
        }
        private void Undo(object obj)
        {
            using (DataModel Context = new DataModel())
            {
                if (ItemId!=0)
                {
                    Item = Context.Set(ObjectType).Find(ItemId);
                }
                else
                {
                    Item = Activator.CreateInstance(ObjectType); 
                }
                NotifyPropertyChanged("Item");
            }
        }

        private void Delete(object obj)
        {
            MessageBoxResult result = MessageBox.Show(Generals.FindTranslation("WantToRemove"), Generals.FindTranslation("Remove"), MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                using (DataModel Context = new DataModel())
                {
                    try
                    {
                        
                        var Eb = Context.Set(ObjectType).Find(ItemId) as EntityBase;
                        Eb.IsInactive = true;
                        
                        Context.SaveChanges();
                        //TODO Dar update a lista parent
                        Generals.GetCurrentWindow().Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void Save(object obj)
        {
            if (CanSave())
            {
                OnBeforeSave?.Invoke(this, EventArgs.Empty);
                MessageBoxResult result = MessageBox.Show(Generals.FindTranslation("WantToSave"),Generals.FindTranslation("Save"), MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    using (DataModel Context = new DataModel())
                    {
                        if (IsNewRecord)
                        {
                            int Id = 0;
                            string textToCompare = "";
                            switch (ObjectType.Name)
                            {
                                case "Driver":
                                    Id = ((Driver)Item).Id;
                                    textToCompare = ((Driver)Item).Name;
                                    if (Context.Drivers.Count(x => x.Name.ToUpper() == textToCompare.ToUpper() && x.Id != Id) > 0)
                                    {
                                        MessageBox.Show(Generals.FindTranslation("DriverAlreadyExists"));
                                        return;
                                    }
                                break;
                                case "Entity": 
                                    Id = ((Entity)Item).Id;
                                    textToCompare = ((Entity)Item).Name;
                                    if (Context.Entities.Count(x => x.Name.ToUpper() == textToCompare.ToUpper() && x.Id != Id) > 0)
                                    {
                                        MessageBox.Show(Generals.FindTranslation("EntityAlreadyExists"));
                                        return;
                                    }
                                break;
                                case "Product":
                                    Id = ((Product)Item).Id;
                                    textToCompare = ((Product)Item).Name;
                                    if (Context.Products.Count(x => x.Name.ToUpper() == textToCompare.ToUpper() && x.Id != Id) > 0)
                                    {
                                        MessageBox.Show(Generals.FindTranslation("DriverAlreadyExists"));
                                        return;
                                    }
                                break;
                                case "Tank":
                                    Id = ((Tank)Item).Id;
                                    textToCompare = ((Tank)Item).Name;
                                    if (Context.Tanks.Count(x => x.Name.ToUpper() == textToCompare.ToUpper() && x.Id != Id) > 0)
                                    {
                                        MessageBox.Show(Generals.FindTranslation("EntityAlreadyExists"));
                                        return;
                                    }
                                break;
                                case "Vehicle": 
                                    Id = ((Vehicle)Item).Id;
                                    textToCompare = ((Vehicle)Item).LicensePlate;
                                    if (Context.Vehicles.Count(x => x.LicensePlate.ToUpper() == textToCompare.ToUpper() && x.Id != Id) > 0)
                                    {
                                        MessageBox.Show(Generals.FindTranslation("EntityAlreadyExists"));
                                        return;
                                    }
                                break;
                                case "Trailer":
                                    Id = ((Trailer)Item).Id;
                                    textToCompare = ((Trailer)Item).LicensePlate;
                                    if (Context.Trailers.Count(x => x.LicensePlate.ToUpper() == textToCompare.ToUpper() && x.Id != Id) > 0)
                                    {
                                        MessageBox.Show(Generals.FindTranslation("EntityAlreadyExists"));
                                        return;
                                    }
                                break;
                            }
                            Context.Set(ObjectType).Add(Item);
                        }
                        else
                        {
                            Ticket ticket = Item as Ticket;
                            if (ticket != null)
                                ticket.UpdateIds();
                            
                            Context.Entry(Item).State = System.Data.Entity.EntityState.Modified;
                        }

                        if (Context.SaveChanges() > 0)
                        {
                            MessageBox.Show(Generals.FindTranslation("SaveWithSucess"));
                            Generals.GetCurrentWindow().Close();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Override this method to add custom logic to check if the form is valid
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanSave()
        {
            return true;
        }

        protected void NotifyPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
