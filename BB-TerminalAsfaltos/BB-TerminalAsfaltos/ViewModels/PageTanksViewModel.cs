﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Collections.ObjectModel;
using static BB_TerminalAsfaltos.Classes.Generals;

namespace BB_TerminalAsfaltos.ViewModels
{
    class PageTanksViewModel : PageBaseViewModel
    {
        public PageTanksViewModel() : base(FindTranslation("Tanks"), typeof(Tank), typeof(WindowTanksFormView), typeof(WindowTanksFormViewModel))
        {
            DateFilterVisibility = Visibility.Collapsed;
            DeleteButtonVisibility = Visibility.Collapsed;

            ColumnsList = new ObservableCollection<DataGridColumn>()
                    {
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Id"),
                            Header=FindTranslation("ID"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Name"),
                            Header=FindTranslation("Name"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        }
                    };

        }

        protected override bool CollectionFilter(object obj)
        {
            return !((Tank)obj).IsInactive
            && ((Tank)obj).Name?.IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0;

        }
    }
}
