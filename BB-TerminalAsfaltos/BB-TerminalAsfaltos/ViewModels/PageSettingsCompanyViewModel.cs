﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;

namespace BB_TerminalAsfaltos.ViewModels
{
    public class PageSettingsCompanyViewModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Company Company { get; set; }
        
        public ClickedCommand<object> SaveCommand { get; set; }
        public ClickedCommand<object> SearchLogoCommand { get; set; }

        public PageSettingsCompanyViewModel()
        {
            using (DataModel Context = new DataModel())
            {
                Company = Context.Companies.FirstOrDefault();
                if (Company == null)
                {
                    Company = new Company();
                }
            }
            SaveCommand = new ClickedCommand<object>(SaveCommandAction);
            SearchLogoCommand = new ClickedCommand<object>(SearchLogoCommandAction);
        }

        

        private void SearchLogoCommandAction(object obj)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                Company.ParsedLogo = new BitmapImage(new Uri(op.FileName));
                
                NotifyPropertyChanged("ParsedLogo");
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(Company.ParsedLogo));
                byte[] data;
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    Company.Logo = Convert.ToBase64String(data);
                }
            }
        }

        private void SaveCommandAction(object obj)
        {
            using (DataModel Context = new DataModel())
            {
               
                if (Company.Id==0)
                {
                    Context.Companies.Add(Company);
                }
                else
                {
                    Context.Entry(Company).State = System.Data.Entity.EntityState.Modified;
                }

                Context.SaveChanges();
                MessageBox.Show(Generals.FindTranslation("SaveWithSucess"));
            }
        }

        

        protected void NotifyPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
