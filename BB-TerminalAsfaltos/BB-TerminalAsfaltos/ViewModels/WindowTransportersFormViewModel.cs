﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BB_TerminalAsfaltos.ViewModels
{
    class WindowTransportersFormViewModel : WindowBaseViewModel
    {

        public WindowTransportersFormViewModel(int Id) : base(typeof(Transporter))
        {
            WindowTitle = Generals.FindTranslation("GeneralRegister");
            if (Id == 0)
            {
                RemoveButtonVisivility = System.Windows.Visibility.Collapsed;
                IsNewRecord = true;
                Item = new Transporter();
            }
            else
            {
                RemoveButtonVisivility = System.Windows.Visibility.Visible;
                using (DataModel Context = new DataModel())
                {
                    Item = Context.Transporters.Find(Id);
                }
            }
        }
    }
}
