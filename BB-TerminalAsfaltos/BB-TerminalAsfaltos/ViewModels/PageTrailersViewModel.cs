﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Collections.ObjectModel;
using static BB_TerminalAsfaltos.Classes.Generals;

namespace BB_TerminalAsfaltos.ViewModels
{
    class PageTrailersViewModel : PageBaseViewModel
    {
        public PageTrailersViewModel() : base(FindTranslation("Trailers"), typeof(Trailer), typeof(WindowTrailersFormView), typeof(WindowTrailersFormViewModel))
        {
            DateFilterVisibility = Visibility.Collapsed;
            DeleteButtonVisibility = Visibility.Collapsed;

            ColumnsList = new ObservableCollection<DataGridColumn>()
                    {
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Id"),
                            Header=FindTranslation("ID"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("LicensePlate"),
                            Header=FindTranslation("LicensePlate"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("DateInsurance") { StringFormat = "dd/MM/yyyy"},
                            Header=FindTranslation("ExpirationDateInsurance"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("DateADR") { StringFormat = "dd/MM/yyyy"},
                            Header=FindTranslation("ExpirationDateADR"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignLeft")
                        },
                    };
            
        }

        protected override bool CollectionFilter(object obj)
        {
            return !((Trailer)obj).IsInactive
                   && ((Trailer)obj).LicensePlate?.IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
                   || ((Trailer)obj).DateInsurance.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0
                   || ((Trailer)obj).DateADR.ToString().IndexOf(Filter, System.StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}
