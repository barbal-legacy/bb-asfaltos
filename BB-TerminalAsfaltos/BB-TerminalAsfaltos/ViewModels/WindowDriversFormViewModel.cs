﻿using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BB_TerminalAsfaltos.ViewModels
{
    class WindowDriversFormViewModel : WindowBaseViewModel
    {
        public WindowDriversFormViewModel(int Id) : base(typeof(Driver))
        {
            WindowTitle = Generals.FindTranslation("GeneralRegister");
            if (Id == 0)
            {
                RemoveButtonVisivility = System.Windows.Visibility.Collapsed;
                IsNewRecord = true;
                Item = new Driver();
            }
            else
            {
                RemoveButtonVisivility = System.Windows.Visibility.Visible;
                using (DataModel Context = new DataModel())
                {
                    Item = Context.Drivers.Find(Id);
                }
            }
        }
    }
}

