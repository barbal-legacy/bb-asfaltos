﻿using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using static BB_TerminalAsfaltos.Classes.Generals;


namespace BB_TerminalAsfaltos.ViewModels
{
    class PageTemporaryTicketsViewModel : PageBaseViewModel
    {
        public static Weighing Weighing { get; set; }

        public PageTemporaryTicketsViewModel() : base(FindTranslation("TemporaryTickets"), typeof(Weighing), typeof(WindowWeighingFormView), typeof(WindowWeighingFormViewModel))
        {
            DateFilterVisibility = Visibility.Collapsed;
            AddButtonVisibility = Visibility.Collapsed;

            ColumnsList = new ObservableCollection<DataGridColumn>()
                    {
                        new DataGridTextColumn
                        {
                            Binding=new Binding("CreatedDate") { StringFormat = "dd/MM/yyyy"},
                            Header=FindTranslation("RecordDate"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Vehicle.LicensePlate"),
                            Header=FindTranslation("LicensePlateVehicle"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Trailer.LicensePlate"),
                            Header=FindTranslation("LicensePlateTrailer"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },new DataGridTextColumn
                        {
                            Binding=new Binding("Driver.Name"),
                            Header=FindTranslation("Driver"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Transporter.Name"),
                            Header=FindTranslation("Transporter"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Entity.Name"),
                            Header=FindTranslation("Entity"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("DeliveryNote"),
                            Header=FindTranslation("DeliveryNote"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("TanksID"),
                            Header=FindTranslation("Tank"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Product.Name"),
                            Header=FindTranslation("Products"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("LoadTemperature"),
                            Header=FindTranslation("LoadTemperature"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Batch"),
                            Header=FindTranslation("Batch"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Observations"),
                            Header=FindTranslation("Observations"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Tare"),
                            Header=FindTranslation("Tare"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("TareHour"),
                            Header=FindTranslation("Hour"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Gross"),
                            Header=FindTranslation("Gross"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("Net"),
                            Header=FindTranslation("Net"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                        new DataGridTextColumn
                        {
                            Binding=new Binding("MaximumWeight"),
                            Header=FindTranslation("MaxWeight"),
                            ElementStyle=(Style)Application.Current.TryFindResource("AlignCenter")
                        },
                    };
            RefreshList();
        }
         
        // This function works for the Search bar -> NotImplemented
        protected override bool CollectionFilter(object obj)
        {
            return !((Weighing)obj).IsInactive && ((Weighing)obj).IsFinished;
        }

        protected override void RefreshList()
        {
            try
            {
                using (DataModel Context = new DataModel())
                {
                    ItemsCollection = new ObservableCollection<dynamic>(
                        Context.Weighings
                        .Include("Driver")
                        .Include("Entity")
                        .Include("Product")
                        .Include("Tank")
                        .Include("Trailer")
                        .Include("Transporter")
                        .Include("Vehicle")
                        .OrderByDescending(x => x.Id).Where(x => !x.IsFinished && !x.IsInactive).ToDynamicList<dynamic>());

                    NotifyPropertyChanged("ItemsCollection");
                    MyView = CollectionViewSource.GetDefaultView(ItemsCollection);
                    MyView.Filter = PreFilter;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao carregar lista"); //TODO: Arranjar tradução
            }
        }

    }
}
