﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Navigation;
using BB_TerminalAsfaltos.Classes;
using BB_TerminalAsfaltos.Model;
using BB_TerminalAsfaltos.ViewModels;
using BB_TerminalAsfaltos.Views;

namespace BB_TerminalAsfaltos
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Window LoginWindow;
        public App()
        {
            
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            try
            {
                BB_TerminalAsfaltos.MainWindow MW = new MainWindow();
                MW.Show();
                App.Current.MainWindow = MW;
                MW.Closed += this.OnShellWindowClosed;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            
            //try
            //{
            //    DataModel Context = new DataModel();
            //    //if (Context.Users.ToList().Count == 0)
            //    //{
            //    //    new WindowUsersFormView { DataContext = new WindowUsersFormViewModel(false) }.ShowDialog();
            //    //}
            //}
            //catch (System.Data.SqlClient.SqlException ex)
            //{
            //    MessageBox.Show(ex.Message);
            //    Environment.Exit(0);
            //}
                
            

            //LoginWindow = new Login { DataContext = new WindowLoginViewModel(this) };
            //LoginWindow.ShowDialog();
            //if (Generals.LoggedInUser == null)
            //{
            //    Environment.Exit(0);
            //}
        }

        internal void LoginOk()
        {
            BB_TerminalAsfaltos.MainWindow MW = new MainWindow();
            MW.Show();
            App.Current.MainWindow = MW;
           
            //LoginWindow.Close();
        }

        //private void Application_Exit(object sender, ExitEventArgs e)
        //{
        //    System.Windows.Application.Current.Shutdown();
        //    Environment.Exit(0);
        //}
        private async void OnShellWindowClosed(object sender, EventArgs e)
        {
            Shutdown();
            System.Windows.Application.Current.Shutdown();
            Environment.Exit(0);
        }
    }
    
}
