﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace BB_TerminalAsfaltos.Classes
{
    public class ClickedCommand<T> : ICommand
    {
        private Action<T> executeMethod;

        public ClickedCommand(Action<T> ExecuteMethod)
        {
            executeMethod = ExecuteMethod;
        }

        public void Execute(object parameter)
        {
            executeMethod?.Invoke((T)parameter);
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}
