﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Xml.Linq;

namespace BB_TerminalAsfaltos.Classes
{
    public class WeighReader
    {
        public event EventHandler<WeightDataEventArgs> OnWeightReceived;

        public string Name { get; set; }

        public Regex RegExMatch { get; set; }
        public Regex RegExWeight { get; set; }
        public string ReadCommand { get; set; }
        public string ZeroCommand { get; set; }
        public string ReadRelaysCommand { get; set; }
        public string ActionRelaysCommand { get; set; }
        public string ReaderCode { get; set; }
        

        public string MinTareWeight { get; set; }
        public string TurnOnRelay2Weight { get; set; }
        public string TurnOffRelay2Weight { get; set; }
        public string TurnOffRelay1Weight { get; set; }
        public string SelectedRelay1 { get; set; }
        public string SelectedRelay2 { get; set; }
        public string SelectedAlertThreshold { get; set; }
        public int RelaysTimeout { get; set; }
        public int WeightTimeout { get; set; }

        SerialCommunication serialCommunication;

        public WeighReader()
        {
            GetReader();
            serialCommunication = new SerialCommunication();
        }

        //Gets the first reader on ReadersList.xml
        //TODO pass a int parameter to get a reader by index.
        public void GetReader()
        {
            XDocument ReadersList = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ReadersList.xml"));

            if (ReadersList == null)
                throw new FileNotFoundException(Generals.FindTranslation("FileNotFoundReadersList"));

            XDocument SavedSettings = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Readers.xml"));

            if (SavedSettings == null)
                throw new FileNotFoundException(Generals.FindTranslation("FileNotFoundReaders"));

            XElement reader = SavedSettings.Descendants("reader").First();
            ReaderCode = reader.Attribute("ReaderCode")?.Value;

            ReadCommand = ReadersList.Descendants("Reader").Where(x => x.Attribute("Name").Value == ReaderCode).First().Attribute("ReadCommand").Value;
            ZeroCommand = ReadersList.Descendants("Reader").Where(x => x.Attribute("Name").Value == ReaderCode).First().Attribute("ZeroCommand").Value;
            ReadRelaysCommand = ReadersList.Descendants("Reader").Where(x => x.Attribute("Name").Value == ReaderCode).First().Attribute("ReadRelaysCommand").Value;
            ActionRelaysCommand = ReadersList.Descendants("Reader").Where(x => x.Attribute("Name").Value == ReaderCode).First().Attribute("ActionRelaysCommand").Value;

            GetConfigurations();

            RegExMatch = new Regex( ReadersList.Descendants("Reader").Where(x => x.Attribute("Name").Value == ReaderCode).First().Attribute("RegExMatch").Value);
            RegExWeight =  new Regex(ReadersList.Descendants("Reader").Where(x => x.Attribute("Name").Value == ReaderCode).First().Attribute("RegExWeight").Value);

        }

        /// <summary>
        /// Fetch necessary configurations to weighing logic
        /// </summary>
        public void GetConfigurations()
        {
            XDocument SavedConfigurations = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration.xml"));

            if (SavedConfigurations == null)
                throw new FileNotFoundException(Generals.FindTranslation("FileNotFoundConfiguration"));

            XElement reader = SavedConfigurations.Descendants("configuration").First();

            MinTareWeight = reader.Attribute("MinTareWeight")?.Value;
            TurnOnRelay2Weight = reader.Attribute("TurnOnRelay2Weight")?.Value;
            TurnOffRelay2Weight = reader.Attribute("TurnOffRelay2Weight")?.Value;
            TurnOffRelay1Weight = reader.Attribute("TurnOffRelay1Weight")?.Value;
            SelectedRelay1 = reader.Attribute("SelectedRelay1")?.Value;
            SelectedRelay2 = reader.Attribute("SelectedRelay2")?.Value;
            SelectedAlertThreshold = reader.Attribute("SelectedAlertThreshold")?.Value;
            RelaysTimeout = int.Parse(reader.Attribute("RelaysTimeout")?.Value);
            WeightTimeout = int.Parse(reader.Attribute("WeightTimeout")?.Value);
        }

        /// <summary>
        /// Opens communication with serial port and initiate weighing
        /// </summary>
        /// <returns></returns>
        public bool Open()
        {
            bool connectionOpen = serialCommunication.Open();

            if (connectionOpen)
                GetWeight();

            return connectionOpen;
        }

        /// <summary>
        /// Close serial port communication and turn off relays status
        /// </summary>
        /// <returns></returns>
        public bool Close()
        {
            if (serialCommunication.serialPort.IsOpen)
            {
                ResetRelaysStatus();
                serialCommunication.serialPort.Close();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Utilitary to convert ASCII to Binary to send command to reader
        /// </summary>
        /// <param name="data"></param>
        /// <param name="byteCommand"></param>
        void ConvertFromASCIIToBinary(string data, out byte[] byteCommand)
        {
            data += (char)13;
            int position = 0;
            byteCommand = new byte[data.Length];

            byteCommand = Encoding.Default.GetBytes(data.ToCharArray());
        }

        /// <summary>
        /// Get weight value from reader and arrange it to be used
        /// </summary>
        public async void GetWeight()
        {
            bool readError = false;

            new Thread(() =>
            {
                try
                {
                    byte[] byteCommand;
                    string weightResult = "";
                    bool isNegative = false;

                    ConvertFromASCIIToBinary(ReadCommand, out byteCommand);
                    //Clear data to read before sending command
                    serialCommunication.serialPort.DiscardInBuffer();
                    serialCommunication.serialPort.Write(byteCommand, 0, byteCommand.Length);

                    //Check size of data to read and set it on dataReceived var to prevent getting undesired info
                    var buffer = new byte[serialCommunication.serialPort.BytesToRead];
                    byte[] dataReceived = new byte[buffer.Length];

                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();
                    Console.WriteLine("StartWeightWatch: ");
                    while (true)
                    {
                        while (!weightResult.Contains((char)13) && !weightResult.Contains((char)10))
                        {
                            //tmpByte = new byte[20];
                            //dataReceived += serialPort.Read(tmpByte, 0, 20);
                        
                            serialCommunication.serialPort.Read(dataReceived, 0, buffer.Length);
                            weightResult = Encoding.ASCII.GetString(dataReceived);

                            //Sends command again if didn't get any data
                            if (stopwatch.Elapsed > TimeSpan.FromMilliseconds(200))
                            {
                                Console.WriteLine("StopWeightWatch: " + stopwatch.Elapsed);
                                stopwatch.Stop();
                                serialCommunication.serialPort.Write(byteCommand, 0, byteCommand.Length);
                                buffer = new byte[serialCommunication.serialPort.BytesToRead];
                                dataReceived = new byte[buffer.Length];
                                stopwatch.Restart();
                            }
                        }

                        //Apply regex to received data to get string prepared to use and Fires event that will keep continuous weight
                        if (RegExWeight.Matches(weightResult ?? "").Count != 0)
                        {
                            if (weightResult.Contains("-"))
                                isNegative = true;

                            weightResult = RegExWeight.Match(weightResult).ToString();
                            Console.WriteLine("regex matches!");

                            if (isNegative)
                                weightResult = "-" + weightResult;
                            OnWeightReceived?.Invoke(this, new WeightDataEventArgs(weightResult));
                            break;
                        }
                        Console.WriteLine(DateTime.Now);
                        break;
                    }
                    Console.WriteLine("Peso: " + weightResult);
                }
                catch (Exception ex)
                {
                    readError = true;
                    Console.WriteLine("GetWeight() error: " + ex.Message);
                }

                //Try to reconnect with reader if communication's missing
                try
                {
                    while(readError)
                    {
                        if (serialCommunication.serialPort.IsOpen)
                        {
                            serialCommunication.serialPort.Close();
                            Thread.Sleep(500);
                            serialCommunication.serialPort.Open();
                        }

                        if (serialCommunication.serialPort.IsOpen)
                        {
                            readError = false;
                            GetWeight();
                        }

                        if (!serialCommunication.serialPort.IsOpen)
                        {
                            try
                            {
                                serialCommunication.serialPort.Open();
                            }
                            catch
                            {
                                break;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception, try re-connect: " + e.Message);
                }
            }).Start();
        }

        /// <summary>
        /// Turn off all relays status (forcibly)
        /// </summary>
        public void ResetRelaysStatus()
        {
            try
            {
                byte[] byteCommand;
                string dataReceived = "";
                string command = ActionRelaysCommand + "ÿÿÿÿÿÿÿÿ"; // ASCII: ÿ | hex: FF | dec: 255 | bin: 1111 1111 <=> 1: OFF | 0: ON

                ConvertFromASCIIToBinary(command, out byteCommand);
                serialCommunication.serialPort.Write(byteCommand, 0, byteCommand.Length);
            }
            catch
            {
                Console.WriteLine("Ocorreu um erro ao fazer restart na placa de relés!");
            }
        }

        /// <summary>
        /// Gets Relay Status and check if value is valid for the weighing logic in specific (can't have more than 2 relays turned on)
        /// </summary>
        /// <returns></returns>
        public string GetRelaysStatus()
        {
            byte[] byteCommand;
            string dataReceived = "";
            string command = ReadRelaysCommand;

            ConvertFromASCIIToBinary(command, out byteCommand);

            //Clears buffer in and out data before send command to reader
            serialCommunication.serialPort.DiscardInBuffer();
            serialCommunication.serialPort.DiscardOutBuffer();

            serialCommunication.serialPort.Write(byteCommand, 0, byteCommand.Length);
            Thread.Sleep(50);

            byte[] relayGroup1relaysStatus = new byte[10];
            Stopwatch stopWatch = new Stopwatch();
            
            while (true)
            {
                try
                {
                    stopWatch.Start();
                    //Receive all Relay sets statuses
                    serialCommunication.serialPort.Read(relayGroup1relaysStatus, 0, 10).ToString(); //dataReceived += 
                                                        // space   C8 C7 ... C1 \r
                }
                catch (TimeoutException tex)
                {
                    Console.WriteLine("elapsed time: " + stopWatch.Elapsed.ToString()); stopWatch.Stop();
                    Console.WriteLine("Já não tem nada para ler");
                    break;
                }

                if (relayGroup1relaysStatus[9] == (char)13)
                    break;
            }

            //Reads only set 1 relays status and get it ready to use 
            dataReceived = Convert.ToString(relayGroup1relaysStatus[1], 2).PadLeft(8, '0');
            Console.WriteLine("Bytes: " + dataReceived);
            if (dataReceived.Count(c => c == '0') > 2)
                Console.WriteLine("MAIS QUE 2 ZEROS");
                
            //convert string into bool array
            //int position = 0;
            //foreach(char c in dataReceived)
            //{
            //    relaysStatus[position] = c == '0' ? true : false;
            //    Console.WriteLine("Reles: " + relaysStatus[position]);
            //    position++;
            //}
            
            return dataReceived;
        }

        /// <summary>
        /// Check the current relays status and change the relays in given position (selected in settings) to be OFF
        /// </summary>
        /// <param name="relayNumber"></param>
        public void TurnOffRelay(int relayNumber)
        {
            string relayStatus = "";
            char[] stringToModify;
            byte[] byteCommand;

            //get current relays status and change the specified relay to OFF
            relayStatus = GetRelaysStatus();
            stringToModify = relayStatus.ToCharArray();
            stringToModify[relayNumber - 1] = '1';

            //convert new relays status to byte array
            byte[] toConvert = Encoding.Default.GetBytes(stringToModify);

            //build and prepare command to send to reader
            string command = ActionRelaysCommand + (char)Convert.ToInt32(Encoding.ASCII.GetString(toConvert), 2) + "ÿÿÿÿÿÿÿ";

            ConvertFromASCIIToBinary(command, out byteCommand);
            serialCommunication.serialPort.Write(byteCommand, 0, byteCommand.Length);
        }

        /// <summary>
        /// Check the current relays status and change the relays in given position (selected in settings) to be ON
        /// </summary>
        /// <param name="relayNumber"></param>
        public void TurnOnRelay(int relayNumber)
        {
            string relayStatus = "";
            char[] stringToModify;
            byte[] byteCommand;

            //get current relays status and change the specified relay to ON
            relayStatus = GetRelaysStatus();
            stringToModify = relayStatus.ToCharArray();
            stringToModify[relayNumber - 1] = '0';

            //convert new relays status to byte array
            byte[] toConvert = Encoding.Default.GetBytes(stringToModify);

            //build and prepare command to send to reader
            string command = ActionRelaysCommand + (char)Convert.ToInt32(Encoding.ASCII.GetString(toConvert), 2) + "ÿÿÿÿÿÿÿ";

            ConvertFromASCIIToBinary(command, out byteCommand);
            serialCommunication.serialPort.Write(byteCommand, 0, byteCommand.Length);
        }

        /// <summary>
        /// Force a desired relays status (done to replace TurnOn/OffRelays)
        /// </summary>
        /// <param name="relaysStatus"></param>
        public void ForceRelaysStatus(char relaysStatus)
        {
            try
            {
                byte[] byteCommand;

                //build and prepare command to send to reader
                string command = ActionRelaysCommand + relaysStatus + "ÿÿÿÿÿÿÿ";

                ConvertFromASCIIToBinary(command, out byteCommand);
                serialCommunication.serialPort.Write(byteCommand, 0, byteCommand.Length);
            }
            catch
            {
                Console.WriteLine("Não foi possível forçar o estado dos relés!");
            }
        }

        /// <summary>
        /// Event to trigger continuous fetch weight
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnWeightDataReceived(WeightDataEventArgs e)
        {
            EventHandler<WeightDataEventArgs> handler = OnWeightReceived;
            handler?.Invoke(this, e);
        }

        public class WeightDataEventArgs : EventArgs
        {
            public WeightDataEventArgs(string weight)
            {
                this.weight = weight;
            }

            public string weight { get; set; }
        }

        private class SerialCommunication
        {
            public SerialPort serialPort;
            public string ComPort { get; set; }
            public int BaudRate { get; set; }
            public Parity Parity { get; set; }
            public StopBits StopBits { get; set; }
            public int DataBits { get; set; }

            public SerialCommunication()
            {
                XDocument SavedSettings = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Readers.xml"));

                if (SavedSettings == null)
                    throw new FileNotFoundException(Generals.FindTranslation("FileNotFoundReaders"));

                XElement reader = SavedSettings.Descendants("reader").First();

                ComPort = reader.Attribute("ComPort")?.Value;
                BaudRate = int.Parse(reader.Attribute("BaudRate")?.Value);
                DataBits = int.Parse(reader.Attribute("DataBits")?.Value);
                Parity = (Parity)Enum.Parse(typeof(Parity), reader.Attribute("Parity")?.Value);
                StopBits = (StopBits)Enum.Parse(typeof(StopBits), reader.Attribute("StopBits")?.Value);
            }

            /// <summary>
            /// Opens connection with serial port using the selected settings
            /// </summary>
            /// <returns></returns>
            public bool Open()
            {
                serialPort = new SerialPort();
                serialPort.PortName = ComPort;
                serialPort.BaudRate = BaudRate;
                serialPort.DataBits = DataBits;
                serialPort.Parity = Parity;
                serialPort.StopBits = StopBits;
                serialPort.ReadTimeout = 50;

                if (!serialPort.IsOpen)
                {
                    serialPort.Open();
                    return true;
                }

                return false;
            }

            /// <summary>
            /// Close serial port communication
            /// </summary>
            /// <returns></returns>
            public bool Close()
            {
                if (serialPort.IsOpen)
                {
                    serialPort.Close();
                    return true;
                }

                return false;
            }
        }
    }
}
