﻿using BB_TerminalAsfaltos.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BB_TerminalAsfaltos.Classes
{

    public static class Generals
    {
        //public static User LoggedInUser { get; set; }
        public static Window GetCurrentWindow() => Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);

        public static string FindTranslation(string key)
        {
            string s = Application.Current.TryFindResource(key)?.ToString();
            return s != "" ? s : "<Translation Missing>";
        }
    }
    
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public sealed class ReverseBoolToVisibilityConverter : IValueConverter
    {
        public Visibility TrueValue { get; set; }
        public Visibility FalseValue { get; set; }

        public ReverseBoolToVisibilityConverter()
        {
            // set defaults
            TrueValue = Visibility.Visible;
            FalseValue = Visibility.Collapsed;
        }

        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return null;
            return (bool)value ? FalseValue : TrueValue;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (Equals(value, FalseValue))
                return true;
            if (Equals(value, TrueValue))
                return false;
            return null;
        }
    }

    public sealed class BoolToTicketState : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            bool state = (bool)value;
            BitmapImage icon = new BitmapImage();
            icon.BeginInit();
            if (state)
            {
                icon.UriSource = new Uri("/Images/icons8-password-50.png", UriKind.Relative);
            }
            else
            {
                icon.UriSource = new Uri("/Images/icons8-sand-timer-50.png", UriKind.Relative);
            }
            icon.EndInit();
            return icon;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {

            return null;
        }
    }

    public class NullVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class ReverseBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return (bool)value;
        }
    }

    public class BoolToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
            {
                {
                    return new SolidColorBrush(Colors.Green);
                }
            }
            return new SolidColorBrush(Colors.Red);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
