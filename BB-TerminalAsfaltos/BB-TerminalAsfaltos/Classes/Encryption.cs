﻿using System.Security.Cryptography;
using System.Text;
using TobyEmden.Security.Encryption;


namespace BB_TerminalAsfaltos.Classes
{
    public static class Encryption
    {
        private static TripleDESCryptoServiceProvider TripleDES = new TripleDESCryptoServiceProvider();

        private static readonly MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();

        // Definição da chave de encriptação/desencriptação
        private const string key = "BARBAL12345test";

        public static byte[] MD5Hash(string value)
        {
            // Converte a chave para um array de bytes
            byte[] byteArray = Encoding.ASCII.GetBytes(value);
            return MD5.ComputeHash(byteArray);
        }

        public static string EncryptRijndael(string InputTxt)
        {
            string encryption;

            Crypto.EncryptionAlgorithm = Crypto.Algorithm.Rijndael;
            Crypto.Encoding = Crypto.EncodingType.BASE_64;

            if (Crypto.IsHashAlgorithm)
            {
                if (Crypto.GenerateHash(InputTxt))
                {
                    encryption = Crypto.Content;
                }
                else
                {
                    encryption = Crypto.CryptoException.Message;
                }
            }
            else
            {
                Crypto.Key = key;

                if (Crypto.EncryptString(InputTxt))
                {
                    encryption = Crypto.Content;
                }
                else
                {
                    encryption = Crypto.CryptoException.Message;
                }
            }

            Crypto.Clear();

            return encryption;
        }

        public static string DecryptRijndael(string InputTxt)
        {
            string decryption;

            Crypto.EncryptionAlgorithm = Crypto.Algorithm.Rijndael;
            Crypto.Encoding = Crypto.EncodingType.BASE_64;

            if (Crypto.IsHashAlgorithm)
            {
                decryption = "Cannot decipher a one-way hash";
            }
            else
            {
                Crypto.Key = key;

                Crypto.Content = InputTxt;

                if (Crypto.DecryptString())
                {
                    decryption = Crypto.Content;
                }
                else
                {
                    decryption = Crypto.CryptoException.Message;
                }
            }

            Crypto.Clear();

            return decryption;
        }
    }
}