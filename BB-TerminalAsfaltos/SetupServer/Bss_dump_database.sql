USE [master]
GO

/****** Object:  Database [BB_FillUp]    Script Date: 13/12/2021 14:28:52 ******/
CREATE DATABASE [BB_FillUp]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BB_FillUp].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BB_FillUp] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BB_FillUp] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BB_FillUp] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BB_FillUp] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BB_FillUp] SET ARITHABORT OFF 
GO
ALTER DATABASE [BB_FillUp] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BB_FillUp] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BB_FillUp] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BB_FillUp] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BB_FillUp] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BB_FillUp] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BB_FillUp] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BB_FillUp] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BB_FillUp] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BB_FillUp] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BB_FillUp] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BB_FillUp] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BB_FillUp] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BB_FillUp] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BB_FillUp] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BB_FillUp] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BB_FillUp] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BB_FillUp] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BB_FillUp] SET  MULTI_USER 
GO
ALTER DATABASE [BB_FillUp] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BB_FillUp] SET DB_CHAINING OFF 
GO
USE [BB_FillUp]
GO

/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 13/12/2021 14:30:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Companies]    Script Date: 13/12/2021 14:38:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[ZipCode] [nvarchar](max) NULL,
	[TaxPayerNumber] [nvarchar](max) NULL,
	[ContactNumber] [nvarchar](max) NULL,
	[Fax] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[WebSite] [nvarchar](max) NULL,
	[DoesTicketNumberResetEveryYear] [bit] NOT NULL,
	[IsTicketDuplicated] [bit] NOT NULL,
	[Logo] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Companies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entities]    Script Date: 13/12/2021 14:39:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Entities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Drivers]    Script Date: 13/12/2021 14:41:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Drivers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[DateID] [datetime] NULL,
	[DateADR] [datetime] NULL,
	[DateDriversLicense] [datetime] NULL,
 CONSTRAINT [PK_dbo.Drivers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 13/12/2021 14:42:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tanks]    Script Date: 13/12/2021 14:43:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tanks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Tanks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tickets]    Script Date: 13/12/2021 14:43:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tickets](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [int] NULL,
	[WeighingId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
	[LoadTemperature] [nvarchar](max) NULL,
	[Batch] [nvarchar](max) NULL,
	[Observations] [nvarchar](max) NULL,
	[DeliveryNote] [nvarchar](max) NULL,
	[MaximumWeight] [nvarchar](max) NULL,
	[Tare] [nvarchar](max) NULL,
	[Gross] [nvarchar](max) NULL,
	[Net] [nvarchar](max) NULL,
	[TareHour] [nvarchar](max) NULL,
	[VehicleId] [int] NULL,
	[TrailerId] [int] NULL,
	[TankId] [int] NULL,
	[ProductId] [int] NULL,
	[DriverId] [int] NULL,
	[TransporterId] [int] NULL,
	[EntityId] [int] NULL,
	[LastPrintDate] [datetime] NULL,
	[LastPrintTime] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Tickets] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Trailers]    Script Date: 13/12/2021 14:44:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Trailers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
	[LicensePlate] [nvarchar](max) NULL,
	[DateInsurance] [datetime] NULL,
	[DateADR] [datetime] NULL,
 CONSTRAINT [PK_dbo.Trailers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transporters]    Script Date: 13/12/2021 14:44:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transporters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Transporters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vehicles]    Script Date: 13/12/2021 14:45:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehicles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
	[LicensePlate] [nvarchar](max) NULL,
	[DateInsurance] [datetime] NULL,
	[DateADR] [datetime] NULL,
 CONSTRAINT [PK_dbo.Vehicles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Weighings]    Script Date: 13/12/2021 14:45:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Weighings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NULL,
	[TrailerID] [int] NULL,
	[TankID] [int] NULL,
	[ProductID] [int] NULL,
	[LoadTemperature] [nvarchar](max) NULL,
	[Batch] [nvarchar](max) NULL,
	[Observations] [nvarchar](max) NULL,
	[DriverID] [int] NULL,
	[TransporterID] [int] NULL,
	[EntityID] [int] NULL,
	[DeliveryNote] [nvarchar](max) NULL,
	[MaximumWeight] [nvarchar](max) NULL,
	[Tare] [nvarchar](max) NULL,
	[Gross] [nvarchar](max) NULL,
	[Net] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsInactive] [bit] NOT NULL,
	[TareHour] [nvarchar](max) NULL,
	[manualTare] [bit] NOT NULL,
	[bypassed] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Weighings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_DriverId]    Script Date: 13/12/2021 14:54:13 ******/
CREATE NONCLUSTERED INDEX [IX_DriverId] ON [dbo].[Tickets]
(
	[DriverId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_EntityId]    Script Date: 13/12/2021 14:54:55 ******/
CREATE NONCLUSTERED INDEX [IX_EntityId] ON [dbo].[Tickets]
(
	[EntityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductId]    Script Date: 13/12/2021 14:55:08 ******/
CREATE NONCLUSTERED INDEX [IX_ProductId] ON [dbo].[Tickets]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TankId]    Script Date: 13/12/2021 14:55:20 ******/
CREATE NONCLUSTERED INDEX [IX_TankId] ON [dbo].[Tickets]
(
	[TankId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TrailerId]    Script Date: 13/12/2021 14:55:30 ******/
CREATE NONCLUSTERED INDEX [IX_TrailerId] ON [dbo].[Tickets]
(
	[TrailerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TransporterId]    Script Date: 13/12/2021 14:55:41 ******/
CREATE NONCLUSTERED INDEX [IX_TransporterId] ON [dbo].[Tickets]
(
	[TransporterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VehicleId]    Script Date: 13/12/2021 14:55:54 ******/
CREATE NONCLUSTERED INDEX [IX_VehicleId] ON [dbo].[Tickets]
(
	[VehicleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_WeighingId]    Script Date: 13/12/2021 14:56:04 ******/
CREATE NONCLUSTERED INDEX [IX_WeighingId] ON [dbo].[Tickets]
(
	[WeighingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DriverID]    Script Date: 13/12/2021 14:56:54 ******/
CREATE NONCLUSTERED INDEX [IX_DriverID] ON [dbo].[Weighings]
(
	[DriverID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_EntityID]    Script Date: 13/12/2021 14:57:15 ******/
CREATE NONCLUSTERED INDEX [IX_EntityID] ON [dbo].[Weighings]
(
	[EntityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductID]    Script Date: 13/12/2021 14:57:25 ******/
CREATE NONCLUSTERED INDEX [IX_ProductID] ON [dbo].[Weighings]
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TankID]    Script Date: 13/12/2021 14:57:37 ******/
CREATE NONCLUSTERED INDEX [IX_TankID] ON [dbo].[Weighings]
(
	[TankID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TrailerID]    Script Date: 13/12/2021 14:57:49 ******/
CREATE NONCLUSTERED INDEX [IX_TrailerID] ON [dbo].[Weighings]
(
	[TrailerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TransporterID]    Script Date: 13/12/2021 14:57:59 ******/
CREATE NONCLUSTERED INDEX [IX_TransporterID] ON [dbo].[Weighings]
(
	[TransporterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VehicleID]    Script Date: 13/12/2021 14:58:09 ******/
CREATE NONCLUSTERED INDEX [IX_VehicleID] ON [dbo].[Weighings]
(
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Trailers] ADD  CONSTRAINT [DF_dbo.Trailers_DateInsurance]  DEFAULT ('2021-01-01T00:00:00.000') FOR [DateInsurance]
GO
ALTER TABLE [dbo].[Trailers] ADD  CONSTRAINT [DF_dbo.Trailers_DateADR]  DEFAULT ('2021-01-01T00:00:00.000') FOR [DateADR]
GO
ALTER TABLE [dbo].[Drivers] ADD  CONSTRAINT [DF_dbo.Drivers_DateID]  DEFAULT ('2021-01-01T00:00:00.000') FOR [DateID]
GO
ALTER TABLE [dbo].[Drivers] ADD  CONSTRAINT [DF_dbo.Drivers_DateADR]  DEFAULT ('2021-01-01T00:00:00.000') FOR [DateADR]
GO
ALTER TABLE [dbo].[Drivers] ADD  CONSTRAINT [DF_dbo.Drivers_DateDriversLicense]  DEFAULT ('2021-01-01T00:00:00.000') FOR [DateDriversLicense]
GO
ALTER TABLE [dbo].[Vehicles] ADD  CONSTRAINT [DF_dbo.Vehicles_DateInsurance]  DEFAULT ('2021-01-01T00:00:00.000') FOR [DateInsurance]
GO
ALTER TABLE [dbo].[Vehicles] ADD  CONSTRAINT [DF_dbo.Vehicles_DateADR]  DEFAULT ('2021-01-01T00:00:00.000') FOR [DateADR]
GO
ALTER TABLE [dbo].[Weighings] ADD  DEFAULT ((0)) FOR [manualTare]
GO
ALTER TABLE [dbo].[Weighings] ADD  DEFAULT ((0)) FOR [bypassed]
GO
ALTER TABLE [dbo].[Weighings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Weighings_dbo.Drivers_DriverID] FOREIGN KEY([DriverID])
REFERENCES [dbo].[Drivers] ([Id])
GO
ALTER TABLE [dbo].[Weighings] CHECK CONSTRAINT [FK_dbo.Weighings_dbo.Drivers_DriverID]
GO
ALTER TABLE [dbo].[Weighings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Weighings_dbo.Entities_EntityID] FOREIGN KEY([EntityID])
REFERENCES [dbo].[Entities] ([Id])
GO
ALTER TABLE [dbo].[Weighings] CHECK CONSTRAINT [FK_dbo.Weighings_dbo.Entities_EntityID]
GO
ALTER TABLE [dbo].[Weighings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Weighings_dbo.Products_ProductID] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[Weighings] CHECK CONSTRAINT [FK_dbo.Weighings_dbo.Products_ProductID]
GO
ALTER TABLE [dbo].[Weighings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Weighings_dbo.Tanks_TankID] FOREIGN KEY([TankID])
REFERENCES [dbo].[Tanks] ([Id])
GO
ALTER TABLE [dbo].[Weighings] CHECK CONSTRAINT [FK_dbo.Weighings_dbo.Tanks_TankID]
GO
ALTER TABLE [dbo].[Weighings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Weighings_dbo.Trailers_TrailerID] FOREIGN KEY([TrailerID])
REFERENCES [dbo].[Trailers] ([Id])
GO
ALTER TABLE [dbo].[Weighings] CHECK CONSTRAINT [FK_dbo.Weighings_dbo.Trailers_TrailerID]
GO
ALTER TABLE [dbo].[Weighings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Weighings_dbo.Transporters_TransporterID] FOREIGN KEY([TransporterID])
REFERENCES [dbo].[Transporters] ([Id])
GO
ALTER TABLE [dbo].[Weighings] CHECK CONSTRAINT [FK_dbo.Weighings_dbo.Transporters_TransporterID]
GO
ALTER TABLE [dbo].[Weighings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Weighings_dbo.Vehicles_VehicleID] FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles] ([Id])
GO
ALTER TABLE [dbo].[Weighings] CHECK CONSTRAINT [FK_dbo.Weighings_dbo.Vehicles_VehicleID]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.Drivers_DriverId] FOREIGN KEY([DriverId])
REFERENCES [dbo].[Drivers] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.Drivers_DriverId]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.Entities_EntityId] FOREIGN KEY([EntityId])
REFERENCES [dbo].[Entities] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.Entities_EntityId]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.Products_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.Products_ProductId]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.Tanks_TankId] FOREIGN KEY([TankId])
REFERENCES [dbo].[Tanks] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.Tanks_TankId]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.Trailers_TrailerId] FOREIGN KEY([TrailerId])
REFERENCES [dbo].[Trailers] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.Trailers_TrailerId]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.Transporters_TransporterId] FOREIGN KEY([TransporterId])
REFERENCES [dbo].[Transporters] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.Transporters_TransporterId]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.Vehicles_VehicleId] FOREIGN KEY([VehicleId])
REFERENCES [dbo].[Vehicles] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.Vehicles_VehicleId]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.Weighings_WeighingId] FOREIGN KEY([WeighingId])
REFERENCES [dbo].[Weighings] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.Weighings_WeighingId]
GO
USE [master]
GO
ALTER DATABASE [BB_FillUp] SET  READ_WRITE 
GO
