﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo.Wmi;
using NetFwTypeLib;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;

namespace SetupServer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Install Sql server
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"SQLEXPR_x86_ENU.exe");
            string pathConfiguration = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"ConfigurationFile.ini");
            Process InstanceProcess = new Process();
            InstanceProcess.StartInfo = new ProcessStartInfo(path, " /ConfigurationFile=" + pathConfiguration);
            InstanceProcess.Start();
            InstanceProcess.WaitForExit();

            //Open Sql server required programs on firewall.
            OpenPort();

            //Create Database from file
            SqlConnection Conn = new SqlConnection(@"Data Source="+ System.Environment.MachineName + @"\SQLBARBAL;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            Server sv = new Server(new ServerConnection(Conn));
            string script= File.ReadAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Bss_dump_database.sql"));
            try
            {
                //SqlCommand Command = new SqlCommand(DataBaseQuery, Conn);
                //Conn.Open();
                //Command.ExecuteNonQuery();
                sv.ConnectionContext.ExecuteNonQuery(script);
            }
            catch (Exception)
            {
                throw;
            }
            Console.WriteLine("Sucesso!");
            Console.ReadLine();

            
        }

        private static void OpenPort()
        {
            var command = "/C netsh advfirewall firewall add rule name=\"SQL BARBAL\" dir=in action=allow program=\"C:\\Program Files (x86)\\Microsoft SQL Server\\MSSQL11.SQLBARBAL\\MSSQL\\Binn\\sqlservr.exe\" enable =yes";
            command += " &  netsh advfirewall firewall add rule name=\"SQL BARBAL Browser\" dir=in action=allow program=\"C:\\Program Files (x86)\\Microsoft SQL Server\\110\\Shared\\sqlbrowser.exe\" enable=yes";
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Verb = "runas";
            startInfo.Arguments = command;
            process.StartInfo = startInfo;
            process.Start();
        }
        
    }
}
