﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	WeighingEngine.cs
//
// summary:	Implements the weighing engine class
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MWeighingEngine
{
    public partial class WeighingEngine
    {
        public bool error { get; set; }

        public string errorMessage { get; set; }

        protected bool isSerial { get; set; }

        protected string portName { get; set; }

        protected int baudRate { get; set; }

        protected int dataBits { get; set; }

        protected Parity parity { get; set; }

        protected StopBits stopBits { get; set; }

        protected string ip { get; set; }

        protected int port { get; set; }

        protected string readCommand { get; set; }

        protected string zeroCommand { get; set; }

        protected string weighingResult { get; set; }

        /// <summary>   The background serial connection worker. </summary>
        protected BackgroundWorker bgSerialConnectionWorker = new BackgroundWorker();

        /// <summary>   The background ethernet connection worker. </summary>
        protected BackgroundWorker bgEthernetConnectionWorker = new BackgroundWorker();

        /// <summary>   The command. </summary>
        protected string command = string.Empty;

        /// <summary>   True to zero. </summary>
        protected bool zero;

        /// <summary>   Event queue for all listeners interested in OnError events. </summary>
        public event EventHandler OnError;

        /// <summary>   The timeout time. </summary>
        protected int TimeoutTime = 3;

        /// <summary>   The result of the asynchronous operation. </summary>
        protected IAsyncResult result;

        #region SERIAL CONNECTION

        protected void BgSerialConnectionWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (bgSerialConnectionWorker.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                using (SerialPort serial = new SerialPort() { PortName = portName, BaudRate = baudRate, DataBits = dataBits, Parity = parity, StopBits = stopBits })
                {
                    serial.Open();

                    if (command?.Equals(readCommand) == true)
                    {
                        serial.WriteLine(command + Convert.ToChar(13));
                    }
                    else if (command != null && zero && command.Equals(zeroCommand))
                    {
                        serial.WriteLine(command + Convert.ToChar(13));
                        zero = false;
                    }

                    Thread.Sleep(200);
                    try
                    {
                        if (!serial.IsOpen || serial.BytesToRead == 0)
                        {
                            weighingResult = "-----";

                            error = true;
                            errorMessage = "Não existe comunicação com o leitor!";
                            OnError?.Invoke(this, EventArgs.Empty);

                            return;
                        }
                        //weighingResult = serial.ReadTo(Convert.ToChar(13).ToString());

                        byte[] dataBuffer = new byte[256];
                        int numBytes = serial.Read(dataBuffer, 0, dataBuffer.Length);
                        int index = Array.LastIndexOf(dataBuffer, byte.Parse("13"));
                        if (index >= 8)
                        {
                            string responseData = Encoding.ASCII.GetString(dataBuffer, index - 8, 8).Replace(Convert.ToChar(0).ToString(), "");
                            if (responseData.Contains(Convert.ToChar(13)) && responseData.Split(Convert.ToChar(13))[1] != null && (responseData.IndexOf(Convert.ToChar(43)) > 0 || responseData.IndexOf(Convert.ToChar(45)) > 0))
                                responseData = responseData.Split(Convert.ToChar(13))[1];
                            weighingResult = responseData;
                        }
                        else
                        {
                            weighingResult = "-----";
                        }
                    }
                    catch (IOException ioEx)
                    {
                        error = true;
                        errorMessage = "I/O Exception " + ioEx.Message;
                        OnError?.Invoke(this, EventArgs.Empty);
                    }
                    catch (UnauthorizedAccessException uaEx)
                    {
                        error = true;
                        errorMessage = "Unauthorized Access Exception" + uaEx.Message;
                        OnError?.Invoke(this, EventArgs.Empty);
                    }
                    catch (ArgumentOutOfRangeException argEx)
                    {
                        error = true;
                        errorMessage = "Argument Exception" + argEx.Message;
                        OnError?.Invoke(this, EventArgs.Empty);
                    }

                    //if (command != null && zero && command.Equals(zeroCommand))
                    //{
                    //    Thread.Sleep(150);
                    //    serial.WriteLine(command + Convert.ToChar(13));
                    //    zero = false;
                    //}
                }
            }
            catch (IOException ioEx)
            {
                error = true;
                errorMessage = "I/O Exception " + ioEx.Message;
                OnError?.Invoke(this, EventArgs.Empty);
            }
            catch (UnauthorizedAccessException uaEx)
            {
                error = true;
                errorMessage = "Unauthorized Access Exception" + uaEx.Message;
                OnError?.Invoke(this, EventArgs.Empty);
            }
            catch (ArgumentOutOfRangeException argEx)
            {
                error = true;
                errorMessage = "Argument Exception" + argEx.Message;
                OnError?.Invoke(this, EventArgs.Empty);
            }
        }

        protected void BgSerialConnectionWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled)
            {
                command = !zero ? readCommand : zeroCommand;
                bgSerialConnectionWorker.RunWorkerAsync();
            }
        }

        #endregion SERIAL CONNECTION

        #region ETHERNET CONNECTION

        protected void BgEthernetConnectionWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (bgEthernetConnectionWorker.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            try
            {
                using (TcpClient tcpClient = new TcpClient())
                {
                    result = tcpClient.BeginConnect(ip, port, null, null);
                    var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(TimeoutTime));
                    if (!success)
                    {
                        error = true;
                        errorMessage = "Failed to connect.";
                        return;
                    }
                    try
                    {
                        using (NetworkStream stream = tcpClient.GetStream())
                        {
                            if (stream.CanWrite)
                            {
                                if (command?.Equals(readCommand) == true)
                                {
                                    byte[] commandArr = Encoding.ASCII.GetBytes(command + Convert.ToChar(13));
                                    stream.Write(commandArr, 0, commandArr.Length);
                                }
                                else if (command != null && zero && command.Equals(zeroCommand))
                                {
                                    byte[] commandArr = Encoding.ASCII.GetBytes(command + Convert.ToChar(13));
                                    stream.Write(commandArr, 0, commandArr.Length);
                                    zero = false;
                                }
                            }

                            Thread.Sleep(200);
                            if (stream.CanRead)
                            {
                                if (!stream.DataAvailable)
                                {
                                    return;
                                }
                                byte[] dataBuffer = new byte[256];
                                int numBytes = stream.Read(dataBuffer, 0, dataBuffer.Length);
                                int index = Array.LastIndexOf(dataBuffer, byte.Parse("13"));
                                if (index >= 8)
                                {
                                    string responseData = Encoding.ASCII.GetString(dataBuffer, index - 8, 8).Replace(Convert.ToChar(0).ToString(), "");
                                    if (responseData.Contains(Convert.ToChar(13)) && responseData.Split(Convert.ToChar(13))[1] != null && (responseData.IndexOf(Convert.ToChar(43)) > 0 || responseData.IndexOf(Convert.ToChar(45)) > 0))
                                        responseData = responseData.Split(Convert.ToChar(13))[1];
                                    weighingResult = responseData;
                                }
                            }
                            else
                            {
                                weighingResult = "-----";
                            }

                            //if (command != null && zero && command.Equals(zeroCommand))
                            //{
                            //    Thread.Sleep(150);
                            //    byte[] commandArr = Encoding.ASCII.GetBytes(command + Convert.ToChar(13));
                            //    stream.Write(commandArr, 0, commandArr.Length);
                            //    zero = false;
                            //}
                        }
                    }
                    catch (IOException ioEx)
                    {
                        error = true;
                        errorMessage = "I/O Exception " + ioEx.Message;
                        OnError?.Invoke(this, EventArgs.Empty);
                    }
                    catch (UnauthorizedAccessException uaEx)
                    {
                        error = true;
                        errorMessage = "Unauthorized Access Exception" + uaEx.Message;
                        OnError?.Invoke(this, EventArgs.Empty);
                    }
                    catch (ArgumentOutOfRangeException argEx)
                    {
                        error = true;
                        errorMessage = "Argument Exception" + argEx.Message;
                        OnError?.Invoke(this, EventArgs.Empty);
                    }
                }
            }
            catch (SocketException ex)
            {
                error = true;
                errorMessage = "Socket Exception " + ex.Message;
                OnError?.Invoke(this, EventArgs.Empty);
            }
            catch (TimeoutException ex)
            {
                error = true;
                errorMessage = "Timeout Exception " + ex.Message;
                OnError?.Invoke(this, EventArgs.Empty);
            }
            catch (IOException ioEx)
            {
                error = true;
                errorMessage = "I/O Exception " + ioEx.Message;
                OnError?.Invoke(this, EventArgs.Empty);
            }
            catch (UnauthorizedAccessException uaEx)
            {
                error = true;
                errorMessage = "Unauthorized Access Exception" + uaEx.Message;
                OnError?.Invoke(this, EventArgs.Empty);
            }
            catch (ArgumentOutOfRangeException argEx)
            {
                error = true;
                errorMessage = "Argument Exception" + argEx.Message;
                OnError?.Invoke(this, EventArgs.Empty);
            }
        }

        protected void BgEthernetConnectionWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled)
            {
                command = !zero ? readCommand : zeroCommand;
                bgEthernetConnectionWorker.RunWorkerAsync();
            }
        }

        #endregion ETHERNET CONNECTION

        public void ZeroWeighing()
        {
            zero = true;
        }

        public string ReturnWeight()
        {
            return weighingResult;
        }

        public void SetTimeOut(int timeout)
        {
            TimeoutTime = timeout;
        }

        protected void Start()
        {
            if (isSerial)
            {
                bgSerialConnectionWorker.DoWork += BgSerialConnectionWorker_DoWork;
                bgSerialConnectionWorker.RunWorkerCompleted += BgSerialConnectionWorker_RunWorkerCompleted;
                bgSerialConnectionWorker.RunWorkerAsync();
            }
            else
            {
                bgEthernetConnectionWorker.DoWork += BgEthernetConnectionWorker_DoWork;
                bgEthernetConnectionWorker.RunWorkerCompleted += BgEthernetConnectionWorker_RunWorkerCompleted;
                bgEthernetConnectionWorker.RunWorkerAsync();
            }
        }

        protected void Stop()
        {
            if (isSerial)
            {
                bgSerialConnectionWorker.DoWork -= BgSerialConnectionWorker_DoWork;
                bgSerialConnectionWorker.RunWorkerCompleted -= BgSerialConnectionWorker_RunWorkerCompleted;
            }
            else
            {
                bgEthernetConnectionWorker.DoWork -= BgEthernetConnectionWorker_DoWork;
                bgEthernetConnectionWorker.RunWorkerCompleted -= BgEthernetConnectionWorker_RunWorkerCompleted;
            }
        }
    }
}