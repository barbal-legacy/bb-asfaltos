Known Issues:

- Everytime we add an entry in any of the combobox lists (add new row in DB) when creating new ticket, the temporary ticket, that might be created from it, won't have this newly added comboboxes values in it;

- When using Com1 (while not having communication and only in this port) the program thinks the connection is open, creating a multi-thread problem (high cpu usage) - If assistance is needed and needs to change port on configuration we need to charge the assistance because the user/client doesn't need to edit/change the configuration port (still can change the tare, but not the port itself);

- Double click on Listing Header executes the EditCommand of the Rows and inutilizes the order function until calling the page list again;